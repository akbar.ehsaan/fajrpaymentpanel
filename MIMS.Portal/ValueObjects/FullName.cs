﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects
{
    public class FullName
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
    }
}
