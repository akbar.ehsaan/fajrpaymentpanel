﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum ResidenceType
    {
        [Display(Name = "مالکیت")]
        Ownership = 1,

        [Display(Name = "اجاره")]
        Rent = 2,

        [Display(Name = "مجوز فعالیت")]
        Licensed = 3, 
        
        [Display(Name = "استشهاد محلی")]
        LocalTestimony =4,
    }
}
