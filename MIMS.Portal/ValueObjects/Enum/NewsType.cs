﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum NewsType
    {
        [Display(Name = "عادی")]
        Normal = 1,

        [Display(Name = "اطلاعیه")]
        Announcement = 2,
    }
}
