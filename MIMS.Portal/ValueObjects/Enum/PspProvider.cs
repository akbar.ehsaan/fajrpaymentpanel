﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PspProvider
    {
        [Display(Name = "سداد (فقط تهران)")]
        Sadad = 1, //"22daf779-398a-4c1e-b2a8-a033a65aeb76"

        [Display(Name = "فن آوا")]
        Fanava = 2, // "042727ac-6f35-4216-b50d-35e68de23900"

        [Display(Name = "پرداخت الکترونیک پاسارگاد")]
        Pep = 3, //"dcdc2023-cabc-42b8-bc5e-325174a053c1"
    }
}
