﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum UserType
    {
        [Display(Name = "مدیر")]
        Admin = 1,

        [Display(Name = "نماینده")]
        Agent = 2,

        [Display(Name = "پذیرنده")]
        Merchant = 3
    }
}
