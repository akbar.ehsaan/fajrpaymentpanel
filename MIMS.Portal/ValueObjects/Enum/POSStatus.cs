﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PosStatus
    {
        [Display(Name = "ناشناس")]
        Unknown = 0,

        [Display(Name = "آزاد")]
        Free = 1,

        [Display(Name = "ترمینال اختصاص داده شده")]
        TerminalAssigned = 2,
    }
}
