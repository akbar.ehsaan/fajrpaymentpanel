﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum TicketStatus
    {
        [Display(Name = "جدید")]
        New,

        [Display(Name = "باز/فعال")]
        Open,

        [Display(Name = "پاسخ داده شده")] 
        Answered,

        [Display(Name = "منتظر")]
        Pending,

        [Display(Name = "در حال پیگیری")]
        OnHold, 
        
        [Display(Name = "در حال انجام")] 
        InProgress,

        [Display(Name = "حل شده")]
        Solved,

        [Display(Name = "بسته")]
        Closed,

        //New     Open
        //Open    Open
        //Pending Awaiting your reply
        //On-hold Open
        //Solved  Solved
        //Closed  Solved
    }
}
