﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum FileType
    {
        [Display(Name = "ناشناس")]
        Unknown = 0,
        IDCardFront = 1,
        IDCardBack = 2,
        CredentialMainPage = 3,
        CredentialLastPage = 4,
        StoreDocument = 5,
    }
}
