﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum Gender
    {
        [Display(Name = "زن")]
        Female = 1,

        [Display(Name = "مرد")]
        Male = 2
    }
}
