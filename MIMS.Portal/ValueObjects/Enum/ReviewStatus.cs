﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum ReviewStatus
    {
        [Display(Name = "نامشخص")]
        Unknown = 0,

        [Display(Name = "منتظر بررسی")]
        Pending = 1,

        [Display(Name = "تایید اولیه")]
        PreApproved = 2,

        [Display(Name = "تایید نهایی")]
        Approved = 3,

        [Display(Name = "رد شده")]
        Rejected = 4,

        [Display(Name = "منتظر ویرایش")] 
        OpenForEditing = 5,

        [Display(Name = "منتظر تکمیل توسط پذیرنده")]
        AwaitingToCompleteByMerchant = 6,

        [Display(Name = "منتظر بروزرسانی توسط پذیرنده")]
        AwaitingToUpdateByMerchant = 6
    }
}
