﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PersonType
    {
        [Display(Name = "حقیقی")]
        Real = 1,

        [Display(Name = "حقوقی")]
        Legal = 2
    }
}
