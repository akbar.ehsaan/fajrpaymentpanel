﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PosOwnerType
    {
        [Display(Name = "ناشناس")]
        Unknown = 0,

        [Display(Name = "شخصی")]
        Private = 1,
    }
}
