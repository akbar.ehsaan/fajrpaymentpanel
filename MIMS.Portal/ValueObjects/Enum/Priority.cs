﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects
{
    public enum Priority
    {
        [Display(Name = "معمولی")]
        Normal = 1,

        [Display(Name = "مهم")]
        Important = 2,

        [Display(Name = "فوری")]
        Urgent = 3
    }
}
