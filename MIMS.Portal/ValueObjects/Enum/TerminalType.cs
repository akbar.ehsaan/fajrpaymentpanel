﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum TerminalType
    {
        [Display(Name = "هدایت تراکنش")]
        TransactionDispatch = 1,
    }
}
