﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum TerminalStatus
    {
        [Display(Name = "نامشخص")]
        Unknown,

        [Display(Name = "منتظر بررسی")]
        Pending,

        [Display(Name = "تایید اولیه")]
        PreApproved,

        [Display(Name = "تعیین سرویس دهنده")]
        PspAssigned,

        [Display(Name = "ارسال شده")]
        Sent,

        [Display(Name = "منتظر وضعیت شاپرک")]
        AwaitingForShaparakResponse,

        [Display(Name = "تایید نشده - اطلاعات هویتی")]
        RejectedByWhois,

        [Display(Name = "تایید نشده - اطلاعات حساب")]
        RejectedByBankAccount,

        [Display(Name = "تایید نشده - اطلاعات فروشگاه")]
        RejectedByStore,

        [Display(Name = "تایید نشده - سایر")]
        RejectedByOther,

        [Display(Name = "تایید شده")]
        Approved,

        [Display(Name = "تخصیص دستگاه")]
        DeviceAssigned,

        [Display(Name = "ثبت دستگاه")] 
        DeviceSubmitted,

        [Display(Name = "فعالسازی")] 
        InitialSetup,

        [Display(Name = "نصب و تحویل")] 
        Delivery,

        [Display(Name = "فعال")]
        Active,

        [Display(Name = "غیرفعال")]
        Inactive,

        Retaked,

        Revoked,
        Withdrawal,
        Canceled
    }
}
