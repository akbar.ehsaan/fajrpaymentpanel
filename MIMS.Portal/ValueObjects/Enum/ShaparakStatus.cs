﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum ShaparakStatus
    {
        [Display(Name = "تایید نشده - اطلاعات هویتی")]
        RejectedByWhois = 1,

        [Display(Name = "تایید نشده - اطلاعات حساب")]
        RejectedByBankAccount = 2,

        [Display(Name = "تایید نشده - اطلاعات فروشگاه")]
        RejectedByStore = 3,

        [Display(Name = "تایید نشده - سایر")]
        RejectedByOther = 4,

        [Display(Name = "تایید شده")]
        Approved = 5,
    }
}
