﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PosConnectionType
    {
        [Display(Name = "شبکه (LAN)")]
        LAN = 1,

        [Display(Name = "سیمکارت (GPRS)")]
        GPRS = 2,

        [Display(Name = "تلفنی (DialUp)")]
        DialUp = 3,

        [Display(Name = "بی سیم (WiFi)")]
        WiFi = 4,
    }
}
