﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum FileSource
    {
        //[Display(Name = "")]
        Unknown = 0,
        Merchant = 10,
        BankAccount = 20,
        Store = 30,
        Ticket = 40,
        Others = 50,
    }
}
