﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ValueObjects.Enum
{
    public enum PosOSType
    {
        [Display(Name = "هوشمند")]
        Smart = 1,

        [Display(Name = "غیرهوشمند")]
        NonSmart = 2,
    }
}
