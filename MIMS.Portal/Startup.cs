using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MIMS.Portal.Infrustructure;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.IdentityModel.Tokens;
using UoN.ExpressiveAnnotations.NetCore.DependencyInjection;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using MIMS.Portal.Commons;
using MIMS.Portal.Commons.Middleware;
using MIMS.Portal.Core.Biz.Services;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Repositories;
using MIMS.Portal.Core.Data.Repositories;

namespace MIMS.Portal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                   options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //--- Lowercase Urls ---//
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddRouting(options =>
            {
                options.ConstraintMap["slugify"] = typeof(SlugifyParameterTransformer);
            });
            services.AddControllersWithViews();
            //services.AddRazorPages(options =>
            //{
            //    options.Conventions.Add(new PageRouteTransformerConvention(new SlugifyParameterTransformer()));
            //});
            //services.AddMvc(options =>
            //{
            //    options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            //});
            //services.AddMvc()
            //services.AddSession();            
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
            });
            services.AddExpressiveAnnotations();

            //--- DB Context ---//
            services.AddScoped<AppDbContext, AppDbContext>();
            //--- Repositories ---//
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IMerchantRepository, MerchantRepository>();
            services.AddScoped<IBankAccountRepository, BankAccountRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IGeoRepository, GeoRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IDeviceRepository, DeviceRepository>();
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<IStoreRepository, StoreRepository>();
            services.AddScoped<ITerminalRepository, TerminalRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IPartnerRepository, PartnerRepository>();
            services.AddScoped<IStoreScopeRepository, StoreScopeRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<IPosBrandRepository, PosBrandRepository>();
            //-- Services --//
            services.AddScoped<IMerchantService, MerchantService>();
            services.AddScoped<IBankAccountService, BankAccountService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IGeoService, GeoService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IDeviceService, DeviceService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<ITerminalService, TerminalService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IFilesManagerService, FilesManagerService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IPartnerService, PartnerService>();
            services.AddScoped<IStoreScopeService, StoreScopeService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IPosBrandService, PosBrandService>();
            //
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(option =>
                    {
                        option.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = "localhost",
                            ValidAudience = "localhost",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("wevhgfrtyhasdfghjklzxcvbnm"))
                        };
                    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseAuthMiddleware();

            if (env.IsDevelopment())
            {
                app.UseStatusCodePagesWithReExecute("/Errors", "?status={0}");
                app.UseExceptionHandler("/Errors");
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Errors", "?status={0}");
                app.UseExceptionHandler("/Errors");
                //The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseStatusCodePages(async context =>
            {
                var request = context.HttpContext.Request;
                var response = context.HttpContext.Response;

                if (response.StatusCode == (int)HttpStatusCode.Unauthorized)
                // you may also check requests path to do this only for specific methods       
                // && request.Path.Value.StartsWith("/specificPath")

                {
                    response.Redirect("/account/sign-in");
                }
            });
            app.UseSession();
            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    //pattern: "{controller=Home}/{action=Index}/{id?}");
                    pattern: "{controller:slugify=Home}/{action:slugify=Index}/{id:slugify?}");
        });
        }
    }
}
