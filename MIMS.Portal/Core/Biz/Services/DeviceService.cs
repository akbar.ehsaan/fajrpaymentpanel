﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class DeviceService : IDeviceService
    {
        public IDeviceRepository _deviceRepository;
        public DeviceService(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public bool Add(PosDevice device)
        {
            return _deviceRepository.Add(device);
        }

        public bool Delete(string id)
        {
            return _deviceRepository.Remove(id);
        }

        public List<PosDevice> FindDevice()
        {
            return _deviceRepository.FindDevice();
        }

        public PosDevice FindById(Guid id)
        {
            return _deviceRepository.FindById(id);
        }

        public List<PosDevice> FindByModelId(Guid id)
        {
            return _deviceRepository.FindDevice().Where(i => i.PosDeviceModelId == id).ToList();
        }

        public List<PosDevice> FindDeviceBySerial(string serial)
        {
            return _deviceRepository.FindDeviceBySerial(serial);
        }

        public bool Update(PosDevice device)
        {
            return _deviceRepository.Edit(device);
        }
    }
}
