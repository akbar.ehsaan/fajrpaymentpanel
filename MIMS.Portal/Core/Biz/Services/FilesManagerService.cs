﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class FilesManagerService : IFilesManagerService
    {
        public IFileRepository _fileuploadRepository;
        private IHostingEnvironment _env;

        public FilesManagerService(IFileRepository fileuploadRepository, IHostingEnvironment env)
        {
            _fileuploadRepository = fileuploadRepository;
            _env = env;
        }

        public bool Create(UploadFile file)
        {

            return _fileuploadRepository.Add(file);
        }

        public bool Delete(Guid id)
        {
            return _fileuploadRepository.Remove(id);
        }

        public List<UploadFile> FindByParentId(Guid parentId)
        {
            return _fileuploadRepository.FindByParentId(parentId);
        }

        public UploadFile FindById(Guid id)
        {
            return _fileuploadRepository.FindById(id);
        }

        public bool SaveFile(IFormFile file, FileSource fileSource, FileType fileType, Guid parentItemId, Guid? childItemId)
        {
            if (file != null && file.Length > 0)
            {
                //try
                {
                    var contentType = file.ContentType;
                    var fileExtension = Path.GetExtension(file.FileName).ToLower();
                    var uploadFile = new UploadFile()
                    {
                        Source = fileSource,
                        Type = fileType,
                        ParentItemId = parentItemId,
                    };

                    var subRoot = fileSource == FileSource.Ticket ? "Tickets" : "Merchats";
                    var fileName = uploadFile.FileName = $"{uploadFile.Type}_{uploadFile.Id}{fileExtension}";
                    var basePath = $"{_env.WebRootPath}/Attachments/{subRoot}/{parentItemId}/{fileSource}";

                    if (!Directory.Exists(basePath))
                        Directory.CreateDirectory(basePath);

                    var filePath = $"{basePath}/{fileName}";

                    using (var stream = File.Create(filePath))
                    {
                        file.CopyToAsync(stream);
                        this.Create(uploadFile);
                    }
                }
                //catch (Exception ex)
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }


            return false;
        }
        public void SetStatus(Guid id, ReviewStatus status)
        {
            var file = _fileuploadRepository.FindById(id);

            if (file != null)
            {
                file.Status = status;
                _fileuploadRepository.Edit(file);
            }
        }
    }

}
