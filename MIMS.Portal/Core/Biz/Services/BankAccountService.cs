﻿using System;
using System.Collections.Generic;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.ValueObjects.Enum;


namespace MIMS.Portal.Core.Biz.Services
{
    public class BankAccountService : GenericService<BankAccount>, IBankAccountService
    {
        protected readonly IBankAccountRepository _bankAccountRepository;

        public BankAccountService(IBankAccountRepository bankAccountRepository, IGenericRepository<BankAccount> gRepository) : base(gRepository)
        {
            _bankAccountRepository = bankAccountRepository;
        }

        public bool Create(BankAccount bankAccount)
        {
            return _bankAccountRepository.Add(bankAccount);
        }

        public bool Edit(BankAccount bankAccount)
        {
            return _bankAccountRepository.Edit(bankAccount);
        }

        public bool Delete(Guid id)
        {
            return _bankAccountRepository.Remove(id);
        }

        public BankAccount GetById(Guid id)
        {
            return _bankAccountRepository.FindById(id);
        }

        public List<BankAccount> GetByMerchantId(Guid id)
        {
            return _bankAccountRepository.FindByMerchantId(id);
        }
        public List<BankAccount> GetAll()
        {
            return _bankAccountRepository.Find();
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            return _bankAccountRepository.StatsByReviewStatus(userId);
        }
        public IEnumerable<BankAccount> GetByUserId(Guid id)
        {
            //var merchants = 
            return _bankAccountRepository.FindByUserId(id);
        }

        public long StatsByCount(Guid? userId)
        {
            return _bankAccountRepository.StatsByCount(userId);
        }


    }
}
