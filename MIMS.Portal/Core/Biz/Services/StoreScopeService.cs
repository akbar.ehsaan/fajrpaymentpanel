﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class StoreScopeService : IStoreScopeService
    {
        IStoreScopeRepository _storeScopeRepository;
        public StoreScopeService(IStoreScopeRepository storeScopeRepository)
        {
            _storeScopeRepository = storeScopeRepository;
        }
        public List<BusinessScope> GetAll()
        {
            return _storeScopeRepository.GetAll();
        }

        public List<string> MergedScope()
        {
            List<BusinessScope> lstRoot = _storeScopeRepository.GetSectionsById(Guid.Empty).ToList();
            List<string> lstresult = new List<string>();
            foreach (BusinessScope rootitem in lstRoot)
            {
                List<BusinessScope> lstchild = _storeScopeRepository.GetSectionsById(rootitem.Id).ToList();

                foreach (BusinessScope parentitem in lstchild)
                {
                    lstresult.Add(rootitem.Name + "-" + parentitem.Name);

                }
            }
            return lstresult;
        }
    }
}
