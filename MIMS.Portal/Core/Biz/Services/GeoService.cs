﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class GeoService : IGeoService
    {
        IGeoRepository _countrySectionRepository;
        public GeoService(IGeoRepository countrySectionRepository)
        {
            _countrySectionRepository = countrySectionRepository;
        }
        public List<GeoSection> FindById(Guid id)
        {
            return _countrySectionRepository.FindById(id);
        }
        public List<GeoSection> GetSectionsById(Guid? id)
        {
            return _countrySectionRepository.GetSectionsById(id).OrderBy(x=>x.Name).ToList();
        }

        public List<GeoSection> GetParents()
        {
            Guid? id = null;
            return _countrySectionRepository.GetSectionsById(id.GetValueOrDefault());
        }
    }
}
