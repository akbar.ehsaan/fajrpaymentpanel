﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class PosBrandService : IPosBrandService
    {
        IPosBrandRepository _posBrand;
        
        public PosBrandService(IPosBrandRepository posBrand)
        {
            _posBrand = posBrand;
        }

        public bool Add(PosBrand posBrand)
        {
            return  _posBrand.Add(posBrand);
        }

        public bool Delete(Guid id)
        {
            return _posBrand.Remove(id);
        }

        public List<PosBrand> Find()
        {
            return _posBrand.Find();
        }

        public List<PosBrand> FindByParentId(Guid id)
        {
            return _posBrand.FindByParentId(id);
        }

        public List<PosBrand> FindParents()
        {
            return _posBrand.Find().Where(i=>i.ParentId == null || i.ParentId == Guid.Empty).ToList();
        }

        public PosBrand FindById(Guid id)
        {
            return _posBrand.FindById(id);
        }

        public bool Update(PosBrand brand)
        {
            return _posBrand.Edit(brand);
        }
    }
}
