﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.IServices;

namespace MIMS.Portal.Core.Biz.Services
{
    public class GenericService<T> : IGenericService<T> where T : class, new()
    {
        protected readonly IGenericRepository<T> _gRepository;

        public GenericService(IGenericRepository<T> gRepository)
        {
            _gRepository = gRepository;
        }

        //        #region Get Functions

        //        //public T SingleOrDefault(Expression<Func<T, bool>> predicate = null,
        //        //    Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
        //        //    Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, bool enableTracking = true,
        //        //    bool ignoreQueryFilters = false)
        //        //{
        //        //    IQueryable<T> query = _dbSet;

        //        //    if (!enableTracking) query = query.AsNoTracking();

        //        //    if (include != null) query = include(query);

        //        //    if (predicate != null) query = query.Where(predicate);

        //        //    if (ignoreQueryFilters) query = query.IgnoreQueryFilters();

        //        //    return orderBy != null ? orderBy(query).FirstOrDefault() : query.FirstOrDefault();
        //        //}

        //        public void Get(T entity)
        //        {
        //            _dbSet.Remove(entity);
        //        }

        //        #endregion

        //        public void Dispose()
        //        {
        //            _dbContext?.Dispose();
        //        }


        //        public void Delete(T entity)
        //        {
        //            _dbSet.Remove(entity);
        //        }

        //        public void Delete(params T[] entities)
        //        {
        //            _dbSet.RemoveRange(entities);
        //        }

        //        public void Delete(IEnumerable<T> entities)
        //        {
        //            _dbSet.RemoveRange(entities);
        //        }

        //public void Get()
        //{
        //    //return _dbSet.Add(entity).Entity;
        //}

        //public void GetById(Guid id)
        //{
        //    //_dbSet.AddRange(entities);
        //}

        //#region Insert Functions

        //public void Insert(T entity)
        //{
        //    //return _dbSet.Add(entity).Entity;
        //}

        //public void Insert(params T[] entities)
        //{
        //    //_dbSet.AddRange(entities);
        //}

        //public void Insert(IEnumerable<T> entities)
        //{
        //    //_dbSet.AddRange(entities);
        //}

        //#endregion


        //        #region Update Functions

        //        public void Update(T entity)
        //        {
        //            _dbSet.Update(entity);
        //        }

        //        public void Update(params T[] entities)
        //        {
        //            _dbSet.UpdateRange(entities);
        //        }

        //        public void Update(IEnumerable<T> entities)
        //        {
        //            _dbSet.UpdateRange(entities);
        //        }

        //        #endregion



        public IEnumerable<T> Filter(Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            bool enableTracking = true)
        {
            return _gRepository.Filter(predicate, orderBy, include, enableTracking);
        }

    }
}
