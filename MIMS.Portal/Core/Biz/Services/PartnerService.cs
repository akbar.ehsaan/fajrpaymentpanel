﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class PartnerService: IPartnerService
    {
        IPartnerRepository _partnerRepository;
        public PartnerService(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }

        public bool Add(Partner entity)
        {
            

            return _partnerRepository.Add(entity);
        }

        public bool Remove(string id)
        {
            return _partnerRepository.Remove(id);
        }

        public List<Partner> FindByMerchantId(string merchantid)
        {
            return _partnerRepository.FindByMerchantId(merchantid);
        }


    }
}
