﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class DepartmentService : IDepartmentService
    {
        IDepartmentRepository _departmentRepository;
        public DepartmentService(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }
        public List<Department> Find()
        {
         return   _departmentRepository.Find();
        }

        public List<Department> FindById(string id)
        {
            return _departmentRepository.FindById(id);
        }
    }
}
