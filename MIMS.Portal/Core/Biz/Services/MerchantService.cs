﻿using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ValueObjects.Enum;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Service
{
    public class MerchantService : IMerchantService
    {
        IMerchantRepository _merchantRepository;
        public MerchantService(IMerchantRepository merchantRepositoy)
        {
            _merchantRepository = merchantRepositoy;
        }

        public bool Create(Merchant merchant)
        {
            //merchant.Id = new Guid();
            //merchant.cre = DateTime.Now;

            return _merchantRepository.Add(merchant);
        }

        public bool Delete(Guid id)
        {
            return _merchantRepository.Remove(id);
        }

        public Merchant GetById(Guid id)
        {
            return _merchantRepository.FindById(id);
        }

        public IEnumerable<Merchant> GetByUserId(Guid id)
        {
            return _merchantRepository.FindByUserId(id);
        }

        public Merchant GetByNationalCode(string nationalCode)
        {
            return _merchantRepository.Find().Where(u => u.NationalCode == nationalCode).FirstOrDefault();
        }

        public IEnumerable<Merchant> GetAll()
        {
            return _merchantRepository.Find();
        }

        public IEnumerable<ChartView> MerchantDateCountReport()
        {
            return _merchantRepository.Find().GroupBy(i => i.CreatedDate.Date).Select(m => new ChartView { key = m.Key.ToPeString(), value = m.Count() }).ToList();
        }

        public bool Edit(Merchant merchant)
        {
            return _merchantRepository.Update(merchant);
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            return _merchantRepository.StatsByReviewStatus(userId);
        }

        public long StatsByCount(Guid? userId)
        {
            return _merchantRepository.StatsByCount(userId);
        }

        public double StatsByGain(Guid? userId, DateTime? startDate = null, DateTime? endDate = null)
        {
            return _merchantRepository.StatsByGain(userId, startDate, endDate);
        }

        public IEnumerable<Merchant> Filter(Expression<Func<Merchant, bool>> predicate = null,
            Func<IQueryable<Merchant>, IOrderedQueryable<Merchant>> orderBy = null,
            Func<IQueryable<Merchant>, IIncludableQueryable<Merchant, object>> include = null,
            bool enableTracking = true)
        {
            return _merchantRepository.Filter(predicate, orderBy, include, enableTracking);
        }

    }
}
