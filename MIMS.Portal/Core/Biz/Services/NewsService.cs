﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class NewsService:INewsService
    {
        public INewsRepository _newsRepository;
        public NewsService(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public bool Add(News entity)
        {
            entity.CreatedDate = DateTime.Now;
            return _newsRepository.Add(entity);
        }

        public List<News> FindAll()
        {
            return _newsRepository.FindAll();
        }

        public List<News> FindById(string id)
        {
            return _newsRepository.FindById(id);
        }

        public bool Remove(string id)
        {
            return _newsRepository.Remove(id);
        }
    }
}
