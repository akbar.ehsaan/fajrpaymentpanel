﻿using MIMS.Portal.Infrustructure;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.ViewModels;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using System.Web.Helpers;
using MIMS.Portal.Commons;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Service
{
    public class UsersService : IUsersService
    {
        public IUserRepository _userRepository;
        public UsersService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        private SecurityTools _passwordTools = new SecurityTools();

        public bool Create(User user)
        {
            var salt = Crypto.GenerateSalt();

            user.Salt = _passwordTools.Base64Encode(salt);
            user.Password = Crypto.HashPassword(_passwordTools.GetPhraseKey(user.Password, salt));

            return _userRepository.Add(user);
        }

        public bool ChangePassword(Guid userId, string oldPassword, string newPassword)
        {
            User user = FindById(userId);

            var userPassword = oldPassword + _passwordTools.Base64Decode(user.Salt);

            if (Crypto.VerifyHashedPassword(user.Password, userPassword))
            {
                var newSalt = Crypto.GenerateSalt();

                user.Salt = _passwordTools.Base64Encode(newSalt);
                user.Password = Crypto.HashPassword(_passwordTools.GetPhraseKey(newPassword, newSalt));

                return _userRepository.Edit(user);
            }

            else
                return false;
        }

        public bool SetPassword(Guid userId, string newPassword)
        {
            User user = FindById(userId);

            var newSalt = Crypto.GenerateSalt();

            user.Salt = _passwordTools.Base64Encode(newSalt);
            user.Password = Crypto.HashPassword(_passwordTools.GetPhraseKey(newPassword, newSalt));

            return _userRepository.Edit(user);

        }

        public void UpdatePassword()
        {
            var users = FindUser();

            foreach (var user in users)
            {


                    var salt = Crypto.GenerateSalt();

                    user.Salt = _passwordTools.Base64Encode(salt);
                    user.Password = Crypto.HashPassword(_passwordTools.GetPhraseKey(user.NationalCode, salt));

                    _userRepository.Edit(user);
                

            }
        }

        public bool Delete(Guid id)
        {
            return _userRepository.Remove(id);
        }

        public User FindById(Guid id)
        {
            return _userRepository.FindById(id);
        }

        public List<User> FindByRole(string role)
        {

            return _userRepository.FindByRole(role);
        }
        public User FindByNationalCode(string nationalCode)
        {
            return _userRepository.Find().Where(u => u.NationalCode == nationalCode).FirstOrDefault();
        }
        public User FindByUserName(string userName)
        {
            //return _userRepository.Find().Where(u => u.UserName.Equals(userName,StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            return _userRepository.Find().Where(u => u.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
        }

        public List<User> FindUser()
        {
            return _userRepository.Find();
        }
        //public List<ChartView> UserMerchantCountDateReport()
        //{
        //    return _userRepository.Find().GroupBy(i =>  new {  i.Provience ,i.me}).Select(m => new ChartView { key = m.Key.Name, value = m.Count() }).ToList();
        //}

        public string SignIn(string username, string password)
        {
            //var user = _userRepository.FindByUsernamePassword(username, password);
            var user = FindByUserName(username);

            if (user != null)
            {
                var userPassword = password + _passwordTools.Base64Decode(user.Salt);

                if (Crypto.VerifyHashedPassword(user.Password, userPassword))
                {
                    //User _user = _userRepository.FindByUsernamePassword(username, password).First();

                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("wevhgfrtyhasdfghjklzxcvbnm"));
                    var sigin = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                    var claims = new List<Claim>
                {
                    new Claim("userId",user.Id.ToString()),
                    new Claim("userName",user.UserName),
                    new Claim("roles",user.Role),
                    new Claim("userRole",user.Role),
                    new Claim("userRoleTag", ((UserType)Enum.Parse(typeof(UserType), user.Role, true)).GetDisplayName()),
                    new Claim("userFullName",user.Fullname),

                };

                    var tokenOption = new JwtSecurityToken(
                        issuer: "localhost",
                        audience: "localhost",
                        claims: claims,
                        expires: DateTime.Now.AddDays(30),
                        signingCredentials: sigin);

                    var strToken = new JwtSecurityTokenHandler().WriteToken(tokenOption);

                    return strToken.ToString();
                }

                return "Not_Found";
            }
            else
            {
                return "Not_Found";
            }

        }

    }
}
