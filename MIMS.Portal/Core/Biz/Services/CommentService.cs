﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class CommentService : ICommentService
    {
        ICommentRepository _commentRepository;
        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public bool Add(Comment entity)
        {
            entity.SubmitDate = DateTime.Now;
          return   _commentRepository.Add(entity);
        }

        public List<Comment> FindByMerchantId(string id)
        {
            return _commentRepository.FindByMerchantId(id);
        }
    }
}
