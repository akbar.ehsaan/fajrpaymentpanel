﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Commons.Extensions;
namespace MIMS.Portal.Service
{
    public class TicketService : ITicketService
    {
        ITicketRepository _ticketRepository;
        IUsersService _userService;
        public TicketService(ITicketRepository ticketRepository,IUsersService userService)
        {
            _ticketRepository = ticketRepository;
            _userService = userService;

        }
        public bool Add(Ticket ticket)
        {
            return _ticketRepository.Add(ticket);
        }

        public bool Close(string ticketid)
        {
           Ticket t= _ticketRepository.FindById(ticketid).First();
            t.TicketStatus = "Close";
            _ticketRepository.Edit(t);
            return true;
        }

        public List<TicketView> FindTicketDetailById(Guid id)
        {
            return ConvertToTicketView(_ticketRepository.FindTicketDetailById(id));
        }

        public List<TicketView> TicketByUserId(string userid)
        {
            return ConvertToTicketView(_ticketRepository.TicketByUserId(userid));
        }

        public List<ChartView> TicketDailyReport()
        {
            return _ticketRepository.Find().GroupBy(i => i.SubmitDate.Date).Select(m => new ChartView { key = m.Key.ToPeString(), value = m.Count() }).ToList();
        }
        private List<TicketView> ConvertToTicketView(List<Ticket> lst)
        {
            List<TicketView> fn = new List<TicketView>();

            foreach (Ticket item in lst)
            {
                TicketView tv = new TicketView();
                tv.Id = item.Id;
                tv.ParentId = item.ParentId;
                tv.Subject = item.Subject;
                tv.SubmitDate = item.SubmitDate;
                tv.TicketStatus = item.TicketStatus;
                tv.Body = item.Body;
                tv.SenderUser = _userService.FindById(item.SenderUserId);
                tv.ReciverrUser = _userService.FindById(item.ReciverrUserId);
                fn.Add(tv);
            }
            return fn;

        }
    }
}
