﻿using System;
using System.Collections.Generic;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.Service;

namespace MIMS.Portal.Core.Biz.Services
{
    public class BankService : IBankService
    {
        IBankRepository _bankRepository;

        public BankService(IBankRepository bankRepository)
        {
            _bankRepository = bankRepository;
        }

        public bool Add(Bank bank)
        {
            bank.Id = new Guid();
            bank.SubmitDate = DateTime.Now;
            return _bankRepository.Add(bank);
        }

        public bool Delete(Guid id)
        {
            return _bankRepository.Remove(id);
        }

        public List<Bank> Find()
        {
            return _bankRepository.Find();
        }

        public Bank FindById(Guid id)
        {
            return _bankRepository.FindById(id);
        }

    }
}
