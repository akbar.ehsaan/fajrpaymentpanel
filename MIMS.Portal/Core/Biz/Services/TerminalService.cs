﻿using System;
using System.Collections.Generic;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.ValueObjects.Enum;


namespace MIMS.Portal.Core.Biz.Services
{
    public class TerminalService : GenericService<Terminal>, ITerminalService
    {
        protected readonly ITerminalRepository _terminalRepository;

        public TerminalService(ITerminalRepository terminalRepository, IGenericRepository<Terminal> gRepository) : base(gRepository)
        {
            //_genericService = genericService;
            _terminalRepository = terminalRepository;
        }

        public bool Create(Terminal terminal)
        {
            //terminal.Id = new Guid();
            //terminal.SubmitDate = DateTime.Now;
            return _terminalRepository.Add(terminal);
        }

        public bool Delete(Guid id)
        {
            return _terminalRepository.Remove(id);
        }

        public bool Edit(Terminal terminal)
        {
            return _terminalRepository.Edit(terminal);
        }

        public List<Terminal> FindByMerchantId(Guid id)
        {

            return _terminalRepository.FindByMerchantId(id);
        }
        public IEnumerable<Terminal> GetByUserId(Guid id)
        { return _terminalRepository.FindByUserId(id); }
        public List<Terminal> Find()
        {

            return _terminalRepository.Find();
        }
        public Terminal FindById(Guid id)
        {

            return _terminalRepository.FindById(id);
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            return _terminalRepository.StatsByReviewStatus(userId);
        }

        public IEnumerable<KeyValuePair<TerminalStatus, long>> StatsByTerminalStatus(Guid? userId)
        {
            return _terminalRepository.StatsByTerminalStatus(userId);
        }

        public IEnumerable<KeyValuePair<string, long>> StatsByStatus(Guid? userId)
        {
            return _terminalRepository.StatsByStatus(userId);
        }

        public IEnumerable<Terminal> GetByPsp(PspProvider pspProvider, TerminalStatus? terminalStatus)
        { return _terminalRepository.FindByPsp(pspProvider, terminalStatus); }

        public IEnumerable<Terminal> GetByTerminalStatus(TerminalStatus status)
        { return _terminalRepository.FindByTerminalStatus(status); }

        public long StatsByCount(Guid? userId)
        {
            return _terminalRepository.StatsByCount(userId);
        }

        //public IEnumerable<Terminal> Filter(Expression<Func<Terminal, bool>> predicate = null,
        //    Func<IQueryable<Terminal>, IOrderedQueryable<Terminal>> orderBy = null,
        //    Func<IQueryable<Terminal>, IIncludableQueryable<Terminal, object>> include = null,
        //    bool enableTracking = true)
        //{
        //    return _terminalRepository.Filter(predicate, orderBy, include, enableTracking);
        //}
    }
}
