﻿using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Biz.Services;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class StoreService : GenericService<Store>, IStoreService
    {
        protected readonly IStoreRepository _storeRepository;

        public StoreService(IStoreRepository storeRepository, IGenericRepository<Store> gRepository) : base(gRepository)
        {
            _storeRepository = storeRepository;
        }

        public bool Create(Store store)
        {
            return _storeRepository.Add(store);
        }

        public bool Delete(Guid id)
        {
            return _storeRepository.Remove(id);
        }

        public bool Edit(Store store)
        {
            return _storeRepository.Edit(store);
        }

        public Store GetById(Guid id)
        {
            return _storeRepository.FindById(id);
        }

        public List<Store> GetByMerchantId(Guid id)
        {
            return _storeRepository.FindByMerchantId(id);
        }

        public IEnumerable<Store> GetByUserId(Guid id)
        {
            return _storeRepository.FindByUserId(id);
        }

        public List<Store> GetAll()
        {
            return _storeRepository.Find();
        }

        public List<ChartView> StatsByStoreProvienceCountReport()
        {
            return _storeRepository.Find().GroupBy(i => i.Provience).Select(m => new ChartView { key = m.Key.Name, value = m.Count() }).ToList();

        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            return _storeRepository.StatsByReviewStatus(userId);
        }

        public long StatsByCount(Guid? userId)
        {
            return _storeRepository.StatsByCount(userId);
        }
    }
}
