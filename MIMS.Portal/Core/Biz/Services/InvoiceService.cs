﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public class InvoiceService : IInvoiceService
    {
        IInvoiceRepository _invoiceRepository;
        public InvoiceService(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        public bool Add(Invoice invoice)
        {
            invoice.SubmitDate = DateTime.Now;
            invoice.PaymentDate = DateTime.Now;
            
            return _invoiceRepository.Add(invoice);
        }

        public bool Delete(string id)
        {
            return _invoiceRepository.Remove(id);
        }

        public List<Invoice> FindByUserId(string id)
        {
            return _invoiceRepository.FindByUserId(id);
        }

        public bool SetSuccessPayment(string orderId)
        {
            Invoice t = _invoiceRepository.FindByOrderId(orderId).First();
            t.PaymentState = "Success";
            return _invoiceRepository.Edit(t);
        }
    }
}
