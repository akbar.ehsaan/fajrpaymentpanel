﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.IServices
{
    public interface IGenericService<T> where T : class
    {
        //bool Create(TEntity entity);

        //bool Update(TEntity entity);

        //bool Delete(Guid id);

        //List<TEntity> Get();

        //List<TEntity> GetById();

        IEnumerable<T> Filter(Expression<Func<T, bool>> predicate = null,
      Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
      Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
      bool enableTracking = true);

    }
}