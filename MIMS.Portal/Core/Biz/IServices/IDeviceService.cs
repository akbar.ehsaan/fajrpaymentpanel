﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IDeviceService
    {
        PosDevice FindById(Guid id);
        List<PosDevice> FindByModelId(Guid id);
        List<PosDevice> FindDevice();
        List<PosDevice> FindDeviceBySerial(string serial);

        bool Add(PosDevice device);
        bool Delete(string id);
        bool Update(PosDevice device);

    }
}
