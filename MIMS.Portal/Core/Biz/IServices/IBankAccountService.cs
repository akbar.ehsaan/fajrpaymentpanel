﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.IServices;
using MIMS.Portal.ValueObjects.Enum;


namespace MIMS.Portal.Core.Biz.IServices
{
    public interface IBankAccountService : IGenericService<BankAccount>
    {
        bool Create(BankAccount bankAccount);

        bool Edit(BankAccount bankAccount);

        bool Delete(Guid id);

        List<BankAccount> GetAll();

        BankAccount GetById(Guid id);

        List<BankAccount> GetByMerchantId(Guid id);

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);

        IEnumerable<BankAccount> GetByUserId(Guid id);


        long StatsByCount(Guid? userId);
    }
}
