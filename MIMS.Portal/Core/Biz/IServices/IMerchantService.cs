﻿using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Biz.IServices
{
    public interface IMerchantService
    {
        bool Create(Merchant merchant);

        bool Edit(Merchant merchant);

        bool Delete(Guid id);

        IEnumerable<Merchant> GetAll();

        Merchant GetById(Guid id);

        Merchant GetByNationalCode(string nationalCode);

        IEnumerable<Merchant> GetByUserId(Guid id);

        IEnumerable<ChartView> MerchantDateCountReport();

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);

        long StatsByCount(Guid? userId);

        double StatsByGain(Guid? userId, DateTime? startDate = null, DateTime? endDate = null);

        IEnumerable<Merchant> Filter(Expression<Func<Merchant, bool>> predicate = null,
            Func<IQueryable<Merchant>, IOrderedQueryable<Merchant>> orderBy = null,
            Func<IQueryable<Merchant>, IIncludableQueryable<Merchant, object>> include = null,
            bool enableTracking = true);
    }
}
