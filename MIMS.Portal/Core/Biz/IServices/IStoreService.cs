﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.IServices;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Biz.IServices
{
    public interface IStoreService : IGenericService<Store>
    {
        bool Create(Store store);

        bool Edit(Store store);

        bool Delete(Guid id);

        List<Store> GetAll();

        Store GetById(Guid id);

        List<Store> GetByMerchantId(Guid id);

        IEnumerable<Store> GetByUserId(Guid id);
        List<ChartView> StatsByStoreProvienceCountReport();

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);

        long StatsByCount(Guid? userId);

    }
}
