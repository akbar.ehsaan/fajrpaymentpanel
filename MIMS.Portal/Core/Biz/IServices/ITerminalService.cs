﻿using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.IServices;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MIMS.Portal.Core.Biz.IServices
{
    public interface ITerminalService : IGenericService<Terminal>
    {
        bool Create(Terminal terminal);

        bool Edit(Terminal terminal);

        bool Delete(Guid id);

        Terminal FindById(Guid id);

        List<Terminal> Find();

        List<Terminal> FindByMerchantId(Guid id);

        IEnumerable<Terminal> GetByUserId(Guid id);

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);
        IEnumerable<KeyValuePair<TerminalStatus, long>> StatsByTerminalStatus(Guid? userId);
        
        IEnumerable<KeyValuePair<string, long>> StatsByStatus(Guid? userId);
        IEnumerable<Terminal> GetByPsp(PspProvider pspProvider , TerminalStatus? terminalStatus);
        IEnumerable<Terminal> GetByTerminalStatus(TerminalStatus status);

        long StatsByCount(Guid? userId);

    }
}
