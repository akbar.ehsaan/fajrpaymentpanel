﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IUsersService
    {
        //List<ChartView> UserMerchantCountDateReport();
        bool Create(User user);
        
        bool Delete(Guid id);
        void UpdatePassword();


        string SignIn(string username, string password);
        
        bool ChangePassword(Guid userId, string oldPassword, string newPassword);
        bool SetPassword(Guid userId, string newPassword);

        List<User> FindByRole(string role);
        User FindById(Guid id);
        List<User> FindUser();

        User FindByNationalCode(string nationalCode);
        User FindByUserName(string userName);

    }
}
