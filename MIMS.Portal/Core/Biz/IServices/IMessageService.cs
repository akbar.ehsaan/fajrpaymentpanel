﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IMessageService
    {
        bool Send(List<string> Numbers, string Text);
    }
}
