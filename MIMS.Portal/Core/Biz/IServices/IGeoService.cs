﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IGeoService
    {
        List<GeoSection> FindById(Guid id);
        
        List<GeoSection> GetSectionsById(Guid? id);

        public List<GeoSection> GetParents();

    }
}
