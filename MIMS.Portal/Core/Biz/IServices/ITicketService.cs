﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface ITicketService
    {
        List<TicketView> FindTicketDetailById(Guid id);
        List<ChartView> TicketDailyReport();
        bool Close(string ticketid);
        List<TicketView> TicketByUserId(string userid);
        bool Add(Ticket ticket);

    }
}
