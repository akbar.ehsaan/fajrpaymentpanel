﻿using Microsoft.AspNetCore.Http;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IFilesManagerService
    {        
        bool Create(UploadFile file);
        bool Delete(Guid id);
        bool SaveFile(IFormFile file, FileSource fileSource, FileType fileType, Guid parentItemId, Guid? childItemId = null);
        void SetStatus(Guid id, ReviewStatus status);

        UploadFile FindById(Guid id);
        List<UploadFile> FindByParentId(Guid parentId);

    }
}
