﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IDepartmentService
    {
        List<Department> Find();
        List<Department> FindById(string id);
    }
}
