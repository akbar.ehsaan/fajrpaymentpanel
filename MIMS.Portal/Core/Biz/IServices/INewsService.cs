﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface INewsService
    {
        bool Add(News entity);
        bool Remove(string id);
        List<News> FindById(string id);
        List<News> FindAll();
    }
}
