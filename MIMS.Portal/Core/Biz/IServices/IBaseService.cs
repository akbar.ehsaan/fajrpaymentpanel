﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Biz.IServices
{
    public interface IBaseService<T> where T : class
    {
        #region Get Functions

        //List<T> GetAll();

        //T GetById(Guid id);

        #endregion

        #region Insert Functions

        void Insert(T entity);

        void Insert(params T[] entities);

        void Insert(IEnumerable<T> entities);

        #endregion
    }
}
