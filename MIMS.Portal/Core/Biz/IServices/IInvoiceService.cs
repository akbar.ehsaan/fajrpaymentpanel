﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IInvoiceService
    {
        List<Invoice> FindByUserId(string id);

        bool Add(Invoice invoice);
        bool Delete(string id);
        bool SetSuccessPayment(string orderId);

    }
}
