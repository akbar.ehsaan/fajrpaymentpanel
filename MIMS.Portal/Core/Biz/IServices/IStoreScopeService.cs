﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IStoreScopeService
    {
        List<BusinessScope> GetAll();
        List<string> MergedScope();

    }
}
