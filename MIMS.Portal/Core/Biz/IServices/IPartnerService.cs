﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IPartnerService
    {
        bool Add(Partner entity);
        bool Remove(string id);
        List<Partner> FindByMerchantId(string merchantid);
    }
}
