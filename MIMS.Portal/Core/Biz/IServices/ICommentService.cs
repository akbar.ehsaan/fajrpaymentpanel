﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface ICommentService
    {
        bool Add(Comment entity);

        List<Comment> FindByMerchantId(string id);
    }
}
