﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IBankService
    {
        bool Add(Bank accountNumber);

        bool Delete(Guid id);

        List<Bank> Find();

        Bank FindById(Guid id);
    }
}
