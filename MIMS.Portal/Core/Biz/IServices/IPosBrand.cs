﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Service
{
    public interface IPosBrandService
    {
        bool Add(PosBrand posBrand);

        bool Delete(Guid id);
        bool Update(PosBrand brand);

        List<PosBrand> Find();
        List<PosBrand> FindByParentId(Guid id);
        List<PosBrand> FindParents();

        PosBrand FindById(Guid id);

    }
}
