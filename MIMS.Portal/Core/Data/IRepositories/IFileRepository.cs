﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IFileRepository
    {
        bool Add(UploadFile entity);
        bool Edit(UploadFile entity);
        bool Remove(Guid id);
        UploadFile FindById(Guid id);
        List<UploadFile> FindByParentId(Guid id);
    }
}
