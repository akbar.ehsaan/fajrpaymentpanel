﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IUserRepository
    {
        List<User> FindByRole(string role);
        bool Add(User entity);
        bool Edit(User entity);
        bool Remove(Guid id);
        User FindById(Guid id);
        List<User> FindByUsernamePassword(string username,string password);
        List<User> Find();

    }
}
