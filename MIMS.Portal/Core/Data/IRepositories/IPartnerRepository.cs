﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IPartnerRepository
    {
        bool Add(Partner entity);
        bool Edit(Partner entity);
        bool Remove(string id);
        List<Partner> FindById(string id);
        List<Partner> FindByMerchantId(string merchantid);

    }
}
