﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.IRepositories
{
    public interface IDeviceRepository
    {
        bool Add(PosDevice entity);
        bool Edit(PosDevice entity);
        bool Remove(string id);
        PosDevice FindById(Guid id);
        List<PosDevice> FindDevice();
        List<PosDevice> FindDeviceBySerial(string serial);

    }
}
