﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IInvoiceRepository
    {
        bool Add(Invoice entity);
        bool Edit(Invoice entity);
        bool Remove(string id);
        List<Invoice> FindById(string id);
        List<Invoice> FindByOrderId(string orderid);

        List<Invoice> FindByUserId(string id);

    }
}
