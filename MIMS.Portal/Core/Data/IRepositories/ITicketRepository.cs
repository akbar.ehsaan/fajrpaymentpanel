﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface ITicketRepository
    {
        bool Add(Ticket entity);
        bool Edit(Ticket entity);
        bool Remove(string id);
        List<Ticket> FindById(string id);
        List<Ticket> FindTicketDetailById(Guid id);

        List<Ticket> TicketByUserId(string userid);

        List<Ticket> Find();

    }
}
