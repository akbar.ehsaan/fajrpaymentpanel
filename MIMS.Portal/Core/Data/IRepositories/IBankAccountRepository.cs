﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.IRepositories
{
    public interface IBankAccountRepository
    {
        bool Add(BankAccount entity);

        bool Edit(BankAccount entity);

        bool Remove(Guid id);

        BankAccount FindById(Guid id);

        List<BankAccount> FindByMerchantId(Guid id);
        List<BankAccount> Find();

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);
        IEnumerable<BankAccount> FindByUserId(Guid id);
        long StatsByCount(Guid? userId);
    }
}
