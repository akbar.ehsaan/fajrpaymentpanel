﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IDepartmentRepository
    {
        List<Department> Find();

        bool Add(Department entity);
        bool Edit(Department entity);
        bool Remove(string id);
        List<Department> FindById(string id);
    }
}
