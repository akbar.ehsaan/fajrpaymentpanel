﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IPosBrandRepository
    {
        bool Add(PosBrand entity);
        bool Edit(PosBrand entity);
        bool Remove(Guid id);

        List<PosBrand> Find();
        List<PosBrand> FindByParentId(Guid id);
        PosBrand FindById(Guid id);
    }
}
