﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface ICommentRepository
    {
        bool Add(Comment entity);

        List<Comment> FindByMerchantId(string id);

    }
}
