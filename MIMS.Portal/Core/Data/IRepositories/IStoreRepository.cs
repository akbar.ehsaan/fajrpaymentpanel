﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IStoreRepository
    {
        bool Add(Store entity);
        bool Edit(Store entity);
        bool Remove(Guid id);
        Store FindById(Guid id);
        List<Store> Find();

        List<Store> FindByMerchantId(Guid id);

        IEnumerable<Store> FindByUserId(Guid id);

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);

        long StatsByCount(Guid? userId);

    }
}
