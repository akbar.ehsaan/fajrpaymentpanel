﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class InvoiceRepository: IInvoiceRepository
    {
        private AppDbContext _ctx;

        public InvoiceRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Invoice entity)
        {
            try
            {
                _ctx.Invoices.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Invoice entity)
        {
            try
            {
                Invoice Edited = _ctx.Invoices.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Invoice> FindById(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.Invoices.Where(i => i.Id == Idg).ToList();
        }

        public List<Invoice> FindByOrderId(string orderid)
        {
            return _ctx.Invoices.Where(i => i.BankOrderId == orderid).ToList();
        }

        public List<Invoice> FindByUserId(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.Invoices.Where(i => i.User.Id == Idg).ToList();
        }

        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                Invoice obj = _ctx.Invoices.Where(i => i.Id == Idg).First();
                _ctx.Invoices.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
