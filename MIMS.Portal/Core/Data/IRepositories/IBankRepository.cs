﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IBankRepository
    {
        bool Add(Bank entity);
        bool Edit(Bank entity);
        bool Remove(Guid id);

        List<Bank> Find();
        Bank FindById(Guid id);
    }
}
