﻿using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.IRepositories
{
    public interface IMerchantRepository : IGenericRepository<Merchant>
    {
        bool Add(Merchant entity);

        bool Update(Merchant entity);

        bool Remove(Guid id);

        IEnumerable<Merchant> Find();

        Merchant FindById(Guid id);

        IEnumerable<Merchant> FindByUserId(Guid id);

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);

        long StatsByCount(Guid? userId);

        double StatsByGain(Guid? userId, DateTime? startDate = null, DateTime? endDate = null);

        IEnumerable<Merchant> Filter(Expression<Func<Merchant, bool>> predicate = null,
            Func<IQueryable<Merchant>, IOrderedQueryable<Merchant>> orderBy = null,
            Func<IQueryable<Merchant>, IIncludableQueryable<Merchant, object>> include = null,
            bool enableTracking = true);
    }
}
