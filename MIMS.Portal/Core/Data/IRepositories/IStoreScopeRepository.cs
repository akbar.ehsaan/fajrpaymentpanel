﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public interface IStoreScopeRepository
    {
        List<BusinessScope> FindById(Guid id);
        List<BusinessScope> GetSectionsById(Guid id);
        List<BusinessScope> GetAll();

    }
}
