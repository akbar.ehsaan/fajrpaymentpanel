﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.IRepositories
{
    public interface IGeoRepository
    {
        bool Add(GeoSection entity);
        bool Edit(GeoSection entity);
        bool Remove(Guid id);
        List<GeoSection> FindById(Guid id);
        List<GeoSection> GetSectionsById(Guid? id);
    }
}
