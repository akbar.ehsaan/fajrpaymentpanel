﻿using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.IRepositories
{
    public interface ITerminalRepository : IGenericRepository<Terminal>
    {
        bool Add(Terminal entity);

        bool Edit(Terminal entity);

        bool Remove(Guid id);

        Terminal FindById(Guid id);

        List<Terminal> Find();

        List<Terminal> FindByMerchantId(Guid id);

        IEnumerable<Terminal> FindByUserId(Guid id);

        IEnumerable<Terminal> FindByTerminalStatus(TerminalStatus status);

        IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId);
        IEnumerable<KeyValuePair<TerminalStatus, long>> StatsByTerminalStatus(Guid? userId);
        IEnumerable<Terminal> FindByPsp(PspProvider pspProvider, TerminalStatus? terminalStatus);


        IEnumerable<KeyValuePair<string, long>> StatsByStatus(Guid? userId);

        long StatsByCount(Guid? userId);

    }
}
