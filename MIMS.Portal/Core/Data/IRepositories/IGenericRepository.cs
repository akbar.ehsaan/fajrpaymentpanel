﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MIMS.Portal.Core.Data.IRepositories

{
    public interface IGenericRepository<T> : IDisposable where T : class, new()
        //public interface IGenericRepository<TEntity> where TEntity : class
    {
        //IQueryable<TEntity> GetAll();

        //Task<TEntity> AddAsync(TEntity entity);

        //Task<TEntity> UpdateAsync(TEntity entity);


        //    T SingleOrDefault(Expression<Func<T, bool>> predicate = null,
        //Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
        //Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
        //bool enableTracking = true,
        //bool ignoreQueryFilters = false);

        //    T Insert(T entity);
        //    void Insert(params T[] entities);
        //    void Insert(IEnumerable<T> entities);

        //    void Update(T entity);
        //    void Update(params T[] entities);
        //    void Update(IEnumerable<T> entities);


        //    void Delete(T entity);

        //    void Delete(params T[] entities);

        //    void Delete(IEnumerable<T> entities);



        IEnumerable<T> Filter(Expression<Func<T, bool>> predicate = null,
    Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
    Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
    bool enableTracking = true);

    }
}