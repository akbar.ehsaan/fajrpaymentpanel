﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MIMS.Portal.ValueObjects.Enum;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Repositories;
using MIMS.Portal.Infrustructure.Repositories;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Core.Data.Repositories
{
    public class TerminalRepository : GenericRepository<Terminal>, ITerminalRepository
    {
        private AppDbContext _dbContext;

        public TerminalRepository(AppDbContext dbContext) :base(dbContext)
        {
            _dbContext = dbContext;
        }
        public bool Add(Terminal entity)
        {
            try
            {
                _dbContext.Terminals.Add(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Terminal entity)
        {
            try
            {
                Terminal Edited = _dbContext.Terminals.Where(i => i.Id == entity.Id).First();
                _dbContext.Entry(Edited).CurrentValues.SetValues(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Terminal FindById(Guid id)
        {
            return _dbContext.Terminals.Include(i => i.Merchant).Include(i => i.BankAccount).Include(i => i.BankAccount.Bank).Include(i => i.Store).Include(i => i.Device).ThenInclude(i => i.PosDeviceModel).ThenInclude(i => i.Parent).Where(i => i.Id == id).FirstOrDefault();
        }
        public List<Terminal> Find()
        {
            var t = _dbContext.Terminals.Include(i => i.Merchant).Include(i => i.BankAccount).Include(i => i.BankAccount.Bank).Include(i => i.Store).ToList();
            return _dbContext.Terminals.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(i => i.Bank).Include(i => i.Store).ThenInclude(i => i.BusinessScope).Include(i => i.Store.BusinessGroup).Include(i => i.Device).ToList();
        }

        public IEnumerable<Terminal> FindByTerminalStatus(TerminalStatus status)
        {
            return _dbContext.Terminals.Where(i => i.TerminalStatus == status).Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(i => i.Bank).Include(i => i.Store).ThenInclude(i => i.BusinessScope).Include(i => i.Store.BusinessGroup).Include(i => i.Device).ThenInclude(i => i.PosDeviceModel).ThenInclude(i => i.Parent).ToList();
        }

        public List<Terminal> FindByMerchantId(Guid id)
        {
            return _dbContext.Terminals.Where(i => i.MerchantId == id).Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(i => i.Bank).Include(i => i.Store).ThenInclude(i => i.BusinessScope).Include(i => i.Store.BusinessGroup).Include(i => i.Device).ThenInclude(i => i.PosDeviceModel).ThenInclude(i => i.Parent).ToList();
        }

        public IEnumerable<Terminal> FindByUserId(Guid id)
        {
            var merchantIds = _dbContext.Merchants.Where(c => c.CreatorUserId == id).Select(c => c.Id);
            return _dbContext.Terminals.Where(i => merchantIds.Contains(i.Merchant.CreatorUserId)).Include(i => i.BankAccount).Include(i => i.Store).Include(i => i.Device).ThenInclude(i => i.PosDeviceModel).ThenInclude(i => i.Parent).ToList();
        }

        public IEnumerable<Terminal> FindByPsp(PspProvider pspProvider, TerminalStatus? terminalStatus)
        {
            var terminals = _dbContext.Terminals.AsQueryable();
            if (terminalStatus != null)
                terminals = terminals.Where(i => i.TerminalStatus == terminalStatus.GetValueOrDefault());

            return terminals.Where(i => i.PspProvider == pspProvider).Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(i => i.Bank).Include(i => i.Store).ThenInclude(i => i.BusinessScope).Include(i => i.Store.BusinessGroup).Include(i => i.Device).ToList();
        }

        public bool Remove(Guid id)
        {
            try
            {
                Terminal obj = _dbContext.Terminals.Where(i => i.Id == id).First();
                _dbContext.Terminals.Remove(obj);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            var terminalsByStatus = new Dictionary<ReviewStatus, long>();
            var terminals = _dbContext.Terminals.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                terminals = terminals.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            terminalsByStatus = terminals.GroupBy(i => i.Status).Select(g => new { g.Key, Value = (long)g.Count() }).ToDictionary(d => d.Key, d => d.Value);

            return terminalsByStatus;
        }

        public IEnumerable<KeyValuePair<TerminalStatus, long>> StatsByTerminalStatus(Guid? userId)
        {
            var terminalsByStatus = new Dictionary<TerminalStatus, long>();
            var terminals = _dbContext.Terminals.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                terminals = terminals.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            terminalsByStatus = terminals.GroupBy(i => i.TerminalStatus).Select(g => new { g.Key, Value = (long)g.Count() }).ToDictionary(d => d.Key, d => d.Value);

            return terminalsByStatus;
        }

        public IEnumerable<KeyValuePair<string, long>> StatsByStatus(Guid? userId)
        {
            var terminalsByStatus = new Dictionary<string, long>();
            var terminals = _dbContext.Terminals.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                terminals = terminals.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            terminalsByStatus = terminals.GroupBy(i => i.Status).ToDictionary(g => g.Key.ToString(), g => (long)g.Count());

            return terminalsByStatus;
        }

        public long StatsByCount(Guid? userId)
        {
            var terminals = _dbContext.Terminals.AsQueryable();
            long count = 0;

            if (userId.GetValueOrDefault() != Guid.Empty)
                terminals = terminals.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            count = terminals.LongCount();

            return count;
        }

    }
}
