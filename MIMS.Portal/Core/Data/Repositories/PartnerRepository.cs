﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class PartnerRepository: IPartnerRepository
    {
        private AppDbContext _ctx;

        public PartnerRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Partner entity)
        {
            try
            {
                _ctx.Partners.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Partner entity)
        {
            try
            {
                Partner Edited = _ctx.Partners.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Partner> FindById(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.Partners.Where(i => i.Id == Idg).ToList();
        }

        public List<Partner> FindByMerchantId(string merchantid)
        {
            Guid Idg = new Guid(merchantid);
            return _ctx.Partners.Where(i => i.Merchant.Id == Idg).ToList();
        }



        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                Partner obj = _ctx.Partners.Where(i => i.Id == Idg).First();
                _ctx.Partners.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
