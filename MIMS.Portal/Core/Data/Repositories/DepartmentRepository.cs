﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class DepartmentRepository: IDepartmentRepository
    {
        private AppDbContext _ctx;

        public DepartmentRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Department entity)
        {
            try
            {
                _ctx.Departments.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Department entity)
        {
            try
            {
                Department Edited = _ctx.Departments.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Department> Find()
        {
           return  _ctx.Departments.ToList();
        }

        public List<Department> FindById(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.Departments.Where(i => i.Id == Idg).ToList();
        }

        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                Department obj = _ctx.Departments.Where(i => i.Id == Idg).First();
                _ctx.Departments.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
