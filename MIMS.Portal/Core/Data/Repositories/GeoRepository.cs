﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class GeoRepository : IGeoRepository
    {
        private AppDbContext _ctx;

        public GeoRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(GeoSection entity)
        {
            try
            {
                _ctx.GeoSections.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(GeoSection entity)
        {
            try
            {
                GeoSection Edited = _ctx.GeoSections.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<GeoSection> FindById(Guid id)
        {
            return _ctx.GeoSections.Where(i => i.Id == id).ToList();
        }

        public List<GeoSection> GetSectionsById(Guid? id)
        {
           return   _ctx.GeoSections.Where(i => i.ParentId == id).ToList();         
        }

        public bool Remove(Guid id)
        {
            try
            {
                GeoSection obj = _ctx.GeoSections.Where(i => i.Id == id).First();
                _ctx.GeoSections.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
