﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private AppDbContext _ctx;

        public UserRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(User entity)
        {
            try
            {
                _ctx.Users.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
         
        }

        public bool Edit(User entity)
        {
            try
            {
                User Edited = _ctx.Users.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<User> Find()
        {
            return _ctx.Users.Include(i => i.Provience).Include(i => i.County).Include(i => i.City).ToList();
        }

        public User FindById(Guid id)
        {
            return _ctx.Users.Where(i => i.Id == id).FirstOrDefault();
        }
        public List<User> FindByRole(string role)
        {
           

            return _ctx.Users.Where(i => i.Role == role).ToList();
        }
        public List<User> FindByUsernamePassword(string username, string password)
        {

            return _ctx.Users.Where(i => i.UserName==username && i.Password==password).ToList();
        }

        public bool Remove(Guid id)
        {
            try
            {
                User obj = _ctx.Users.Where(i => i.Id == id).First();
                _ctx.Users.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
