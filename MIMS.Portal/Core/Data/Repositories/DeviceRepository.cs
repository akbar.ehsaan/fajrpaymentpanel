﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class DeviceRepository : IDeviceRepository
    {
        private AppDbContext _ctx;

        public DeviceRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(PosDevice entity)
        {
            try
            {
                _ctx.PosDevices.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(PosDevice entity)
        {
            try
            {
                PosDevice Edited = _ctx.PosDevices.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public PosDevice FindById(Guid id)
        {

            return _ctx.PosDevices.FirstOrDefault(i => i.Id == id);
        }

        public List<PosDevice> FindDevice()
        {
            return _ctx.PosDevices.ToList();
        }

        public List<PosDevice> FindDeviceBySerial(string serial)
        {
            return _ctx.PosDevices.Where(i => i.DeviceSerial == serial).ToList();
        }

        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                PosDevice obj = _ctx.PosDevices.Where(i => i.Id == Idg).First();
                _ctx.PosDevices.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
