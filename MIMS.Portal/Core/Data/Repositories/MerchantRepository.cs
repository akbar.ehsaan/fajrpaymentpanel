﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Repositories;
using MIMS.Portal.ValueObjects.Enum;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class MerchantRepository : GenericRepository<Merchant>, IMerchantRepository
    {
        private AppDbContext _dbContext;

        public MerchantRepository(AppDbContext dbContext) : base (dbContext)
        {
            _dbContext = dbContext;
        }
        public bool Add(Merchant entity)
        {
            try
            {
                _dbContext.Merchants.Add(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Update(Merchant entity)
        {
            try
            {
                Merchant Edited = _dbContext.Merchants.Where(i => i.Id == entity.Id).First();
                _dbContext.Entry(Edited).CurrentValues.SetValues(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Remove(Guid id)
        {
            try
            {
                Merchant obj = _dbContext.Merchants.SingleOrDefault(i => i.Id == id);
                _dbContext.Merchants.Remove(obj);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<Merchant> Find()
        {
            return _dbContext.Merchants.Include(i => i.Partners).Include(i => i.CreatorUser).Include(i => i.ModifierUser).ToList();
        }

        public Merchant FindById(Guid id)
        {
            return _dbContext.Merchants.Where(i => i.Id == id).Include(i => i.Partners).FirstOrDefault();
        }

        public IEnumerable<Merchant> FindByUserId(Guid id)
        {
            return _dbContext.Merchants.Where(i => i.CreatorUserId == id).Include(i => i.Partners).ToList();
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            var merchantsByStatus = new Dictionary<ReviewStatus, long>();
            var merchants = _dbContext.Merchants.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                merchants = merchants.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            merchantsByStatus = merchants.GroupBy(i => i.Status).Select(g => new { Key = g.Key, Value = (long)g.Count() }).ToDictionary(d => d.Key, d => d.Value);

            return merchantsByStatus;
        }

        public long StatsByCount(Guid? userId)
        {
            var merchants = _dbContext.Merchants.AsQueryable();
            long count = 0;

            if (userId.GetValueOrDefault() != Guid.Empty)
                merchants = merchants.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            count = merchants.LongCount();

            return count;
        }

        public double StatsByGain(Guid? userId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            var merchants = _dbContext.Merchants.AsQueryable();

            var allMerchantsCount = merchants.Count();
            var newMerchantsCount = 0;

            if (startDate == null)
                startDate = DateTime.Now.Date.AddDays(-10);

            if (endDate == null)
                endDate = DateTime.Now.Date;

            var newMerchants = merchants.Where(i => i.CreatedDate.Date >= startDate && i.CreatedDate.Date <= endDate);

            if (userId.GetValueOrDefault() != Guid.Empty)
                newMerchants = newMerchants.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            newMerchantsCount = newMerchants.Count();
            var gainRate = ((double)newMerchantsCount / (double)allMerchantsCount) * 100;

            return gainRate;
        }

    }
}
