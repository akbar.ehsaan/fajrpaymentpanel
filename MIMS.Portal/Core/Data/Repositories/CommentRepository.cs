﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private AppDbContext _ctx;

        public CommentRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Comment entity)
        {
            try
            {
                _ctx.Comments.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Comment> FindByMerchantId(string id)
        {
            Guid gid = new Guid(id);
            return _ctx.Comments.Where(i => i.Merchant.Id == gid).OrderByDescending(i=>i.SubmitDate).ToList();
        }
    }
}
