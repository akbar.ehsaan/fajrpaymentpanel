﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private AppDbContext _ctx;

        public NewsRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(News entity)
        {
            try
            {
                _ctx.News.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<News> FindAll()
        {

            return _ctx.News.OrderByDescending(i=>i.CreatedDate).ToList();
        }

        public List<News> FindById(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.News.Where(i => i.Id == Idg).ToList();
        }

        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                News obj = _ctx.News.Where(i => i.Id == Idg).First();
                _ctx.News.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
