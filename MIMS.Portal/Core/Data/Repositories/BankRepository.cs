﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class BankRepository : IBankRepository
    {
        private AppDbContext _ctx;

        public BankRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public bool Add(Bank entity)
        {
            try
            {
                _ctx.Banks.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Bank entity)
        {
            try
            {
                Bank Edited = _ctx.Banks.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Bank> Find()
        {
            return _ctx.Banks.OrderBy(i=>i.BankName).ToList();
        }

        public Bank FindById(Guid id)
        {

            return _ctx.Banks.Where(i => i.Id == id).FirstOrDefault();
        }


        public bool Remove(Guid id)
        {
            try
            {
                Bank obj = _ctx.Banks.Where(i => i.Id == id).FirstOrDefault();
                _ctx.Banks.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
