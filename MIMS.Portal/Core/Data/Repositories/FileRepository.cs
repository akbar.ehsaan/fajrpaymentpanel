﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class FileRepository : IFileRepository
    {
        private AppDbContext _ctx;

        public FileRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(UploadFile entity)
        {
            try
            {
                _ctx.FilesManager.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(UploadFile entity)
        {
            try
            {
                UploadFile Edited = _ctx.FilesManager.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<UploadFile> FindByParentId(Guid id)
        {

            return _ctx.FilesManager.Where(i => i.ParentItemId == id).ToList();
        }

        public UploadFile FindById(Guid id)
        {

            return _ctx.FilesManager.Where(i => i.Id == id).FirstOrDefault();
        }

      
        public bool Remove(Guid id)
        {
            try
            {
                UploadFile obj = _ctx.FilesManager.Where(i => i.Id == id).First();
                _ctx.FilesManager.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
