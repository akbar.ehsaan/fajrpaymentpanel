﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class StoreScopeRepository : IStoreScopeRepository
    {
        private AppDbContext _ctx;

        public StoreScopeRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        

       
        public List<BusinessScope> FindById(Guid id)
        {
            return _ctx.BusinessScopes.Where(i => i.Id == id).ToList();
        }

        public List<BusinessScope> GetSectionsById(Guid id)
        {
            return _ctx.BusinessScopes.Where(i => i.ParentId == id).ToList();
        }

        public List<BusinessScope> GetAll()
        {
            return _ctx.BusinessScopes.ToList();

        }

       
    }
}
