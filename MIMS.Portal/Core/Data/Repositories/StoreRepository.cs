﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class StoreRepository : IStoreRepository
    {
        private AppDbContext _ctx;

        public StoreRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Store entity)
        {
            try
            {
                _ctx.Stores.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Store entity)
        {
            try
            {
                Store Edited = _ctx.Stores.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Store> Find()
        {
            return _ctx.Stores.Include(i=>i.Merchant).Include(i => i.Provience).Include(i => i.County).Include(i => i.City).Include(i=>i.BusinessGroup).Include(i=>i.BusinessScope).ToList();
        }

        public Store FindById(Guid id)
        {
            return _ctx.Stores.Include(i => i.Merchant).Include(i => i.Provience).Include(i => i.County).Include(i => i.City).Include(i => i.BusinessGroup).Include(i => i.BusinessScope).SingleOrDefault(i => i.Id == id);
        }

        public List<Store> FindByMerchantId(Guid id)
        {
            return _ctx.Stores.Include(i => i.Provience).Include(i => i.County).Include(i => i.City).Include(i => i.BusinessGroup).Include(i => i.BusinessScope).Where(i => i.MerchantId == id).ToList();
        }

        public IEnumerable<Store> FindByUserId(Guid id)
        {
            return _ctx.Stores.Include(i => i.Provience).Include(i => i.County).Include(i => i.City).Include(i => i.BusinessGroup).Include(i => i.BusinessScope).Where(i => i.CreatorUserId == id).ToList();
        }

        public bool Remove(Guid id)
        {
            try
            {
                Store obj = _ctx.Stores.Where(i => i.Id == id).FirstOrDefault();
                _ctx.Stores.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            var storesByStatus = new Dictionary<ReviewStatus, long>();
            var stores = _ctx.Stores.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                stores = stores.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            storesByStatus = stores.GroupBy(i => i.Status).Select(g => new { Key = g.Key, Value = (long)g.Count() }).ToDictionary(d => d.Key, d => d.Value);

            return storesByStatus;
        }

        public long StatsByCount(Guid? userId)
        {
            var stores = _ctx.Stores.AsQueryable();
            long count = 0;

            if (userId.GetValueOrDefault() != Guid.Empty)
                stores = stores.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            count = stores.LongCount();

            return count;
        }
    }
}
