﻿using MIMS.Portal.Core.Domains;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class TicketRepository : ITicketRepository
    {
        private AppDbContext _ctx;

        public TicketRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(Ticket entity)
        {
            try
            {
                _ctx.Tickets.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(Ticket entity)
        {
            try
            {
                Ticket Edited = _ctx.Tickets.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Ticket> Find()
        {
            return _ctx.Tickets.ToList();
        }

        public List<Ticket> FindById(string id)
        {
            Guid Idg = new Guid(id);

            return _ctx.Tickets.Where(i => i.Id == Idg).ToList();
        }

        public List<Ticket> FindTicketDetailById(Guid id)
        {
            return _ctx.Tickets.Include(i => i.User).Where(i => i.Id == id || i.ParentId == id).OrderByDescending(i => i.SubmitDate).ToList();
        }

        public bool Remove(string id)
        {
            try
            {
                Guid Idg = new Guid(id);
                Ticket obj = _ctx.Tickets.Where(i => i.Id == Idg).First();
                _ctx.Tickets.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Ticket> TicketByUserId(string userid)
        {
            Guid gid = new Guid(userid);
            List<Ticket> final = new List<Ticket>();
            List<Ticket> lst = _ctx.Tickets.Include(i => i.User).Where(i => i.SenderUserId == gid || i.ReciverrUserId == gid).ToList();
            foreach (Ticket item in lst)
            {

                if (item.ParentId == new Guid("00000000-0000-0000-0000-000000000000"))
                {
                    if (final.Where(i => i.Id == item.Id).Count() == 0)
                    {
                        final.Add(item);

                    }
                }
                else
                {
                    if (final.Where(i => i.Id == FindById(item.ParentId.ToString()).First().Id).Count() == 0)
                    {
                        final.Add(FindById(item.ParentId.ToString()).First());
                    }
                }

            }

            return final.OrderByDescending(i => i.SubmitDate).ToList();
        }
    }
}
