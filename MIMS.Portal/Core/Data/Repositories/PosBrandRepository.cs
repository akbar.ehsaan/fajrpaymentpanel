﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class PosBrandRepository : IPosBrandRepository
    {
        private AppDbContext _ctx;

        public PosBrandRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public bool Add(PosBrand entity)
        {
            try
            {
                _ctx.PosBrands.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(PosBrand entity)
        {
            try
            {
                PosBrand Edited = _ctx.PosBrands.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<PosBrand> Find()
        {
            return _ctx.PosBrands.OrderBy(i=>i.Name).ToList();
        }
        public List<PosBrand> FindByParentId(Guid id)
        {
            return _ctx.PosBrands.Where(i => i.ParentId == id).ToList();
        }

        public PosBrand FindById(Guid id)
        {

            return _ctx.PosBrands.Where(i => i.Id == id).FirstOrDefault();
        }


        public bool Remove(Guid id)
        {
            try
            {
                PosBrand obj = _ctx.PosBrands.Where(i => i.Id == id).FirstOrDefault();
                _ctx.PosBrands.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
