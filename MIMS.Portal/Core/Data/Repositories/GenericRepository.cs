﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Infrustructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        protected readonly AppDbContext _dbContext;
        protected readonly DbSet<T> _dbSet;

        public GenericRepository(AppDbContext context)
        {
            _dbContext = context ?? throw new ArgumentException(nameof(context));
            _dbSet = _dbContext.Set<T>();
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }


        //public IQueryable<TEntity> GetAll()
        //{
        //    try
        //    {
        //        return _ctx.Set<TEntity>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"Couldn't retrieve entities: {ex.Message}");
        //    }
        //}

        //public async Task<TEntity> AddAsync(TEntity entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
        //    }

        //    try
        //    {
        //        await _ctx.AddAsync(entity);
        //        await _ctx.SaveChangesAsync();

        //        return entity;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
        //    }
        //}

        //public async Task<TEntity> UpdateAsync(TEntity entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
        //    }

        //    try
        //    {
        //        _ctx.Update(entity);
        //        await _ctx.SaveChangesAsync();

        //        return entity;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
        //    }
        //}

        public IEnumerable<T> Filter(Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            bool enableTracking = true)
        {
            IQueryable<T> query = _dbSet;

            if (!enableTracking)
                query = query.AsNoTracking();

            if (include != null)
                query = include(query);

            if (predicate != null)
                query = query.Where(predicate);

            var prop = ((typeof(T))?.BaseType).GetProperty("CreatedDate");
            var orderByDescending = true;
            if (prop != null)
            {

                //var param = Expression.Parameter(typeof(T).BaseType, "f");
                //var parent = Expression.Property(param, "CreatedDate");
                //var sortExpression = Expression.Lambda<Func<T, object>>(parent, param);
                //var sortExpression = Expression.Lambda<Func<T, object>>
                //    (Expression.Convert(Expression.Property(param, "CreatedDate"), typeof(object)), param);
                //var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Property(param, "CreatedDate"), param);

                //var param = Expression.Parameter(typeof(T), "p");
                //var parts = "CreatedDate".Split('.');
                //Expression parent = param;
                //foreach (var part in parts)
                //{
                //    parent = Expression.Property(parent, part);
                //}
                //Expression<Func<T, object>> expression = null;
                //if (parent.Type.IsValueType)
                //{
                //    var converted = Expression.Convert(parent, typeof(object));
                //    expression= Expression.Lambda<Func<T, object>>(converted, param);
                //}
                //else
                //{
                //    expression= Expression.Lambda<Func<T, object>>(parent, param);
                //}

                //var type = typeof(T);
                //var parameterExpression = Expression.Parameter(type, "x");
                //var body = Expression.PropertyOrField(parameterExpression, "CreatedDate");
                //var convertedBody = Expression.MakeUnary(ExpressionType.Convert, body, typeof(object));
                ////var expression = Expression.Lambda<Func<T, object>>(convertedBody, new[] { parameterExpression });


                //if (orderByDescending)
                //    query = query.OrderByDescending(expression);
                //else
                //    query = query.OrderBy(expression);
            }
            //query = query.OrderByDescending(r => r.GetType().BaseType.GetProperty("CreatedDate"));

            query = query.Take(500);

            if (orderBy != null)
                return orderBy(query);

            return query;
        }
    }
}