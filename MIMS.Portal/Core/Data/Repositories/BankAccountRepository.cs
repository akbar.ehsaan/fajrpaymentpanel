﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Core.Data.IRepositories;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class BankAccountRepository : IBankAccountRepository
    {
        private AppDbContext _ctx;

        public BankAccountRepository(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        public bool Add(BankAccount entity)
        {
            try
            {
                _ctx.BankAccounts.Add(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool Edit(BankAccount entity)
        {
            try
            {
                BankAccount Edited = _ctx.BankAccounts.Where(i => i.Id == entity.Id).First();
                _ctx.Entry(Edited).CurrentValues.SetValues(entity);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public BankAccount FindById(Guid id)
        {

            return _ctx.BankAccounts.Include(u => u.Merchant).Include(u => u.Bank).Where(i => i.Id == id).FirstOrDefault();
        }

        public List<BankAccount> FindByMerchantId(Guid id)
        {
            return _ctx.BankAccounts.Include(u => u.Bank).Where(i => i.MerchantId == id).ToList();
        }
        public List<BankAccount> Find()
        {
            return _ctx.BankAccounts.Include(u => u.Merchant).Include(u => u.Bank).ToList();
        }
        public bool Remove(Guid id)
        {
            try
            {
                BankAccount obj = _ctx.BankAccounts.Where(i => i.Id == id).FirstOrDefault();
                _ctx.BankAccounts.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public IEnumerable<KeyValuePair<ReviewStatus, long>> StatsByReviewStatus(Guid? userId)
        {
            var bankAccountsByStatus = new Dictionary<ReviewStatus, long>();
            var bankAccounts = _ctx.BankAccounts.AsQueryable();

            if (userId.GetValueOrDefault() != Guid.Empty)
                bankAccounts = bankAccounts.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            bankAccountsByStatus = bankAccounts.GroupBy(i => i.Status).Select(g => new { Key = g.Key, Value = (long)g.Count() }).ToDictionary(d => d.Key, d => d.Value);

            return bankAccountsByStatus;
        }

        public IEnumerable<BankAccount> FindByUserId(Guid id)
        {
            return _ctx.BankAccounts.Include(u => u.Bank).Where(i => i.CreatorUserId == id).ToList();
        }

        public long StatsByCount(Guid? userId)
        {
            var bankAccounts = _ctx.BankAccounts.AsQueryable();
            long count = 0;

            if (userId.GetValueOrDefault() != Guid.Empty)
                bankAccounts = bankAccounts.Where(i => i.CreatorUserId == userId.GetValueOrDefault());

            count = bankAccounts.LongCount();

            return count;
        }
    }
}
