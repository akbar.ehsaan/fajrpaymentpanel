﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Domains
{
    public class Invoice
    {
        public Guid Id { set; get; }
        public DateTime SubmitDate { set; get; }
        public long Amount { set; get; }
        public string TrackBankId { set; get; }
        public string PaymentState { set; get; }
        public string BankOrderId { set; get; }
        public DateTime PaymentDate { set; get; }


        public User User { set; get; }
    }
}
