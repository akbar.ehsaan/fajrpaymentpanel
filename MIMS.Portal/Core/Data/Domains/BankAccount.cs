﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MIMS.Portal.ValueObjects.Enum;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("BankAccounts")]
    public class BankAccount : BaseEntity
    {
        [ForeignKey(nameof(Merchant))]
        public Guid MerchantId { get; set; }
        public Merchant Merchant { set; get; }

        [ForeignKey(nameof(Bank))]
        public Guid BankId { get; set; }
        public Bank Bank { get; set; }

        public string BankBranchCode { set; get; }

        public string AccountNumber { get; set; }

        public string IbanNumber { set; get; }

        public string AccountHolderFirstName { set; get; }

        public string AccountHolderLastName { set; get; }

        public string AccountHolderFullName { get { return $"{AccountHolderFirstName} {AccountHolderLastName}"; } }

        [Column(TypeName = "nvarchar(50)")]
        public ReviewStatus Status { set; get; }

        public string ReviewComment { set; get; }

        //
        // Related Collections
        //
        //
        public virtual ICollection<Terminal> Terminals { set; get; }
    }
}
