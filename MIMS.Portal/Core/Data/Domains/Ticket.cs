﻿using MIMS.Portal.Core.Data.Domains;
using System;

namespace MIMS.Portal.Core.Domains
{
    public class Ticket
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string TicketStatus { get; set; }
        public Guid SenderUserId { get; set; }
        public Guid ReciverrUserId { get; set; }
        public DateTime SubmitDate { get; set; }

        public User User { get; set; }

    }
}
