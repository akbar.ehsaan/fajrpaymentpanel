﻿using System;

namespace MIMS.Portal.Core.Data.Domains
{
    public class Bank
    {
        public Guid Id { set; get; }

        public DateTime SubmitDate { set; get; }

        public string BankName { set; get; }

        public string IbanCode { set; get; }
    }
}
