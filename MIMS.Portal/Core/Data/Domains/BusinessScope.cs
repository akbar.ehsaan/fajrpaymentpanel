﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Data.Domains
{
    public class BusinessScope
    {
        public Guid Id { set; get; }

        [ForeignKey(nameof(BusinessScope))]
        public Guid? ParentId { set; get; }
        public BusinessScope Parent { set; get; }

        public string Name { set; get; }

        //
        // Related Collections
        //

        [NotMapped]
        public virtual ICollection<Store> Stores { set; get; }
    }
}
