﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("GeoSections")]
    public class GeoSection
    {
        public Guid Id { set; get; }
        public string Name { set; get; }

        public Guid? ParentId { set; get; }

        public string Type { set; get; }
    }
}
