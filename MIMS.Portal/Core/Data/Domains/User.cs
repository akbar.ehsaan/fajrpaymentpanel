﻿using Microsoft.EntityFrameworkCore;
using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("Users")]
    [Index(nameof(UserName), IsUnique = true)]
    [Index(nameof(Email), IsUnique = true)]
    [Index(nameof(Mobile), IsUnique = true)]
    [Index(nameof(NationalCode), IsUnique = true)]
    public class User : BaseEntity
    {
        [StringLength(50)]
        public string UserName { set; get; }

        public string Password { set; get; }

        public string Salt { set; get; }

        public string Role { set; get; }

        public string Permission { set; get; }

        public string FirstName { set; get; }

        public string LastName { set; get; }

        public string Fullname
        {
            get { return $"{FirstName} {LastName}"; }
        }

        [StringLength(10)]
        public string NationalCode { set; get; }

        public DateTime? Birthdate { get; set; }

        [StringLength(50)]
        public string Email { set; get; }

        [StringLength(50)]
        public string Mobile { set; get; }

        public string Phone { set; get; }

        public string PostalCode { set; get; }

        public string Address { set; get; }

        [ForeignKey(nameof(Provience))]
        public Guid? ProvienceId { get; set; }
        public virtual GeoSection Provience { set; get; }

        [ForeignKey(nameof(County))]
        public Guid? CountyId { get; set; }
        public virtual GeoSection County { set; get; }

        [ForeignKey(nameof(City))]
        public Guid? CityId { get; set; }
        public virtual GeoSection City { set; get; }

        public bool IsEmailConfirmed { get; set; }

        public bool IsMobileConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public string Status { set; get; }

        //
        // Related Collections
        // 

        public virtual ICollection<Invoice> Invoices { set; get; }
        public virtual ICollection<Ticket> Tickets { set; get; }
    }
}
