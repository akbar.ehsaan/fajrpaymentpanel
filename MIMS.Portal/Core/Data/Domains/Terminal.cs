﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("Terminals")]
    public class Terminal : BaseEntity
    {
        [Column(TypeName = "nvarchar(50)")]
        public TerminalType Type { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public PspProvider? PspProvider { set; get; }

        [ForeignKey(nameof(Merchant))]
        public Guid MerchantId { get; set; }
        public virtual Merchant Merchant { set; get; }

        [ForeignKey(nameof(BankAccount))]
        public Guid AccountNumberId { get; set; }
        public virtual BankAccount BankAccount { set; get; }

        [ForeignKey(nameof(Store))]
        public Guid StoreId { get; set; }
        public virtual Store Store { set; get; }

        public string TerminalNumber { set; get; }

        public string MerchantNumber { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public PosConnectionType ConnectionType { set; get; }

        [ForeignKey(nameof(Brand))]
        public Guid? PosDeviceBrandId { get; set; }
        public virtual PosBrand Brand { set; get; }

        [ForeignKey(nameof(Model))]
        public Guid? PosDeviceModelId { get; set; }
        public virtual PosBrand Model { set; get; }

        [ForeignKey(nameof(Device))]
        public Guid? PosDeviceId { get; set; }
        public virtual PosDevice Device { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public ReviewStatus Status { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public TerminalStatus TerminalStatus { set; get; }

        public string ReviewComment { set; get; }

        public string MerchantTrackingCode { set; get; }

        public string StoreTrackingCode { set; get; }

        public DateTime? SubmittedDate { get; set; }

        public DateTime? AssignedDate { get; set; }

        public DateTime? DeliveredDate { get; set; }

        public DateTime? CanceledDate { get; set; }
    }
}
