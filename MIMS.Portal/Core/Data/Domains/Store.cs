﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("Stores")]
    public class Store : BaseEntity
    {
        [ForeignKey(nameof(Merchant))]
        public Guid MerchantId { get; set; }
        public Merchant Merchant { set; get; }

        public string StoreName { set; get; }

        public string EnglishStoreName { set; get; }

        [ForeignKey(nameof(BusinessGroup))]
        public Guid BusinessGroupId { get; set; }
        public BusinessScope BusinessGroup { set; get; }

        [ForeignKey(nameof(BusinessScope))]
        public Guid BusinessScopeId { get; set; }
        public BusinessScope BusinessScope { set; get; }

        public string Mobile { set; get; }

        public string Phone { set; get; }

        [ForeignKey(nameof(Provience))]
        public Guid ProvienceId { get; set; }
        public virtual GeoSection Provience { set; get; }

        [ForeignKey(nameof(County))]
        public Guid CountyId { get; set; }
        public virtual GeoSection County { set; get; }

        [ForeignKey(nameof(City))]
        public Guid CityId { get; set; }
        public virtual GeoSection City { set; get; }

        public string PostalCode { set; get; }

        public string Address { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public ResidenceType ResidenceType { set; get; }

        public string DocumentNumber { set; get; }

        public DateTime? DocumentStartDate { set; get; }

        public DateTime? DocumentEndDate { set; get; }

        public string EconomicCode { set; get; }

        public string TaxRegistrationTrackingCode { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public ReviewStatus Status { set; get; }

        public string ReviewComment { set; get; }

        //
        // Related Collections
        // 

        public virtual ICollection<Terminal> Terminals { set; get; }
    }
}
