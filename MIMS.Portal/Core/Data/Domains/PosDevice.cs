﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("PosDevices")]
    public class PosDevice : BaseEntity
    {
        [ForeignKey(nameof(PosDeviceBrand))]
        public Guid PosDeviceBrandId { get; set; }
        public virtual PosBrand PosDeviceBrand { get; set; }

        [ForeignKey(nameof(PosDeviceModel))]
        public Guid PosDeviceModelId { get; set; }
        public virtual PosBrand PosDeviceModel { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public PosConnectionType DeviceConnectionType { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public PosOSType DeviceOSType { set; get; }

        public string DeviceSerial { set; get; }

        public string FirmwareVersion { set; get; }

        public string AppVersion { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public PosOwnerType DeviceOwnerType { set; get; }

        [ForeignKey(nameof(User))]
        public Guid SupervisorUserId { get; set; }
        public virtual User User { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public PosStatus DeviceStatus { set; get; }

        //[ForeignKey(nameof(Terminal))]
        //public Guid? TerminalId { get; set; }
        //public virtual Terminal Terminal { get; set; }

        //
        // Related Collections
        // 

        //public ICollection<Terminal> Terminals { set; get; }
    }
}
