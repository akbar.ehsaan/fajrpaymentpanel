﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Domains
{
    [Table("News")]
    public class News : BaseEntity
    {
        public string Subject { get; set; }
        
        public string Body { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public NewsType Type { set; get; }
    }
}
