﻿using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Domains
{
    [Table("FilesManager")]
    public class UploadFile : BaseEntity
    {
        [Column(TypeName = "nvarchar(50)")]
        public FileSource Source { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public FileType Type { set; get; }

        public Guid ParentItemId { set; get; }

        public string FileName { set; get; }

        public string Path { set; get; }

        public string FileUrl
        {
            get
            {
                var subRoot = Source == FileSource.Ticket ? "Tickets" : "Merchats";
                var url = $"~/Attachments/{subRoot}/{ParentItemId}/{Source}/{FileName}";
                return url;

            }
        }

        [Column(TypeName = "nvarchar(50)")]
        public ReviewStatus Status { set; get; } = ReviewStatus.Pending;
    }
}
