﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Domains
{
    [Table("PosBrands")]
    public class PosBrand : BaseEntity
    {
        public string Name { set; get; }

        [ForeignKey(nameof(Parent))]
        public Guid? ParentId { set; get; }
        public PosBrand Parent { set; get; }
    }
}
