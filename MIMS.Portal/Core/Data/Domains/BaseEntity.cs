﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Core.Data.Domains
{
    public class BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Column(Order = 100)]
        public string SystemNote { get; set; }

        [Column(Order = 101)]
        public DateTime CreatedDate { get; set; }

        [Column(Order = 102)]
        [ForeignKey(nameof(CreatorUser))]
        public Guid CreatorUserId { get; set; }
        public virtual User CreatorUser { get; set; }

        [Column(Order = 102)]
        public string CreatedBy { get; set; }

        [Column(Order = 103)]
        public DateTime ModifiedDate { get; set; }

        [Column(Order = 104)]
        [ForeignKey(nameof(ModifierUser))]
        public Guid ModifierUserId { get; set; }
        public virtual User ModifierUser { get; set; }

        [Column(Order = 105)]
        public string ModifiedBy { get; set; }


        //public BaseEntity()
        //{
        //    this.Id = Guid.NewGuid();
        //}
    }
}
