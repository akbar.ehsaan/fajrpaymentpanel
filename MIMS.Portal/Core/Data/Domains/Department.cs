﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Domains
{
    public class Department
    {
        public Guid Id { set; get; }
        public DateTime SubmitDate { set; get; }

        public string DepartmantName { get; private set; }
        public Guid? ParentId { set; get; }

    }
}
