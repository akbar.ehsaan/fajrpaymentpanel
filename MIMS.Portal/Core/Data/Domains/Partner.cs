﻿using MIMS.Portal.Core.Data.Domains;
using System;

namespace MIMS.Portal.Core.Domains
{
    public class Partner
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Family { set; get; }
        public string EnglishName { set; get; }
        public string EnglishFamily { set; get; }
        public string NationalCode { set; get; }
        public string BirthPlace { set; get; }
        public string Gender { set; get; }
        public string IdentityNumber { set; get; }
        public string FatherName { set; get; }
        public string EnglishFatherName { set; get; }
        public string PostalCode { set; get; }

        public string Mobile { set; get; }
        public string Telephone { set; get; }
        public Merchant Merchant { set; get; }

    }
}
