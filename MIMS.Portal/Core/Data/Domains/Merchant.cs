﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;

namespace MIMS.Portal.Core.Data.Domains
{
    [Table("Merchants")]
    public class Merchant : BaseEntity
    {
        [Column(TypeName = "nvarchar(50)")]
        public PersonType Type { set; get; }

        public string FirstName { set; get; }

        public string LastName { set; get; }

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        public string FatherName { set; get; }

        public string EnglishFirstName { set; get; }

        public string EnglishLastName { set; get; }

        public string EnglishFatherName { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public Gender Gender { set; get; }

        public string NationalCode { set; get; }

        [DataType(DataType.Date)]
        public DateTime BirthDate { set; get; }

        public string BirthCertificateNumber { set; get; }

        public string CompanyName { set; get; }

        public string EnglishCompanyName { set; get; }

        public string CompanyNationalID { set; get; }

        public string CompanyRegisterCode { set; get; }

        public string CompanyEconomicCode { set; get; }

        [DataType(DataType.Date)]
        public DateTime? CompanyRegisterDate { set; get; }

        public string Mobile { set; get; }

        public string Phone { set; get; }

        public string Fax { set; get; }

        public string Email { set; get; }

        [ForeignKey(nameof(Provience))]
        public Guid? ProvienceId { get; set; }
        public virtual GeoSection Provience { set; get; }

        [ForeignKey(nameof(County))]
        public Guid? CountyId { get; set; }
        public virtual GeoSection County { set; get; }

        [ForeignKey(nameof(City))]
        public Guid? CityId { get; set; }
        public virtual GeoSection City { set; get; }

        public string PostalCode { set; get; }

        public string Address { set; get; }

        public string MarketerCode { set; get; }

        public string ReferrerFirstName { set; get; }

        public string ReferrerLastName { set; get; }

        public string ReferrerFullName { get { return $"{ReferrerFirstName} {ReferrerLastName}"; } }

        public string ReferrerNationalCode { set; get; }

        public string ReferrerMobile { set; get; }

        public string ReferrerPhone { set; get; }

        [Column(TypeName = "nvarchar(50)")]
        public ReviewStatus Status { set; get; }

        public string ReviewComment { set; get; }

        //
        // Related Collections
        // 

        public ICollection<Partner> Partners { set; get; }

        public ICollection<BankAccount> AccountNumbers { set; get; }

        public ICollection<Store> Stores { set; get; }

        public ICollection<Terminal> Terminals { set; get; }

        public ICollection<Comment> Comments { set; get; }
    }
}
