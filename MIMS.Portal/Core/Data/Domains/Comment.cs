﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Core.Domains
{
    public class Comment
    {
        public Guid Id { get; set; }
        public Merchant Merchant { get; set; }
        public string Body { get; set; }
        public string UserSubmitName { get; set; }
        public DateTime SubmitDate { get; set; }
    }
}
