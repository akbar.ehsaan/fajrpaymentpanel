﻿using MIMS.Portal.Core.Domains;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Threading;
using System.Security.Claims;
using MIMS.Portal.ViewModels;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Infrustructure.Repositories
{
    public class AppDbContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AppDbContext(DbContextOptions<AppDbContext> options, IHttpContextAccessor httpContextAccessor)
          : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Merchant> Merchants { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Terminal> Terminals { get; set; }
        public virtual DbSet<PosDevice> PosDevices { get; set; }
        public virtual DbSet<UploadFile> FilesManager { get; set; }

        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }

        //--- Base Data ---//
        public virtual DbSet<GeoSection> GeoSections { get; set; }

        public virtual DbSet<Bank> Banks { get; set; }

        public virtual DbSet<BusinessScope> BusinessScopes { get; set; }

        public virtual DbSet<PosBrand> PosBrands { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return (await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken));
        }

        private void OnBeforeSaving()
        {
            //var identityName = Thread.CurrentPrincipal.Identity.Name;
            var userClaims = _httpContextAccessor?.HttpContext.User.Identity as ClaimsIdentity;
            var userId = new Guid(userClaims.FindFirst("userId").Value);
            var userName = userClaims.FindFirst("userName").Value;
            var entries = ChangeTracker.Entries();
            var timeStamp = DateTime.Now;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                if (entry.Entity is BaseEntity trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            // set the updated date to "now"
                            trackable.ModifiedDate = timeStamp;
                            trackable.ModifierUserId = userId;
                            trackable.ModifiedBy = userName;

                            // mark property as "don't touch"
                            // we don't want to update on a Modify operation
                            entry.Property("CreatedDate").IsModified = false;
                            entry.Property("CreatorUserId").IsModified = false;
                            entry.Property("CreatedBy").IsModified = false;
                            break;

                        case EntityState.Added:
                            // set both updated and created date to "now"
                            trackable.CreatedDate = timeStamp;
                            trackable.CreatorUserId = userId;
                            trackable.CreatedBy = userName;
                            trackable.ModifiedDate = timeStamp;
                            trackable.ModifierUserId = userId;
                            trackable.ModifiedBy = userName;
                            break;
                    }
                }
            }
        }
    }
}
