﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DestinationId",
                table: "FilesManager",
                newName: "ParentItemId");

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "FilesManager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "FilesManager",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "FilesManager",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "FilesManager");

            migrationBuilder.DropColumn(
                name: "Source",
                table: "FilesManager");

            migrationBuilder.RenameColumn(
                name: "ParentItemId",
                table: "FilesManager",
                newName: "DestinationId");

            migrationBuilder.DropColumn(
              name: "Status",
              table: "FilesManager");
        }
    }
}
