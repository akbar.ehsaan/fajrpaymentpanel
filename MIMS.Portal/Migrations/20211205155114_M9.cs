﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                  name: "PBirthDate",
                  table: "Merchants");

            migrationBuilder.AddColumn<DateTime>(
                name: "AssignedDate",
                table: "Terminals",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CanceledDate",
                table: "Terminals",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeliveredDate",
                table: "Terminals",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MerchantTrackingCode",
                table: "Terminals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StoreTrackingCode",
                table: "Terminals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SubmittedDate",
                table: "Terminals",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TerminalStatus",
                table: "Terminals",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AppVersion",
                table: "PosDevices",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirmwareVersion",
                table: "PosDevices",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CityId",
                table: "Merchants",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CountyId",
                table: "Merchants",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProvienceId",
                table: "Merchants",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Merchants_CityId",
                table: "Merchants",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Merchants_CountyId",
                table: "Merchants",
                column: "CountyId");

            migrationBuilder.CreateIndex(
                name: "IX_Merchants_ProvienceId",
                table: "Merchants",
                column: "ProvienceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Merchants_GeoSections_CityId",
                table: "Merchants",
                column: "CityId",
                principalTable: "GeoSections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Merchants_GeoSections_CountyId",
                table: "Merchants",
                column: "CountyId",
                principalTable: "GeoSections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Merchants_GeoSections_ProvienceId",
                table: "Merchants",
                column: "ProvienceId",
                principalTable: "GeoSections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
               name: "PBirthDate",
               table: "Merchants",
               type: "nvarchar(max)",
               nullable: true);

            migrationBuilder.DropColumn(
                name: "AssignedDate",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "CanceledDate",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "DeliveredDate",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "MerchantTrackingCode",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "StoreTrackingCode",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "SubmittedDate",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "TerminalStatus",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "AppVersion",
                table: "PosDevices");

            migrationBuilder.DropColumn(
                name: "FirmwareVersion",
                table: "PosDevices");

            migrationBuilder.DropForeignKey(
                name: "FK_Merchants_GeoSections_CityId",
                table: "Merchants");

            migrationBuilder.DropForeignKey(
                name: "FK_Merchants_GeoSections_CountyId",
                table: "Merchants");

            migrationBuilder.DropForeignKey(
                name: "FK_Merchants_GeoSections_ProvienceId",
                table: "Merchants");

            migrationBuilder.DropIndex(
                name: "IX_Merchants_CityId",
                table: "Merchants");

            migrationBuilder.DropIndex(
                name: "IX_Merchants_CountyId",
                table: "Merchants");

            migrationBuilder.DropIndex(
                name: "IX_Merchants_ProvienceId",
                table: "Merchants");

            migrationBuilder.DropColumn(
                name: "CityId",
                table: "Merchants");

            migrationBuilder.DropColumn(
                name: "CountyId",
                table: "Merchants");

            migrationBuilder.DropColumn(
                name: "ProvienceId",
                table: "Merchants");
        }
    }
}
