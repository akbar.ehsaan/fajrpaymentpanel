﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PosDevices_Terminals_TerminalId",
                table: "PosDevices");

            migrationBuilder.DropIndex(
                name: "IX_PosDevices_TerminalId",
                table: "PosDevices");

            migrationBuilder.DropColumn(
                name: "TerminalId",
                table: "PosDevices");

            migrationBuilder.AddColumn<Guid>(
                name: "BusinessGroupId",
                table: "Stores",
                type: "uniqueidentifier",
                nullable: true,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Stores_BusinessGroupId",
                table: "Stores",
                column: "BusinessGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessScopes_ParentId",
                table: "BusinessScopes",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessScopes_BusinessScopes_ParentId",
                table: "BusinessScopes",
                column: "ParentId",
                principalTable: "BusinessScopes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_BusinessScopes_BusinessGroupId",
                table: "Stores",
                column: "BusinessGroupId",
                principalTable: "BusinessScopes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessScopes_BusinessScopes_ParentId",
                table: "BusinessScopes");

            migrationBuilder.DropForeignKey(
                name: "FK_Stores_BusinessScopes_BusinessGroupId",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_BusinessGroupId",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_BusinessScopes_ParentId",
                table: "BusinessScopes");

            migrationBuilder.DropColumn(
                name: "BusinessGroupId",
                table: "Stores");

            migrationBuilder.AddColumn<Guid>(
                name: "TerminalId",
                table: "PosDevices",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PosDevices_TerminalId",
                table: "PosDevices",
                column: "TerminalId",
                unique: true,
                filter: "[TerminalId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_PosDevices_Terminals_TerminalId",
                table: "PosDevices",
                column: "TerminalId",
                principalTable: "Terminals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
