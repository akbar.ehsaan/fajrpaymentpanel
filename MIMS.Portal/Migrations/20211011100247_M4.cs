﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatorUserId",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true
                //defaultValue: new Guid("00000000-0000-0000-0000-000000000000")
                );

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifierUserId",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true
                //defaultValue: new Guid("00000000-0000-0000-0000-000000000000")
                );

            migrationBuilder.AddColumn<string>(
                name: "SystemNote",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

           
            migrationBuilder.CreateIndex(
                name: "IX_Users_CreatorUserId",
                table: "Users",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ModifierUserId",
                table: "Users",
                column: "ModifierUserId");


            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_CreatorUserId",
                table: "Users",
                column: "CreatorUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_ModifierUserId",
                table: "Users",
                column: "ModifierUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_CreatorUserId",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_ModifierUserId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_CreatorUserId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_ModifierUserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ModifierUserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SystemNote",
                table: "Users");
        }
    }
}
