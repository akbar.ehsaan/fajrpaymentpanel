﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferrerFullname",
                table: "Merchants");

            migrationBuilder.DropColumn(
                name: "AccountHolderFullName",
                table: "BankAccounts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReferrerFullname",
                table: "Merchants",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountHolderFullName",
                table: "BankAccounts",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
