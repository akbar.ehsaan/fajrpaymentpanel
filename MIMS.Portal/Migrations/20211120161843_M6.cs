﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MIMS.Portal.Migrations
{
    public partial class M6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SubmitDate",
                table: "News",
                newName: "ModifiedDate");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "News",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "News",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "CreatorUserId",
                table: "News",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "News",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifierUserId",
                table: "News",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "SystemNote",
                table: "News",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "News",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_News_CreatorUserId",
                table: "News",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_News_ModifierUserId",
                table: "News",
                column: "ModifierUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_News_Users_CreatorUserId",
                table: "News",
                column: "CreatorUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_News_Users_ModifierUserId",
                table: "News",
                column: "ModifierUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_News_Users_CreatorUserId",
                table: "News");

            migrationBuilder.DropForeignKey(
                name: "FK_News_Users_ModifierUserId",
                table: "News");

            migrationBuilder.DropIndex(
                name: "IX_News_CreatorUserId",
                table: "News");

            migrationBuilder.DropIndex(
                name: "IX_News_ModifierUserId",
                table: "News");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "News");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "News");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "News");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "News");

            migrationBuilder.DropColumn(
                name: "ModifierUserId",
                table: "News");

            migrationBuilder.DropColumn(
                name: "SystemNote",
                table: "News");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "News");

            migrationBuilder.RenameColumn(
                name: "ModifiedDate",
                table: "News",
                newName: "SubmitDate");
        }
    }
}
