﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace MIMS.Portal.Commons.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)

        {
            var accessToken = httpContext.Request.Cookies["AccessToken"];
            //httpContext.Response.Redirect("/home/Login");

            //// Redirect to login if user is not authenticated. This instruction is neccessary for JS async calls, otherwise everycall will return unauthorized without explaining why
            //if (!httpContext.User.Identity.IsAuthenticated && httpContext.Request.Path.Value != "/Account/Login")
            //{
            //    httpContext.Response.Redirect("/home/Login");
            //}

            //// Move forward into the pipeline
            //await _next(httpContext);

            //if (accessToken == null
            //    &&
            //    !string.Equals(httpContext.Request.Path.Value, "/home/Login", StringComparison.OrdinalIgnoreCase))
            //{
            //    httpContext.Response.Redirect("/home/Login");
            //}


            if (!string.IsNullOrEmpty(accessToken))
            {
                httpContext.Request.Headers.Add("Authorization", "Bearer " + accessToken);
            }
            await _next(httpContext);

        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthMiddleware>();
        }
    }
}
