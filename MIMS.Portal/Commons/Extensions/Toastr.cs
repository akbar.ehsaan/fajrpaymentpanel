using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace MIMS.Portal.Commons.Extensions
{
    public static class Toastrs
    {
        public static List<Toastr> ToastrsList { get; set; }

        static Toastrs()
        {
            ToastrsList = new List<Toastr> { };
        }

        public static void Add(ToastrType type, string subject, string content)
        {
            ToastrsList.Add(new Toastr { Type = type, Subject = subject, Content = content });
        }

        public static string GetList()
        {
            return JsonConvert.SerializeObject(ToastrsList);
        }

        public static string FetchList()
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var toastrsList = JsonConvert.SerializeObject(ToastrsList);
            ToastrsList.Clear();
            return toastrsList;
        }
    }

    public class Toastr
    {
        public ToastrType Type { get; set; }
        public string Style { get { return Enum.GetName(typeof(ToastrType), Type).ToLower(); } }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
    public enum ToastrType
    {
        Success,
        Error,
        Warning,
        Info
    }


}
