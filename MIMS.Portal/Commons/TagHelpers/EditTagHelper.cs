﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIMS.Portal.Infrastructure.TagHelpers
{


    [HtmlTargetElement("edit")]
    public class EditTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-model")]
        public ModelExpression AspModel { get; set; }

        [HtmlAttributeName("asp-property")]
        public ModelExpression AspProperty { get; set; }

        [HtmlAttributeName("asp-item")]
        public ModelExpression AspItem { get; set; }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        protected IHtmlGenerator _generator { get; set; }

        public EditTagHelper(IHtmlGenerator generator)
        {
            _generator = generator;
        }

        private static string _sectionHeader { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var aspModel = AspModel;
            var aspProp = AspProperty;
            var aspItem = AspItem;
            var propertyName = AspProperty.Model.ToString();


            var modelExProp =
               AspModel.ModelExplorer.Properties.Single(x => x.Metadata.PropertyName.Equals(propertyName));
            //AspModel.ModelExplorer.GetExplorerForProperty(propName);

            var currentSection = "";
            var propertyType = modelExProp.Metadata.ModelType;
            var propertyValue = modelExProp.Model;
            var propertyDisplayName = modelExProp.Metadata.DisplayName;
            var propertyEditFormatString = modelExProp.Metadata.EditFormatString;
            var propertyAttributes = ((DefaultModelMetadata)modelExProp.Metadata).Attributes.PropertyAttributes;
            var propertyDisplayAttribute = propertyAttributes.OfType<DisplayAttribute>().FirstOrDefault();
            var propertyForeignKey = propertyAttributes.OfType<ForeignKeyAttribute>().FirstOrDefault();


            if (propertyDisplayAttribute != null)
            {
                if (propertyDisplayAttribute.GroupName != null)
                { currentSection = propertyDisplayAttribute.GroupName; }
            }

            if (propertyDisplayAttribute?.GetOrder().GetValueOrDefault(0) >= 0)
            {


                var input = new TagBuilder(" ");
                if (!string.IsNullOrWhiteSpace(currentSection) && currentSection != _sectionHeader)
                {
                    var groupHeader = string.Format("<h4 class=\"form-section\"><i class=\"icon-head\"></i> {0} </h4>", currentSection);
                    _sectionHeader = currentSection;
                }


                if (typeof(string).IsAssignableFrom(propertyType) || typeof(int).IsAssignableFrom(propertyType) || typeof(decimal).IsAssignableFrom(propertyType) || typeof(double).IsAssignableFrom(propertyType) || typeof(long).IsAssignableFrom(propertyType))
                {
                    input = _generator.GenerateTextBox(ViewContext, modelExProp,
                        propertyName, propertyValue, propertyEditFormatString, new { @class = "form-control" });
                }
                else if (typeof(DateTime).IsAssignableFrom(propertyType))
                {
                    input = _generator.GenerateTextBox(ViewContext, modelExProp,
                        propertyName, propertyValue, propertyEditFormatString, new { @class = "form-control", @type = "date" });
                }
                //else if (typeof(Time).IsAssignableFrom(propertyType) || typeof(DateTimeOffset).IsAssignableFrom(propertyType))
                //{
                //    input = _generator.GenerateTextBox(ViewContext, modelExProp,
                //        propertyName, propertyValue, propertyEditFormatString, new { @class = "form-control", @type = "time" });
                //}
                else if (typeof(bool).IsAssignableFrom(propertyType))
                {
                    input = _generator.GenerateCheckBox(ViewContext, modelExProp,
                          propertyName, (bool)propertyValue, new { @class = "form-control" });
                }
                else if (typeof(bool).IsAssignableFrom(propertyType))
                {
                    //input = _generator.GenerateRadioButton(ViewContext, modelExProp,
                    //      propertyName, (bool)propertyValue, new { @class = "form-control" });
                }
                else if (typeof(bool).IsAssignableFrom(propertyType))
                {
                    //    input = _generator.GenerateTextArea(ViewContext, modelExProp,
                    //          propertyName, (bool)propertyValue, new { @class = "form-control" });
                }
                else if (typeof(ICollection).IsAssignableFrom(propertyType) || (propertyType.IsGenericType && typeof(ICollection<>).IsAssignableFrom(propertyType.GetGenericTypeDefinition())))
                //propertyType.IsAssignableFrom(typeof(ICollection<>))
                //propertyType.IsAssignableFrom(typeof(List<>))
                {
                    //EqualityComparer<T>.Default.Equals(x, y);
                    //IEnumerable<SelectListItem> selectList = null;
                    var selectList = new List<SelectListItem>();

                    //selectList.Add(new SelectListItem { Text = "بنزینی-چشمی", Value = "PetrolGas" });
                    if (propertyForeignKey != null)
                    {
                        propertyName = propertyForeignKey.Name;
                    }

                    if (propertyValue != null && ((IList)propertyValue).Count > 0)
                    {
                        var tt = propertyValue.GetType();
                        //selectList.Add(new SelectListItem { Value = SelectList propertyValue.str });
                        //IEnumerable<SelectListItem> myCollection = ((IList)propertyValue);
                        foreach (var item in (IList)propertyValue)
                        {

                            //selectList.Add(new SelectListItem { Text = , Value = "PetrolGas" });
                        }

                        var result = ((IEnumerable)propertyValue).Cast<object>().ToList();
                        selectList = (List<SelectListItem>)propertyValue;
                        //result.Select(a =>
                        //new SelectListItem
                        //{
                        //    Value = int.Parse(a),
                        //    Text = a.Text
                        //});
                        //List<T> genericList = (List<T>)propertyValue;
                        //selectList = iii;
                        //iiise(item => list2.Add(new GeoSection() { SectionName = a.Text }));
                    }
                    input = _generator.GenerateSelect(ViewContext, modelExProp, "--",
                           propertyName, selectList, false, new { @class = "form-control" });
                }
                if (propertyType.IsEnum)
                {
                    //                args[i].GetType().IsEnum
                    //Or

                    //if (args[i] is Enum)
                    //                {
                    //                }
                    //                Or

                    //bool isEnum = typeof(Enum).IsAssignableFrom(args[i].GetType());
                    //                Or

                    //bool isEnum = typeof(Enum).IsInstanceOfType(o);
                    //Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectListItem
                    //{
                    //    Text = v.ToString(),
                    //    Value = ((int)v).ToString()
                    //}).ToList();
                    Type realModelType = propertyType;
                    var propertyValues = Enum.GetValues(propertyType);
                    List<SelectListItem> selectList = new List<SelectListItem>();

                    foreach (var item in propertyValues)
                    {
                        var type = item.GetType();
                        var member = type.GetMember(item.ToString());
                        DisplayAttribute displayName = (DisplayAttribute)member[0].GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault();

                        var dropdownItem = new SelectListItem
                        {

                            Text = displayName.Name,
                            Value = item.ToString(),
                          
                        };

                        selectList.Add(dropdownItem);
                    }

                    input = _generator.GenerateSelect(ViewContext, modelExProp, "--",
                           propertyName, selectList, false, new { @class = "form-control" });

                    Type underlyingType = Nullable.GetUnderlyingType(propertyType.GetEnumUnderlyingType());
                    if (underlyingType != null)
                    {
                        realModelType = underlyingType;
                    }
                }

                var label = _generator.GenerateLabel(ViewContext, modelExProp,
    propertyName, propertyDisplayName, new { @class = "control-label", @type = "email" });

                var validation = _generator.GenerateValidationMessage(ViewContext, modelExProp,
                   propertyName, string.Empty, string.Empty, new { @class = "text-danger" });
                // input.Attributes.Add("placeholder", propertyDisplayAttribute.GetPrompt());
                input.Attributes.Add("title", propertyDisplayAttribute.GetDescription());
                input.Attributes.Add("data-toggle", "tooltip");
                input.Attributes.Add("data-placement", "top");

                var inputParent = new TagBuilder("div");
                inputParent.AddCssClass("col-md-101");
                inputParent.InnerHtml.AppendHtml(input);
                inputParent.InnerHtml.AppendHtml(validation);

                var parent = new TagBuilder("div");
                parent.AddCssClass("form-group1");
                parent.InnerHtml.AppendHtml(label);
                parent.InnerHtml.AppendHtml(inputParent);

                output.Content.SetHtmlContent(parent);
                //base.Process(context, output);
            }

        }
    }
}
public class ModelMetadataProvider : IModelMetadataProvider
{
    public IEnumerable<ModelMetadata> GetMetadataForProperties(Type modelType)
    {
        throw new NotImplementedException();
    }

    public ModelMetadata GetMetadataForType(Type modelType)
    {
        throw new NotImplementedException();
    }
}
namespace MIMS.Portal.Infrastructure.TagHelpers
{
    [HtmlTargetElement("formi")]
    public class FormControlTextBoxHelper : TagHelper
    {
        [HtmlAttributeName("asp-for")]
        public ModelExpression For { get; set; }

        private readonly IHtmlGenerator _generator;

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public FormControlTextBoxHelper(IHtmlGenerator generator)
        {
            _generator = generator;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            using (var writer = new StringWriter())
            {
                writer.Write(@"<div class=""form-group"">");

                var label = _generator.GenerateLabel(
                                ViewContext,
                                For.ModelExplorer,
                                For.Name, null,
                                new { @class = "control-label" });

                label.WriteTo(writer, NullHtmlEncoder.Default);

                var textArea = _generator.GenerateTextBox(ViewContext,
                                    For.ModelExplorer,
                                    For.Name,
                                    For.Model,
                                    null,
                                    new { @class = "form-control" });

                textArea.WriteTo(writer, NullHtmlEncoder.Default);

                var validationMsg = _generator.GenerateValidationMessage(
                                        ViewContext,
                                        For.ModelExplorer,
                                        For.Name,
                                        null,
                                        ViewContext.ValidationMessageElement,
                                        new { @class = "text-danger" });

                validationMsg.WriteTo(writer, NullHtmlEncoder.Default);

                writer.Write(@"</div>");

                output.Content.SetHtmlContent(writer.ToString());

            }

        }
    }
}
