﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel.DataAnnotations;
using System.Reflection;


namespace MIMS.Portal.Commons.TagHelpers
{
    [HtmlTargetElement("input", Attributes = "asp-description-for")]
    [HtmlTargetElement("select", Attributes = "asp-description-for")]
    public class DescriptionFor : TagHelper
    {
        [HtmlAttributeName("asp-description-for")]
        public ModelExpression For { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var attributes = For?.Metadata?.ContainerType
                             ?.GetProperty(For.Name)
                             ?.GetCustomAttribute<DisplayAttribute>();

            if (attributes != null)
            {
                output.Attributes.Add("title", attributes?.GetDescription());
                output.Attributes.Add("data-toggle", "tooltip");
                output.Attributes.Add("data-placement", "bottom");
            }
        }
    }

}
