﻿using System;
using System.Linq;
using System.Web.Helpers;

namespace MIMS.Portal.Commons
{
    public class SecurityTools
    {
        public SecurityTools()
        {


        }
        public string GetSalt(int Length = 8)
        {
            int saltLength = Length;

            if (saltLength < 6) //minLength
                saltLength = 6;

            if (saltLength > 12) //maxLength
                saltLength = 12;

            //const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            //var randNum = new Random();
            //var chars = new char[saltLength];
            //var allowedCharCount = allowedChars.Length;

            //for (var i = 0; i <= saltLength - 1; i++)
            //{
            //    chars[i] = allowedChars[Convert.ToInt32((allowedCharCount) * randNum.NextDouble())];
            //}

            //return new string(chars);


            var chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, saltLength)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());

            return result;
        }
        public string GetPhraseKey(string Password, string Salt)
        {
            var PhraseKey = Password + Salt;

            var hashedPassword = Crypto.HashPassword(PhraseKey);

            return PhraseKey;
        }
        public string Base64Encode(string ClearString)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(ClearString);
            return Convert.ToBase64String(data);
        }
        public string Base64Decode(string EncodedString)
        {
            byte[] data = Convert.FromBase64String(EncodedString);
            return System.Text.Encoding.UTF8.GetString(data);
        }
        public bool ValidateUser(Guid UserId, string Password)
        {
            //using (DbEntities _db = new DbEntities())
            //{
            //    var user = _db.Users.FirstOrDefault(u => u.UserId == UserId);

            //    if (user != null)
            //    {
            //        var enteredPassword = Password + Base64Decode(user.Salt);

            //        return Crypto.VerifyHashedPassword(user.Password, enteredPassword);
            //    }
            //}
            return false;
        }
        public bool ValidateUser(string Username, string Password)
        {
            //using (DbEntities _db = new DbEntities())
            //{
            //    var user = _db.Users.FirstOrDefault(u => u.Username == Username);

            //    if (user != null)
            //    {
            //        var enteredPassword = Password + Base64Decode(user.Salt);

            //        return Crypto.VerifyHashedPassword(user.Password, enteredPassword);
            //    }
            //}
            return false;
        }
    }
}