﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Terminal;

namespace MIMS.Portal.Views.Merchants.Components.MerchantTerminals
{
    public class MerchantTerminalsViewComponent : ViewComponent
    {
        private readonly ITerminalService _terminalService;

        public MerchantTerminalsViewComponent(ITerminalService terminalService)
        {
            _terminalService = terminalService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid merchantId)
        {
            var model = Enumerable.Empty<TerminalViewModel>();

            if (merchantId == Guid.Empty)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantTerminalsViewComponent",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);

                return View(model);
            }

            try
            {
                model = _terminalService.FindByMerchantId(merchantId).ToList().ConvertAll(x => (TerminalViewModel)x);

                if (!model.Any())
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Warning,
                        Subject = "پایانه",
                        Content = "پایانه ای برای این پذیرنده یافت نشد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return View(model);
                }
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantTerminalsService",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return View(model);
        }
    }
}
