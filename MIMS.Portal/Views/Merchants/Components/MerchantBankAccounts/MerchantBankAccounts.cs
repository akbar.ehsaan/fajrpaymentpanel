﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.BankAccount;

namespace MIMS.Portal.Views.Merchants.Components.MerchantBankAccounts
{
    public class MerchantBankAccountsViewComponent : ViewComponent
    {
        private readonly IBankAccountService _bankAccountService;

        public MerchantBankAccountsViewComponent(IBankAccountService bankAccountService)
        {
            _bankAccountService = bankAccountService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid merchantId)
        {
            var model = Enumerable.Empty<BankAccountViewModel>();

            if (merchantId == Guid.Empty)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "BankAccountsViewComponent",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);

                return View(model);
            }

            try
            {
                model = _bankAccountService.GetByMerchantId(merchantId).ToList().ConvertAll(x => (BankAccountViewModel)x);

                if (!model.Any())
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Warning,
                        Subject = "شماره حساب",
                        Content = "شماره حسابی برای این پذیرنده یافت نشد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return View(model);
                }
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "BankAccountsService",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return View(model);
        }
    }
}
