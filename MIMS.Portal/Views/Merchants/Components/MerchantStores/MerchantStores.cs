﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Store;

namespace MIMS.Portal.Views.Merchants.Components.MerchantStores
{
    public class MerchantStoresViewComponent : ViewComponent
    {
        private readonly IStoreService _storeService;

        public MerchantStoresViewComponent(IStoreService storeService)
        {
            _storeService = storeService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid merchantId)
        {
            var model = Enumerable.Empty<StoreViewModel>();

            if (merchantId == Guid.Empty)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantStoresViewComponent",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);

                return View(model);
            }

            try
            {
                model = _storeService.GetByMerchantId(merchantId).ToList().ConvertAll(x => (StoreViewModel)x);

                if (!model.Any())
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Warning,
                        Subject = "فروشگاه",
                        Content = "فروشگاهی برای این پذیرنده یافت نشد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return View(model);
                }
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantStoresService",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return View(model);
        }
    }
}
