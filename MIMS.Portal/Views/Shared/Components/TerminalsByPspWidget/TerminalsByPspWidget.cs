﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MIMS.Portal.Commons.Extensions;
using System.Security.Claims;
using System.Text.Json;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Terminal;

namespace MIMS.Portal.Views.Shared.Components.TerminalsByPspWidget
{
    public class TerminalsByPspWidgetViewComponent : ViewComponent
    {
        private readonly ITerminalService _terminalService;

        public TerminalsByPspWidgetViewComponent(ITerminalService terminalService)
        {
            _terminalService = terminalService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var terminals = Enumerable.Empty<TerminalViewModel>();

            try
            {
                terminals = _terminalService.Find().ToList().ConvertAll(x => (TerminalViewModel)x);
                var terminalsByPsp = terminals.Where(x => x.PspProvider != null)
                    .GroupBy(x => x.PspProvider)
                    .Select(y =>
                                new
                                {
                                    Psp = y.Key.GetDisplayName(),
                                    Count = y.Count()
                                }).OrderBy(t => t.Psp).ToList();
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {

                }
                else
                {
                    //merchants = _merchantService.FindByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                var t = JsonSerializer.Serialize(terminalsByPsp);
                TempData["TerminalsByPsp"] = JsonSerializer.Serialize(terminalsByPsp) ;
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            finally {
               
            }

            

            return View();
        }
    }
}
