﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using System.Text.Json;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Store;

namespace MIMS.Portal.Views.Shared.Components.TerminalsByPspWidget
{
    public class StoresByScopeWidgetViewComponent : ViewComponent
    {
        private readonly IStoreService _storeService;

        public StoresByScopeWidgetViewComponent(IStoreService storeService)
        {
            _storeService = storeService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var stores = Enumerable.Empty<StoreViewModel>();

            try
            {
                stores = _storeService.GetAll().ToList().ConvertAll(x => (StoreViewModel)x);
                var storesByBusinessGroup = stores.GroupBy(x => x.BusinessGroupId)
                    .Select(y =>
                                new
                                {
                                    ScopeId = y.Key,
                                    ScopeName = y.FirstOrDefault().BusinessGroupName ,
                                    Count = y.Count()
                                }).OrderBy(t => t.ScopeId).ToList();
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {

                }
                else
                {
                    //merchants = _merchantService.FindByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                var t = JsonSerializer.Serialize(storesByBusinessGroup);
                TempData["StoresByBusinessGroup"] = JsonSerializer.Serialize(storesByBusinessGroup) ;
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            finally {
               
            }

            

            return View();
        }
    }
}
