﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Store;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class StoreCountWidgetViewComponent : ViewComponent
    {
        private readonly IStoreService _storeService;
        private readonly IMerchantService _merchantService;

        public StoreCountWidgetViewComponent(IStoreService storeService, IMerchantService merchantService)
        {
            _merchantService = merchantService;
            _storeService = storeService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var stores = Enumerable.Empty<StoreViewModel>();
            long storesCount = 0;

            try
            {
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {
                    storesCount = _storeService.StatsByCount(null);
                }
                else
                {
                    var merchants = _merchantService.GetByUserId(userId).Select(x => x.Id);
                    //storesCount = _storeService.GetByMerchantId(x.MerchantId)).ToList().ConvertAll(x => (StoreViewModel)x);
                    storesCount = _storeService.StatsByCount(userId);
                }

            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            TempData["StoresCount"] = storesCount.ToString("N0");

            return View();
        }
    }
}
