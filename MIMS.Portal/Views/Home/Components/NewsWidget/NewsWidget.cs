﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class NewsWidgetViewComponent : ViewComponent
    {

        private readonly INewsService _newsService;

        public NewsWidgetViewComponent(INewsService newsService)
        {
            _newsService = newsService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var news = Enumerable.Empty<NewsViewModel>();

            try
            {

                    news = _newsService.FindAll().OrderByDescending(x=>x.CreatedDate).Take(5).ToList().ConvertAll(x => (NewsViewModel)x);


            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "NewsWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }


            return View(news);
        }
    }
}
