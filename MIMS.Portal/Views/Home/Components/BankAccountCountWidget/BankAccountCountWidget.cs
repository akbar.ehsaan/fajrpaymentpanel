﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class BankAccountCountWidgetViewComponent : ViewComponent
    {
        private readonly IBankAccountService _bankAccountService;

        public BankAccountCountWidgetViewComponent(IBankAccountService bankAccountService)
        {
            _bankAccountService = bankAccountService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var merchants = Enumerable.Empty<MerchantViewModel>();

            try
            {
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {
                    //var merchants2 = _merchantService.GetAll().ToList();
                    //merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                else
                {
                    //merchants = _merchantService.FindByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }

            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            TempData["MerchantsCount"] = merchants.Count().ToString("N0");

            return View();
        }
    }
}
