﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Terminal;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class TerminalCountWidgetViewComponent : ViewComponent
    {

        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;

        public TerminalCountWidgetViewComponent(ITerminalService terminalService, IMerchantService merchantService)
        {
            _merchantService = merchantService;
            _terminalService = terminalService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var terminals = Enumerable.Empty<TerminalViewModel>();
            long terminalsCount = 0;

            try
            {
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {
                    terminalsCount = _terminalService.StatsByCount(null);
                }
                else
                {
                    terminalsCount = _terminalService.StatsByCount(userId);
                    //var merchants = _merchantService.GetByUserId(userId).Select(x => x.Id);
                    //terminalsCount = _terminalService.Find().Where(x => merchants.Contains(x.MerchantId)).ToList().ConvertAll(x => (TerminalViewModel)x);
                }

            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            TempData["TerminalsCount"] = terminalsCount.ToString("N0");

            return View();
        }
    }
}
