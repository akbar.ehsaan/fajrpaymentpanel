﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class AgentCountWidgetViewComponent : ViewComponent
    {
        private readonly IUsersService _usersService;

        public AgentCountWidgetViewComponent(IUsersService usersService)
        {
            _usersService = usersService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var users = Enumerable.Empty<UserViewModel>();

            try
            {
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }
                if (User.IsInRole("Admin"))
                {
                    users = _usersService.FindByRole("Agent").ConvertAll(x => (UserViewModel)x);
                }
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantCountWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            TempData["AgentsCount"] = users.Count().ToString("N0");

            return View();
        }
    }
}
