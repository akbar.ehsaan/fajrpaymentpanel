﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Views.Home.Components.MerchantCountWidget
{
    public class GainRateWidgetViewComponent : ViewComponent
    {
        private readonly IMerchantService _merchantService;

        public GainRateWidgetViewComponent(IMerchantService merchantService)
        {
            _merchantService = merchantService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid userId)
        {
            var merchants = Enumerable.Empty<MerchantViewModel>();
            //decimal gainRate = 0;
            double gainRate = 0;

            try
            {
                if (userId == Guid.Empty)
                {
                    var x = HttpContext.User.Identity as ClaimsIdentity;
                    userId = new Guid(x.FindFirst("userId").Value);
                }

                if (User.IsInRole("Admin"))
                {
                    gainRate = _merchantService.StatsByGain(null);
                }
                else
                {
                    gainRate = _merchantService.StatsByGain(userId);
                }

            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "GainRateWidgetWidget",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            TempData["GainRate"] = gainRate.ToString("N2");

            return View();
        }
    }
}
