﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.ViewModels;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Views.DataTransfer.Components.PepExportData
{
    public class PepExportDataViewComponent : ViewComponent
    {
        private readonly ITerminalService _terminalService;

        public PepExportDataViewComponent(ITerminalService terminalService)
        {
            _terminalService = terminalService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = Enumerable.Empty<PspPasargadViewModel>();


            try
            {
                model = _terminalService.GetByPsp(PspProvider.Pep, TerminalStatus.PreApproved).ToList().ConvertAll(x => (PspPasargadViewModel)x);

            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "TerminalsService",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return View(model);
        }
    }
}
