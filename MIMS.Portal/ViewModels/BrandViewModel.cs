﻿using MIMS.Portal.Core.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels
{
    public class BrandViewModel
    {
        public Guid Id { set; get; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = "الزامی")]
        public string Name { set; get; }

        [Display(Name = "نام برند")]
        public Guid? ParentId { set; get; }

        public PosBrand Parent { set; get; }

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }


        //
        //-- mapping --//
        //

        public static implicit operator BrandViewModel(PosBrand model)
        {

            var view = new BrandViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                ParentId = model.ParentId,
                Parent = model.Parent,
                CreatedDate = model.CreatedDate
            };

            return view;
        }

        public static implicit operator PosBrand(BrandViewModel view)
        {
            var model = new PosBrand()
            {
                Id = view.Id,
                Name = view.Name,
                ParentId = view.ParentId,
                Parent = view.Parent,
                CreatedDate = view.CreatedDate,
            };

            return model;
        }

    }
}

