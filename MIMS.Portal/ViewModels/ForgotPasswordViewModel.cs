﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels
{
    public partial class ForgotPasswordViewModel
    {
        [StringLength(255)]
        [Display(Name = "نام کاربری", Prompt = "نام کاربری")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[a-zA-Z0-9_.]{1,50}$", ErrorMessage = "نامعتبر")]
        //[Remote("CheckUsername", "Validator", ErrorMessage = "نامعتبر")]
        public string UserName { set; get; }

        [Display(Name = "شماره ملی", Prompt = "0987654321")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string NationalCode { set; get; }

        [Display(Name = "همراه", Prompt = "همراه")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = "نامعتبر")]
        public string Mobile { set; get; }

        [Display(Name = "کد تصویری", Prompt = "کد تصویری")]
        [Required(ErrorMessage = "الزامی")] 
        public string Captcha { get; set; }

    }
}
