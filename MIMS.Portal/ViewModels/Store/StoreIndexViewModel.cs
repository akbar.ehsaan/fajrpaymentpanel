﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels.Store
{
    public class StoreIndexViewModel
    {
        public StoreFilterViewModel Filters { get; set; }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> Stats { get; set; }

        public IEnumerable<StoreViewModel> Stores { get; set; } = Enumerable.Empty<StoreViewModel>();
    }
}
