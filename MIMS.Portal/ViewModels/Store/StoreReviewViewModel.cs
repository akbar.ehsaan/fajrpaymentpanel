﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels.Store
{
    public class StoreReviewViewModel
    {
        public StoreViewModel StoreDetails { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public Guid StoreId { get; set; }

        [Display(Name = "مستند اقامت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid StoreDocumentId { get; set; }

        public string StoreDocumentUrl { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public ReviewStatus StoreDocumentStatus { get; set; }

        [Display(Name = "تعیین وضعیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public bool ReviewStatus { get; set; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        [RequiredIf("ReviewStatus == false", ErrorMessage = "الزامی")]
        public string ReviewComment { set; get; }
    }

}
