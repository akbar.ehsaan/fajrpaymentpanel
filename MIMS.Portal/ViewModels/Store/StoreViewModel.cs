﻿using System;
using System.ComponentModel.DataAnnotations;
using UoN.ExpressiveAnnotations.NetCore.Attributes;
using MIMS.Portal.Commons.Extensions;
using Microsoft.AspNetCore.Http;
using MIMS.Portal.ValueObjects.Enum;

namespace MIMS.Portal.ViewModels.Store
{
    public class StoreViewModel
    {
        public Guid Id { set; get; }

        [Required(ErrorMessage = "الزامی")]
        public Guid MerchantId { set; get; }

        [Display(Name = "نام و نام خانوادگی پذیرنده")]
        public string MerchantFullName { set; get; }

        [Display(Name = "شماره ملی پذیرنده")]
        public string MerchantNationalCode { set; get; }

        [Display(Name = "نام فروشگاه", Prompt = "فروشگاه خدمات نمونه", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[\u0600-\u06FF ]+$", ErrorMessage = "فقط حروف فارسی")]
        public string StoreName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام فروشگاه (انگلیسی)", Prompt = "Nemone", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishStoreName { set; get; }

        [Display(Name = "گروه صنفی", Description = "انتخاب گروه فعالیت")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BusinessGroupId { set; get; }
        public string BusinessGroupName { set; get; }

        [Display(Name = "صنف فعالیت", Description = "انتخاب حوزه فعالیت")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BusinessScopeId { set; get; }
        public string BusinessScopeName { set; get; }

        [Display(Name = "گروه و صنف فعالیت")]
        public string BusinessFullName { set; get; }

        [Display(Name = "شماره اقتصادی", Prompt = "411098765432", Description = "صرفا عدد")]
        [RegularExpression(@"(^411\d{9}$)|(^$)", ErrorMessage = "نامعتبر")]
        public string EconomicCode { get; set; }

        [Display(Name = "کد رهگیری ثبت نام مالیاتی", Prompt = "98765432", Description = "صرفا عدد")]
        [Required(ErrorMessage = "الزامی")]
        public string TaxTrackingCode { set; get; }

        [Display(Name = "همراه", Prompt = "09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، 09121234567")]
        public string Mobile { set; get; }

        [Display(Name = "تلفن", Prompt = "02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، 02187654321")]
        public string Phone { get; set; }

        [Display(Name = "استان")]
        [Required(ErrorMessage = "الزامی")]
        public Guid ProvinceId { get; set; }
        public string ProvinceName { get; set; }

        [Display(Name = "شهرستان")]
        [Required(ErrorMessage = "الزامی")]
        public Guid CountyId { get; set; }
        public string CountyName { get; set; }

        [Display(Name = "شهر")]
        [Required(ErrorMessage = "الزامی")]
        public Guid CityId { get; set; }
        public string CityName { get; set; }

        [Display(Name = "کد پستی", Prompt = "1234567890", Description = "صرفا عدد")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string PostalCode { get; set; }

        [Display(Name = "نشانی", Prompt = "تهران، آزادی...", Description = "نشانی به شکل خیابان اصلی، خیابان فرعی، جزئیات آدرس، پلاک")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF \d\-\،\,\(\)]+ )*[\u0600-\u06FF \d\-\،\,\(\)]+$", ErrorMessage = "نامعتبر")]
        public string Address { get; set; }

        [Display(Name = "نوع اقامت", Prompt = "", Description = "بر اساس مستندات")]
        [Required(ErrorMessage = "الزامی")]
        public ResidenceType ResidenceType { set; get; }

        [Display(Name = "شماره قرارداد/مجوز", Prompt = "32131", Description = "صرفا شماره مجوز")]
        [RequiredIf("ResidenceType == 2 || ResidenceType == 3", ErrorMessage = "الزامی")]
        public string DocumentNumber { set; get; }

        [Display(Name = "تاریخ شروع", Prompt = "1399/01/01", Description = "صرفا تاریخ")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        [RequiredIf("ResidenceType == 2 || ResidenceType == 3", ErrorMessage = "الزامی")]
        public string DocumentStartDate { set; get; }

        [Display(Name = "تاریخ پایان", Prompt = "1399/01/01", Description = "صرفا تاریخ")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        [RequiredIf("ResidenceType == 2 || ResidenceType == 3", ErrorMessage = "الزامی")]
        public string DocumentEndDate { set; get; }

        [Display(Name = "مستند اقامت", Prompt = "مستند اقامت", Description = "تصویر مستند اقامت با پسوندهای مجاز png, jpg, jpeg")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Upload)]
        public IFormFile StoreDocument { get; set; }
        public string StoreDocumentUrl { get; set; }

        [Display(Name = "وضعیت", Prompt = "", Description = "")]
        public ReviewStatus Status { set; get; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        public string ReviewComment { set; get; }

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator StoreViewModel(Core.Data.Domains.Store entity)
        {
            var view = new StoreViewModel()
            {
                Id = entity.Id,
                MerchantId = entity.Merchant?.Id ?? entity.MerchantId,
                MerchantFullName = $"{entity.Merchant?.FirstName} {entity.Merchant?.LastName}",
                MerchantNationalCode = entity.Merchant?.NationalCode,
                StoreName = entity.StoreName,
                EnglishStoreName = entity.EnglishStoreName,
                BusinessGroupId = entity.BusinessGroup?.Id ?? entity.BusinessGroupId,
                BusinessGroupName = entity.BusinessGroup?.Name,
                BusinessScopeId = entity.BusinessScope?.Id ?? entity.BusinessScopeId,
                BusinessScopeName = entity.BusinessScope?.Name,
                BusinessFullName = $"{entity.BusinessGroup?.Name}-{entity.BusinessScope?.Name}",
                EconomicCode = entity.EconomicCode,
                TaxTrackingCode = entity.TaxRegistrationTrackingCode,
                ProvinceId = entity.Provience?.Id ?? entity.ProvienceId,
                ProvinceName = entity.Provience?.Name,
                CountyId = entity.County?.Id ?? entity.CountyId,
                CountyName = entity.County?.Name,
                CityId = entity.City?.Id ?? entity.CityId,
                CityName = entity.City?.Name,
                PostalCode = entity.PostalCode,
                Address = entity.Address,
                Mobile = entity.Mobile,
                Phone = entity.Phone,
                ResidenceType = entity.ResidenceType,
                DocumentNumber = entity.DocumentNumber,
                DocumentStartDate = entity.DocumentStartDate?.ToPeString(),
                DocumentEndDate = entity.DocumentEndDate?.ToPeString(),
                Status = entity.Status,
                ReviewComment = entity.ReviewComment,
                CreatedDate = entity.CreatedDate,
            };

            return view;
        }

        public static implicit operator Core.Data.Domains.Store(StoreViewModel view)
        {
            DateTime? contractStartDate = null;
            DateTime? contractEndDate = null;

            if (!string.IsNullOrWhiteSpace(view.DocumentStartDate))
                contractStartDate = view.DocumentStartDate?.ToEnDate();

            if (!string.IsNullOrWhiteSpace(view.DocumentEndDate))
                contractEndDate = view.DocumentEndDate?.ToEnDate();


            var model = new Core.Data.Domains.Store()
            {
                Id = view.Id,
                MerchantId = view.MerchantId,
                StoreName = view.StoreName,
                EnglishStoreName = view.EnglishStoreName,
                BusinessGroupId = view.BusinessGroupId,
                BusinessScopeId = view.BusinessScopeId,
                EconomicCode = view.EconomicCode,
                TaxRegistrationTrackingCode = view.TaxTrackingCode,
                ProvienceId = view.ProvinceId,
                CountyId = view.CountyId,
                CityId = view.CityId,
                PostalCode = view.PostalCode,
                Address = view.Address,
                Mobile = view.Mobile,
                Phone = view.Phone,
                ResidenceType = view.ResidenceType,
                DocumentNumber = view.DocumentNumber,
                DocumentStartDate = contractStartDate,
                DocumentEndDate = contractEndDate,
                ReviewComment = view.ReviewComment,
                Status = view.Status,
                CreatedDate = view.CreatedDate
            };

            return model;
        }
    }
}
