﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels.Store
{
    public class StoreFilterViewModel
    {
        [Display(Name = "شماره ملی", Prompt = " 0987654321", Description = "صرفا عدد و 10 رقم")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string NationalCode { set; get; }

        [StringLength(255)]
        [Display(Name = "نام", Prompt = " علی", Description = "صرفا حروف فارسی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string LastName { set; get; }

        [Display(Name = "وضعیت")]
        public ReviewStatus? Status { get; set; }
    }
}
