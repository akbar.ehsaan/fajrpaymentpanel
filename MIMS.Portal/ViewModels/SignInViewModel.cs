﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels
{
    public partial class SignInViewModel
    {
        [Display(Name = "نام کاربری", Prompt = "نام کاربری")]
        [Required(ErrorMessage = "الزامی")]
        public string UserName { get; set; }

        [Display(Name = "گذرواژه", Prompt = "گذرواژه")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "کد تصویری", Prompt = "کد تصویری")]
        [Required(ErrorMessage = "الزامی")] 
        public string Captcha { get; set; }

        //public static implicit operator SignInViewModel(SignIn dto)
        //{
        //    return new SignInViewModel
        //    {
        //        Username = dto.Username.Value,
        //        Password = dto.Password.Value,
        //    };
        //}

        //public static implicit operator SignIn(SignInViewModel vm)
        //{
        //    return new SignIn
        //    {
        //        Username = new Username { Value = vm.Username },
        //        Password = new Password { Value = vm.Password }
        //    };
        //}
    }
}
