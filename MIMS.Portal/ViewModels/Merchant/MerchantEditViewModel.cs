﻿using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.ValueObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using UoN.ExpressiveAnnotations.NetCore.Attributes;
using MIMS.Portal.ValueObjects.Enum;
using Microsoft.AspNetCore.Http;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.ViewModels
{
    public class MerchantEditViewModel
    {
        public Guid Id { set; get; }

        [Display(Name = "نوع پذیرنده", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public PersonType MerchantType { set; get; }

        [StringLength(255)]
        [Display(Name = "نام", Prompt = " علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string LastName { set; get; }

        [Display(Name = "نام و نام خانوادگی")]
        public string MerchantFullname { get { return $"{FirstName} {LastName}"; } }

        [StringLength(255)]
        [Display(Name = "نام پدر", Prompt = " محمد", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FatherName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام (انگلیسی)", Prompt = " Ali", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishFirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی (انگلیسی)", Prompt = " Ahmadi", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishLastName { set; get; }

        [Display(Name = "نام و نام خانوادگی (انگلیسی)")]
        public string EnglishMerchantFullname { get { return $"{EnglishFirstName} {EnglishLastName}"; } }

        [StringLength(255)]
        [Display(Name = "نام پدر (انگلیسی)", Prompt = " Mohammad", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishFatherName { set; get; }

        [Display(Name = "جنسیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Gender Gender { set; get; }

        [Display(Name = "شماره ملی", Prompt = " 0987654321", Description = "صرفا عدد و 10 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        [Remote("CheckNationalID", "Validator", ErrorMessage = "نامعتبر", AdditionalFields ="Id")]
        public string NationalCode { set; get; }

        [Display(Name = "تاریخ تولد", Prompt = " 1399/01/01", Description = "صرفا تاریخ")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        public string BirthDate { set; get; }

        [Display(Name = "شماره شناسنامه", Prompt = " 123", Description = "صرفا عدد")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{1,}$", ErrorMessage = "نامعتبر")]
        public string BirthCertificateNumber { set; get; }

        [Display(Name = "نام شرکت", Prompt = " شرکت خدمات نمونه", Description = "صرفا حروف فارسی", GroupName = "qaz123")]
        [RegularExpression(@"^[\u0600-\u06FF ]+$", ErrorMessage = "فقط حروف فارسی")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string CompanyName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام شرکت (انگلیسی)", Prompt = " Nemone", Description = "صرفا حروف انگلیسی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string EnglishCompanyName { set; get; }

        [Display(Name = "شناسه ملی", Prompt = "مثال 10380204000", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{11}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string CompanyNationalId { get; set; }

        [Display(Name = "شماره ثبت", Prompt = " 123", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{2,10}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string CompanyRegisterNumber { get; set; }

        [Display(Name = "شماره اقتصادی", Prompt = " 411098765432", Description = "صرفا عدد")]
        [RegularExpression(@"(^411\d{9}$)|(^$)", ErrorMessage = "نامعتبر")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string CompanyEconomicCode { get; set; }

        [Display(Name = "تاریخ ثبت", Prompt = " 1399/01/01", Description = "صرفا تاریخ")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        [RequiredIf("MerchantType == 2", ErrorMessage = "الزامی")]
        public string CompanyRegisterDate { get; set; }

        //public DateTime? GregorianCompanyRegisterDate { get { return CompanyRegisterDate.ToEnDate(); } }


        [Display(Name = "همراه", Prompt = " 09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، مثال 09121234567")]
        public string Mobile { set; get; }

        [Display(Name = "تلفن", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string Phone { get; set; }

        //[Display(Name = "نمابر", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        //[Required(ErrorMessage = "الزامی")]
        //  [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string Fax { get; set; }

        [Display(Name = "رایانامه", Prompt = "info@iran.ir", Description = "")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "کد پستی", Prompt = "1234567890", Description = "صرفا عدد")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string PostalCode { get; set; }

        [Display(Name = "نشانی", Prompt = "تهران، آزادی...", Description = "نشانی به شکل خیابان اصلی، خیابان فرعی، جزئیات آدرس، پلاک", GroupName = "موقعیت")]
        // [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF \d\-\،\,\(\)]+ )*[\u0600-\u06FF \d\-\،\,\(\)]+$", ErrorMessage = "نامعتبر")]
        public string Address { get; set; }

        [StringLength(255)]
        [Display(Name = "نام (معرف)", Prompt = "علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string ReferrerFirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی (معرف)", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string ReferrerLastName { set; get; }

        [Display(Name = "نام و نام خانوادگی (معرف)")]
        public string ReferrerFullname { get { return $"{ReferrerFirstName} {ReferrerLastName}"; } }

        [Display(Name = "شماره ملی (معرف)", Prompt = " 0987654321", Description = "صرفا عدد و 10 رقم")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        [Remote("CheckNationalID", "Validator", "", ErrorMessage = "نامعتبر")]
        public string ReferrerNationalCode { set; get; }

        [Display(Name = "همراه (معرف)", Prompt = " 09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، مثال 09121234567")]
        public string ReferrerMobile { set; get; }

        [Display(Name = "تلفن (معرف)", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string ReferrerPhone { set; get; }

        //[Required(ErrorMessage = "الزامی")]
        [Display(Name = "کد بازاریاب", Prompt = " 159", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{1,}$", ErrorMessage = " نامعتبر، مثال 159")]
        public string MarketerCode { set; get; }

        [Display(Name = "کارت ملی (روی)", Prompt = "کارت ملی (روی)", Description = "تصویر روی کارت ملی با پسوندهای مجاز png, jpg, jpeg")]
        [RequiredIf("IDCardFrontId == Guid('00000000-0000-0000-0000-000000000000') ", ErrorMessage = "الزامی")]
        [DataType(DataType.Upload)]
        public IFormFile IDCardFront { get; set; }
        public string IDCardFrontUrl { get; set; }
        public Guid IDCardFrontId { get; set; }

        [Display(Name = "کارت ملی (پشت)", Prompt = "کارت ملی (پشت)", Description = "تصویر پشت کارت ملی با پسوندهای مجاز png, jpg, jpeg")]
        [DataType(DataType.Upload)]
        public IFormFile IDCardBack { get; set; }
        public string IDCardBackUrl { get; set; }
        public Guid IDCardBackId { get; set; }

        [Display(Name = "شناسنامه (صفحه اول)", Prompt = "شناسنامه (صفحه اول)", Description = "پسوندهای مجاز png, jpg, jpeg")]
        [RequiredIf("CredentialMainPageId == Guid('00000000-0000-0000-0000-000000000000')", ErrorMessage = "الزامی")]
        [DataType(DataType.Upload)]
        public IFormFile CredentialMainPage { get; set; }
        public string CredentialMainPageUrl { get; set; }
        public Guid CredentialMainPageId { get; set; }

        [Display(Name = "شناسنامه (صفحه توضیحات)", Prompt = "شناسنامه (صفحه توضیحات)", Description = "پسوندهای مجاز png, jpg, jpeg")]
        [DataType(DataType.Upload)]
        public IFormFile CredentialLastPage { get; set; }
        public string CredentialLastPageUrl { get; set; }
        public Guid CredentialLastPageId { get; set; }

        [Display(Name = "ثبت کننده", Prompt = "", Description = "")]
        public Guid CreatorUserId { set; get; }
        public string CreatorFullName { set; get; }

        [Display(Name = "وضعیت", Prompt = "", Description = "")]
        public ReviewStatus Status { set; get; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        public string? ReviewComment { set; get; }

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator MerchantEditViewModel(Merchant entity)
        {
            var view = new MerchantEditViewModel()
            {
                Id = entity.Id,
                MerchantType = entity.Type,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                FatherName = entity.FatherName,
                EnglishFirstName = entity.EnglishFirstName,
                EnglishLastName = entity.EnglishLastName,
                EnglishFatherName = entity.EnglishFatherName,
                Gender = entity.Gender,
                NationalCode = entity.NationalCode,
                BirthDate = entity.BirthDate.ToPeString("yyyy/MM/dd"),
                BirthCertificateNumber = entity.BirthCertificateNumber,
                CompanyName = entity.CompanyName,
                EnglishCompanyName = entity.EnglishCompanyName,
                CompanyNationalId = entity.CompanyNationalID,
                CompanyRegisterNumber = entity.CompanyRegisterCode,
                CompanyEconomicCode = entity.CompanyEconomicCode,
                CompanyRegisterDate = entity.CompanyRegisterDate != null ? entity.CompanyRegisterDate.Value.ToPeString() : "",
                Mobile = entity.Mobile,
                Phone = entity.Phone,
                Fax = entity.Fax,
                Email = entity.Email,
                PostalCode = entity.PostalCode,
                Address = entity.Address,
                ReferrerFirstName = entity.ReferrerFirstName,
                ReferrerLastName = entity.ReferrerLastName,
                ReferrerNationalCode = entity.ReferrerNationalCode,
                ReferrerMobile = entity.ReferrerMobile,
                ReferrerPhone = entity.ReferrerPhone,
                MarketerCode = entity.MarketerCode,
                Status = entity.Status,
                ReviewComment = entity.ReviewComment,
                CreatorUserId = entity.CreatorUser?.Id ?? entity.CreatorUser?.Id ?? Guid.Empty,
                CreatorFullName = entity.CreatorUser?.Fullname,
                CreatedDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator Merchant(MerchantEditViewModel view)
        {
            var referreFullname = Newtonsoft.Json.JsonConvert.SerializeObject(
                new FullName
                {
                    FirstName = view.ReferrerFirstName,
                    LastName = view.ReferrerLastName
                });

            DateTime? gregorianCompanyRegisterDate = null;

            if (!string.IsNullOrWhiteSpace(view.CompanyRegisterDate))
            {
                gregorianCompanyRegisterDate = view.CompanyRegisterDate.ToEnDate();
            }

            var entity = new Merchant()
            {
                Id = view.Id,
                Type = view.MerchantType,
                FirstName = view.FirstName,
                LastName = view.LastName,
                FatherName = view.FatherName,
                EnglishFirstName = view.EnglishFirstName,
                EnglishLastName = view.EnglishLastName,
                EnglishFatherName = view.EnglishFatherName,
                Gender = view.Gender,
                NationalCode = view.NationalCode,
                BirthDate = view.BirthDate.ToEnDate(),
                BirthCertificateNumber = view.BirthCertificateNumber,
                CompanyName = view.CompanyName,
                EnglishCompanyName = view.EnglishCompanyName,
                CompanyNationalID = view.CompanyNationalId,
                CompanyRegisterCode = view.CompanyRegisterNumber,
                CompanyEconomicCode = view.CompanyEconomicCode,
                CompanyRegisterDate = gregorianCompanyRegisterDate,
                Mobile = view.Mobile,
                Phone = view.Phone,
                Fax = view.Fax,
                Email = view.Email,
                PostalCode = view.PostalCode,
                Address = view.Address,
                ReferrerFirstName = view.ReferrerFirstName,
                ReferrerLastName = view.ReferrerLastName,
                ReferrerNationalCode = view.ReferrerNationalCode,
                ReferrerMobile = view.ReferrerMobile,
                ReferrerPhone = view.ReferrerPhone,
                MarketerCode = view.MarketerCode,
                Status = view.Status,
                ReviewComment = view.ReviewComment,
                CreatedDate = view.CreatedDate
            };

            return entity;
        }
    }
}
