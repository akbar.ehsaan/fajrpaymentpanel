﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels
{
    public class MerchantDetailsReviewViewModel
    {
        public MerchantViewModel MerchantDetails { set; get; }

        public MerchantReviewViewModel MerchantReview { set; get; }
}
}
