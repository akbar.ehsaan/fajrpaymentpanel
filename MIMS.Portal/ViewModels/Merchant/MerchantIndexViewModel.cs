﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels
{
    public class MerchantIndexViewModel
    {
        public MerchantFilterViewModel Filters { get; set; }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> Stats { get; set; }
        
        public IEnumerable<MerchantViewModel> Merchants { get; set; } = Enumerable.Empty<MerchantViewModel>();
    }
}
