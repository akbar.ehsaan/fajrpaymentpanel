﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels
{
    public class MerchantReviewViewModel
    {
        [Required(ErrorMessage = "الزامی")]
        public Guid MerchantId { get; set; }

        [Display(Name = "کارت ملی (روی)", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid IDCardFrontId { get; set; }

        public string IDCardFrontUrl { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public ReviewStatus IDCardFrontStatus { get; set; }

        [Display(Name = "کارت ملی (پشت)", Prompt = "", Description = "")]
        public Guid IDCardBackId { get; set; }

        public string IDCardBackUrl { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public ReviewStatus IDCardBackStatus { get; set; }

        [Display(Name = "شناسنامه (صفحه اول)", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid CredentialMainPageId { get; set; }

        public string CredentialMainPageUrl { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public ReviewStatus CredentialMainPageStatus { get; set; }

        [Display(Name = "شناسنامه (صفحه توضیحات)", Prompt = "", Description = "")]
        public Guid CredentialLastPageId { get; set; }

        public string CredentialLastPageUrl { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public ReviewStatus CredentialLastPageStatus { get; set; }

        [Display(Name = "تعیین وضعیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public bool ReviewStatus { get; set; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        [RequiredIf("ReviewStatus == false", ErrorMessage = "الزامی")]
        public string? ReviewComment { set; get; }
    }

    public class MerchantReviewedViewModel
    {
        public MerchantReviewViewModel MerchantReview { set; get; }
    }
}
