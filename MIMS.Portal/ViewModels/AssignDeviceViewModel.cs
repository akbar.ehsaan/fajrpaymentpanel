﻿using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels.Terminal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels
{
    public class AssignDeviceViewModel
    {
        public TerminalViewModel? TerminalDetails { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public Guid TerminalId { get; set; }

        [Display(Name = "نوع دستگاه", Prompt = "", Description = "نوع دستگاه بر حسب اتصال")]
        [Required(ErrorMessage = "الزامی")]
        public PosConnectionType DeviceConnectionType { set; get; }

        [Display(Name = "برند دستگاه", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid? DeviceBrandId { set; get; }

        [Display(Name = "مدل دستگاه", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid? DeviceModelId { set; get; }

        [Display(Name = "سریال دستگاه", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Guid? DeviceId { set; get; }       
    }
}
