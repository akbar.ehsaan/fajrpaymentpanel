﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels
{
    public class RecoveryPasswordViewModel
    {
        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public Guid RecoveryId { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public string Token { get; set; }

        [Display(Name = "کد یکبار مصرف", Prompt = "کد یکبار مصرف")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "نامعتبر")]
        public string OtpCode { get; set; }

        [Display(Name = "گذرواژه جدید", Prompt = "گذرواژه جدید")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "گذرواژه جدید (تکرار)", Prompt = "گذرواژه جدید (تکرار)")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "عدم همخوانی تکرار")]
        public string ConfirmNewPassword { get; set; }
    }
}
