﻿using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels
{
    public class DeviceViewModel
    {
        public Guid Id { set; get; }

        [Display(Name = "نام برند")]
        [Required(ErrorMessage = "الزامی")]
        public Guid PosBrandId { set; get; }

        [Display(Name = "برند")]
        public string BrandName { set; get; }

        [Display(Name = "مدل")]
        [Required(ErrorMessage = "الزامی")]
        public Guid PosModelId { set; get; }

        [Display(Name = "مدل")]
        public string ModelName { set; get; }

        [Display(Name = "سریال دستگاه", Prompt = "147852369", Description = "صرفا عدد، حروف انگلیسی و خط تیره")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([0-9a-zA-Z]{3,}([0-9a-zA-Z-]*)?[0-9a-zA-Z])$", ErrorMessage = "نامعتبر")]
        [Remote("CheckDeviceSerial", "Validator", ErrorMessage = "نامعتبر", AdditionalFields = "Id,PosBrandId")]
        public string Serial { set; get; }

        [Display(Name = "نوع اتصال", Prompt = "", Description = "نوع اتصال")]
        [Required(ErrorMessage = "الزامی")]
        public PosConnectionType ConnectionType { set; get; }

        [Display(Name = "سیستم عامل", Prompt = "", Description = "سیستم عامل دستگاه")]
        //[Required(ErrorMessage = "الزامی")]
        public PosOSType? OsType { set; get; } = PosOSType.NonSmart;

        [Display(Name = "نسخه پایانه", Description = "صرفا عدد، حروف انگلیسی و خط تیره")]
        [RegularExpression(@"^([0-9a-zA-Z]{3,}([0-9a-zA-Z-]*)?[0-9a-zA-Z])$", ErrorMessage = "نامعتبر")]
        public string AppVersion { set; get; }

        [Display(Name = "نسخه BIOS", Description = "صرفا عدد، حروف انگلیسی و خط تیره")]
        [RegularExpression(@"^([0-9a-zA-Z]{3,}([0-9a-zA-Z-]*)?[0-9a-zA-Z])$", ErrorMessage = "نامعتبر")]
        public string FirmwareVersion { set; get; }

        [Display(Name = "مالکیت", Prompt = "", Description = "نوع مالکیت دستگاه")]
        //[Required(ErrorMessage = "الزامی")]
        public PosOwnerType? OwnerType { set; get; } = PosOwnerType.Unknown;

        [Display(Name = "کاربر")]
        [Required(ErrorMessage = "الزامی")]
        public Guid SupervisorUserId { set; get; }

        [Display(Name = "کاربر")]
        public string SupervisorName { set; get; }

        [Display(Name = "وضعیت", Prompt = "", Description = "وضعیت دستگاه")]
        public PosStatus Status { set; get; } = PosStatus.Unknown;

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator DeviceViewModel(PosDevice entity)
        {

            var view = new DeviceViewModel()
            {
                Id = entity.Id,
                PosBrandId = entity.PosDeviceBrand?.Id ?? entity.PosDeviceBrandId,
                BrandName = entity.PosDeviceBrand?.Name,
                PosModelId = entity.PosDeviceModel?.Id ?? entity.PosDeviceModelId,
                ModelName = entity.PosDeviceModel?.Name,
                //ConnectionType = (PosConnectionType)Enum.Parse(typeof(PosConnectionType), model.DeviceConnectionType.ToString()),
                ConnectionType = entity.DeviceConnectionType,
                OwnerType = entity.DeviceOwnerType,
                OsType = entity.DeviceOSType,
                AppVersion = entity.AppVersion,
                FirmwareVersion = entity.FirmwareVersion,
                Status = entity.DeviceStatus,
                Serial = entity.DeviceSerial,
                SupervisorUserId = entity.User?.Id ?? entity.SupervisorUserId,
                SupervisorName = entity.User?.UserName,
                CreatedDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator PosDevice(DeviceViewModel view)
        {
            var model = new PosDevice()
            {
                Id = view.Id,
                PosDeviceBrandId = view.PosBrandId,
                PosDeviceModelId = view.PosModelId,
                DeviceSerial = view.Serial,
                DeviceConnectionType = view.ConnectionType,
                DeviceOSType = view.OsType ?? PosOSType.NonSmart,
                AppVersion = view.AppVersion,
                FirmwareVersion = view.FirmwareVersion,
                DeviceOwnerType = view.OwnerType ?? PosOwnerType.Unknown,
                DeviceStatus = view.Status,
                SupervisorUserId = view.SupervisorUserId,
                CreatedDate = view.CreatedDate,
            };

            return model;
        }
    }
}
