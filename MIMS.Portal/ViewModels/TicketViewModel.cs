﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects;
using System;
using System.ComponentModel.DataAnnotations;
using MIMS.Portal.ValueObjects.Enum;
using Microsoft.AspNetCore.Http;

namespace MIMS.Portal.ViewModels
{
    public class TicketViewModel
    {
        public Guid Id { set; get; }

        public Guid ParentId { set; get; }

        [Display(Name = "عنوان")]
        [Required(ErrorMessage = "الزامی")]
        public string Subject { set; get; }

        [Display(Name = "محتوای پیام")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.MultilineText)]
        public string Body { set; get; }

        [Display(Name = "پیوست")]
        //[RegularExpression(@"([a-zA-Z0-9\u0600-\u06FF\s_\\.\-:])+(.png|.jpg|.gif|.doc|.docx|.pdf|.xls|.xlsx|.txt)$", ErrorMessage = "نامعتبر")]
        [DataType(DataType.Upload)]
        [FileExtensions(Extensions = "png,jpg,gif,doc,docx,pdf,xls,xlsx,txt", ErrorMessage = "فایل نامعتبر")]
        public IFormFile Attachment { set; get; }

        public Guid SenderUserId { set; get; }

        public string SenderUserFullName { set; get; }

        public Guid ReciverUserId { set; get; }

        public string ReciverUserFullName { set; get; }

        [Display(Name = "اولویت", Prompt = "", Description = "اولویت پیام")]
        public Priority Priority { set; get; } = Priority.Normal;

        [Display(Name = "وضعیت", Prompt = "", Description = "وضعیت پیام")]
        public TicketStatus Status { set; get; } = TicketStatus.Pending;

        public DateTime SubmitDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator TicketViewModel(Ticket entity)
        {

            var view = new TicketViewModel()
            {
                Id = entity.Id,
                ParentId = entity.ParentId,
                Subject = entity.Subject,
                Body = entity.Body,
                SenderUserId = entity.SenderUserId,
                //SenderUserFullName = $"{entity.Merchant?.FirstName} {entity.Merchant?.LastName}",
                ReciverUserId = entity.ReciverrUserId,
                //ReciverUserFullName = $"{entity.Merchant?.FirstName} {entity.Merchant?.LastName}",
                Status = (TicketStatus)Enum.Parse(typeof(TicketStatus), entity.TicketStatus, true),
                SubmitDate = entity.SubmitDate
            };

            return view;
        }

        public static implicit operator TicketViewModel(TicketView entity)
        {

            var view = new TicketViewModel()
            {
                Id = entity.Id,
                ParentId = entity.ParentId,
                Subject = entity.Subject,
                Body = entity.Body,
                SenderUserId = entity.SenderUser?.Id ?? Guid.Empty,
                SenderUserFullName = $"{entity.SenderUser?.FirstName} {entity.SenderUser?.LastName}",
                ReciverUserId = entity.ReciverrUser?.Id ?? Guid.Empty,
                ReciverUserFullName = $"{entity.ReciverrUser?.FirstName} {entity.ReciverrUser?.LastName}",
                Status = (TicketStatus)Enum.Parse(typeof(TicketStatus), entity.TicketStatus, true),
                SubmitDate = entity.SubmitDate
            };

            return view;
        }

        public static implicit operator Ticket(TicketViewModel view)
        {
            var model = new Ticket()
            {
                Id = view.Id,
                ParentId = view.ParentId,
                Subject = view.Subject,
                Body = view.Body,
                SenderUserId = view.SenderUserId,
                ReciverrUserId = view.ReciverUserId,
                TicketStatus = view.Status.ToString(),
                SubmitDate = view.SubmitDate
            };

            return model;
        }
    }
}
