﻿using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels.Upgarde
{
    public partial class AuthSignInViewModel
    {
        [Display(Name = "شماره ملی", Prompt = "شماره ملی", Description = "صرفا عدد و 10 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string NationalCode { set; get; }

        [Display(Name = "شماره همراه", Prompt = "شماره همراه", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = "نامعتبر")]
        public string Mobile { set; get; }

        [Display(Name = "کد تصویری", Prompt = "کد تصویری")]
        [Required(ErrorMessage = "الزامی")]
        public string Captcha { get; set; }
    }
}
