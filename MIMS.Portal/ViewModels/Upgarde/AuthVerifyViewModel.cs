﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels.Upgarde
{
    public class AuthVerifyViewModel
    {
        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public Guid VerifyId { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public string Token { get; set; }

        [Display(Name = "کد یکبار مصرف", Prompt = "کد یکبار مصرف")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]{1,10}$", ErrorMessage = "نامعتبر")]
        public string OtpCode { get; set; }

        [Display(Name = "شماره ملی")]
        public string NationalCode { set; get; }

        [Display(Name = "نام و نام خانوادگی")]
        public string MerchantFullName { set; get; }

        [Display(Name = "نام فروشگاه")]
        public string StoreName { set; get; }
    }
}
