﻿using MIMS.Portal.ViewModels.BankAccount;
using MIMS.Portal.ViewModels.Store;
using System;
using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels.Upgarde
{
    public class MerchantInfoViewModel
    {
        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public Guid VerifyId { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessage = "الزامی")]
        public string Token { get; set; }
        public MerchantViewModel Merchant { get; set; }
        public BankAccountViewModel BankAccount { get; set; }
        public StoreViewModel Store { get; set; }

    }
}
