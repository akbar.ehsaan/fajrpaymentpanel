﻿using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.ValueObjects;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.ViewModels
{
    public class PspPasargadViewModel
    {
        public Guid Id { set; get; }
       
        [Display(Name = "شماره ملی", Prompt = " 0987654321", Description = "صرفا عدد و 10 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        [Remote("CheckNationalID", "Validator", ErrorMessage = "نامعتبر")]
        public string NationalCode { set; get; }

        [StringLength(255)]
        [Display(Name = "نام", Prompt = " علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string LastName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام پدر", Prompt = " محمد", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FatherName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام (انگلیسی)", Prompt = " Ali", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishFirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی (انگلیسی)", Prompt = " Ahmadi", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishLastName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام پدر (انگلیسی)", Prompt = " Mohammad", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishFatherName { set; get; }

        [Display(Name = "جنسیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public Gender Gender { set; get; }

        [Display(Name = "تاریخ تولد", Prompt = " 1399/01/01", Description = "صرفا تاریخ")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        public string BirthDate { set; get; }

        [Display(Name = "تلفن", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string Phone { get; set; }

        [Display(Name = "همراه", Prompt = " 09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، مثال 09121234567")]
        public string Mobile { set; get; }

        [StringLength(255)]
        [Display(Name = "نام (معرف)", Prompt = "علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string ReferrerFirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی (معرف)", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string ReferrerLastName { set; get; }

        [Display(Name = "تلفن (معرف)", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string ReferrerPhone { set; get; }

        [Display(Name = "همراه (معرف)", Prompt = " 09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، مثال 09121234567")]
        public string ReferrerMobile { set; get; }

        [Display(Name = "شماره شبا", Prompt = "IR062960000000100324200001", Description = "صرفا شماره شبای بانکی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^(IR|ir)+([0-9]{24})$", ErrorMessage = "نامعتبر")]
        public string AccountIban { get; set; }

        [Display(Name = "شماره حساب", Prompt = "", Description = "صرفا شماره حساب معتبر بانکی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]+(-|.|\/)+([0-9]+)+$", ErrorMessage = "نامعتبر")]
        public string AccountNumber { get; set; }

        [Display(Name = "بانک")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BankId { get; set; }

        [Display(Name = "بانک")]
        public string BankName { set; get; }

        [Display(Name = "کد شعبه")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{1,10}$", ErrorMessage = "نامعتبر")]
        public string BankBranchCode { get; set; }

        [Display(Name = "کد پستی", Prompt = "1234567890", Description = "صرفا عدد")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string PostalCode { get; set; }

        [Display(Name = "صنف/حوزه فعالیت", Prompt = "مثال", Description = "انتخاب رسته فعالیت مجاز")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BusinessScopeId { set; get; }
        public string BusinessScopeName { set; get; }

        [Display(Name = "نام فروشگاه", Prompt = "فروشگاه خدمات نمونه", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[\u0600-\u06FF ]+$", ErrorMessage = "فقط حروف فارسی")]
        public string StoreName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام فروشگاه (انگلیسی)", Prompt = "Nemone", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([A-Za-z\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string EnglishStoreName { set; get; }

        [Display(Name = "نوع اقامت", Prompt = "", Description = "بر اساس مستندات")]
        [Required(ErrorMessage = "الزامی")]
        public ResidenceType ResidenceType { set; get; }

        [Display(Name = "کد رهگیری ثبت نام مالیاتی", Prompt = "98765432", Description = "صرفا عدد")]
        [Required(ErrorMessage = "الزامی")]
        public string TaxTrackingCode { set; get; }

        [Display(Name = "نوع دستگاه", Prompt = "", Description = "نوع دستگاه بر حسب اتصال")]
        [Required(ErrorMessage = "الزامی")]
        public PosConnectionType DeviceConnectionType { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator PspPasargadViewModel(MIMS.Portal.Core.Data.Domains.Terminal entity)
        {
            var view = new PspPasargadViewModel()
            {
                Id = entity.Id,
                NationalCode = entity.Merchant.NationalCode,
                FirstName = entity.Merchant.FirstName,
                LastName = entity.Merchant.LastName,
                FatherName = entity.Merchant.FatherName,
                EnglishFirstName = entity.Merchant.EnglishFirstName,
                EnglishLastName = entity.Merchant.EnglishLastName,
                EnglishFatherName = entity.Merchant.EnglishFatherName,
                Gender = entity.Merchant.Gender,
                BirthDate = entity.Merchant.BirthDate.ToPeString("yyyy/MM/dd"),
                Phone = entity.Merchant.Phone,
                Mobile = entity.Merchant.Mobile,
                ReferrerFirstName = entity.Merchant.ReferrerFirstName,
                ReferrerLastName = entity.Merchant.ReferrerLastName,
                ReferrerPhone = entity.Merchant.ReferrerPhone,
                ReferrerMobile = entity.Merchant.ReferrerMobile,
                AccountIban = entity.BankAccount.IbanNumber,
                AccountNumber = entity.BankAccount.AccountNumber,
                BankName = entity.BankAccount.Bank.BankName,
                BankBranchCode = entity.BankAccount.BankBranchCode,
                PostalCode = entity.Store.PostalCode,
                BusinessScopeName = $@"{entity.Store.BusinessGroup.Name}-{entity.Store.BusinessScope.Name}",
                StoreName=entity.Store.StoreName,
                EnglishStoreName=entity.Store.EnglishStoreName,
                ResidenceType=entity.Store.ResidenceType,
                TaxTrackingCode=entity.Store.TaxRegistrationTrackingCode,
                DeviceConnectionType = entity.Device?.DeviceConnectionType ?? entity.ConnectionType,
            };

            return view;
        }
    }
}
