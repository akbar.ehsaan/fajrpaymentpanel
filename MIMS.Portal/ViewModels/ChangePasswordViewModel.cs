﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MIMS.Portal.ViewModels
{
    public partial class ChangePasswordViewModel
    {
        [ScaffoldColumn(false)]
        public Guid UserId { get; set; }

        [Display(Name = "گذرواژه فعلی", Prompt = "گذرواژه فعلی", Description = "گذرواژه فعلی")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Display(Name = "گذرواژه جدید", Prompt = "گذرواژه جدید", Description = "گذرواژه جدید")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "گذرواژه جدید (تکرار)", Prompt = "گذرواژه جدید (تکرار)", Description = "گذرواژه جدید (تکرار)")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "عدم همخوانی تکرار")]
        public string ConfirmNewPassword { get; set; }
    }
}
