﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels
{
    public class NewsViewModel
    {
        public Guid Id { set; get; }

        [Display(Name = "عنوان")]
        [Required(ErrorMessage = "الزامی")]
        public string Title { set; get; }

        [Display(Name = "متن")]
        [Required(ErrorMessage = "الزامی")]
        public string Body { set; get; }

        [Display(Name = "نوع خبر")]
        [Required(ErrorMessage = "الزامی")] 
        public NewsType Type { set; get; }

        [Display(Name = "تاریخ ثبت")]
        public DateTime CreatedDate { set; get; }


        //
        //-- mapping --//
        //

        public static implicit operator NewsViewModel(News entity)
        {

            var view = new NewsViewModel()
            {
                Id = entity.Id,
                Title = entity.Subject,
                Body = entity.Body,
                Type = entity.Type,
                CreatedDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator News(NewsViewModel view)
        {
            var model = new News()
            {
                Id = view.Id,
                Subject = view.Title,
                Body = view.Body,
                Type = view.Type,
            };

            return model;
        }

    }
}

