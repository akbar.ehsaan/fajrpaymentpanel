﻿using MIMS.Portal.ValueObjects.Enum;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.ViewModels
{
    public class UserEditViewModel
    {
        public Guid Id { set; get; }

        [StringLength(255)]
        [Display(Name = "نام کاربری", Prompt = "user", Description = "صرفا حروف انگلیسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[a-zA-Z0-9_.]{3,50}$", ErrorMessage = "نامعتبر")]
        [Remote("CheckUsername", "Validator", ErrorMessage = "نامعتبر")]
        public string UserName { set; get; }

        [StringLength(255)]
        [Display(Name = "گذر واژه", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        [StringLength(255)]
        [Display(Name = "دسترسی", Prompt = "", Description = "")]
        //[Required(ErrorMessage = "الزامی")]
        public string Permission { set; get; }

        [Display(Name = "نوع کاربر", Prompt = "", Description = "تعیین نوع کاربر برای دسترسی به سامانه")]
        [Required(ErrorMessage = "الزامی")]
        public UserType UserType { set; get; }

        [StringLength(255)]
        [Display(Name = "نام", Prompt = " علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string LastName { set; get; }

        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get { return $"{FirstName} {LastName}"; } }

        [StringLength(255)]
        [Display(Name = "نام پدر", Prompt = " محمد", Description = "صرفا حروف فارسی")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string FatherName { set; get; }

        [Display(Name = "جنسیت", Prompt = "", Description = "")]
        //[Required(ErrorMessage = "الزامی")]
        public Gender Gender { set; get; }

        [Display(Name = "شماره ملی", Prompt = " 0987654321", Description = "صرفا عدد و 10 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        [Remote("CheckUserNationalCode", "Validator", ErrorMessage = "نامعتبر")]
        public string NationalCode { set; get; }

        [Display(Name = "تاریخ تولد", Prompt = " 1399/01/01", Description = "صرفا تاریخ")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]{4}(-|\/)(((0[123456])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]|3[0]))|((0[789]|(10|11|12))(-|\/)(0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "نامعتبر")]
        public string BirthDate { set; get; }

        [Display(Name = "رایانامه", Prompt = "info@iran.ir", Description = "")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "همراه", Prompt = " 09121234567", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^09[0-9]{9}$", ErrorMessage = " نامعتبر، مثال 09121234567")]
        public string Mobile { set; get; }

        [Display(Name = "تلفن", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string Phone { get; set; }

        //[Display(Name = "نمابر", Prompt = " 02187654321", Description = "صرفا عدد و 11 رقم")]
        //[Required(ErrorMessage = "الزامی")]
        //[RegularExpression(@"^0\d{2}\d{5,8}$", ErrorMessage = " نامعتبر، مثال 02187654321")]
        public string Fax { get; set; }

        [Display(Name = "استان")]
        [Required(ErrorMessage = "الزامی")]
        public Guid ProvinceId { get; set; }

        [Display(Name = "شهرستان")]
        [Required(ErrorMessage = "الزامی")]
        public Guid CountyId { get; set; }

        [Display(Name = "شهر")]
        [Required(ErrorMessage = "الزامی")]
        public Guid CityId { get; set; }

        [Display(Name = "کد پستی", Prompt = "1234567890", Description = "صرفا عدد")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "نامعتبر")]
        public string PostalCode { get; set; }

        [Display(Name = "نشانی", Prompt = "تهران، آزادی...", Description = "نشانی به شکل خیابان اصلی، خیابان فرعی، جزئیات آدرس، پلاک", GroupName = "موقعیت")]
        //[Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF \d\-\،\,\(\)]+ )*[\u0600-\u06FF \d\-\،\,\(\)]+$", ErrorMessage = "نامعتبر")]
        public string Address { get; set; }

        public DateTime SubmitDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator UserEditViewModel(User entity)
        {
            var view = new UserEditViewModel()
            {
                Id = entity.Id,
                UserType = (UserType)Enum.Parse(typeof(UserType), entity.Role, true),
                UserName = entity.UserName,
                Password = entity.Password,
                Permission = entity.Permission,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                //FatherName = entity.LastName,
                //Gender = (Gender)Enum.Parse(typeof(Gender), entity.Gender),
                NationalCode = entity.NationalCode,
                //BirthDate = entity.birthDate,
                Email = entity.Email,
                Mobile = entity.Mobile,
                Phone = entity.Phone,
                //Fax = entity.Fax,
                ProvinceId = entity.Provience?.Id ?? Guid.Empty,
                CountyId = entity.County?.Id ?? Guid.Empty,
                CityId = entity.City?.Id ?? Guid.Empty,
                PostalCode = entity.PostalCode,
                Address = entity.Address,
                SubmitDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator User(UserEditViewModel view)
        {
            var model = new User()
            {
                Id = view.Id,
                Role = view.UserType.ToString(),
                UserName = view.UserName.ToLower(),
                Password = view.Password,
                Permission = view.Permission,
                FirstName = view.FirstName,
                LastName = view.LastName,
                //FatherName = view.FatherName,
                //Gender = view.Gender.ToString(),
                NationalCode = view.NationalCode,
                //BirthDate = view.BirthDate,
                Email = view.Email,
                Mobile = view.Mobile,
                Phone = view.Phone,
                //Fax = view.Fax,
                ProvienceId = view.ProvinceId,
                CountyId = view.CountyId,
                CityId = view.CityId,
                PostalCode = view.PostalCode,
                Address = view.Address,
                CreatedDate = view.SubmitDate
            };

            return model;
        }
    }
}
