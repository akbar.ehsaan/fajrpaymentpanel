﻿using MIMS.Portal.Core.Data.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels
{
    public class TicketView
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string TicketStatus { get; set; }
        public User SenderUser{ get; set; }
        public User ReciverrUser { get; set; }
        public DateTime SubmitDate { get; set; }

    
    }
}
