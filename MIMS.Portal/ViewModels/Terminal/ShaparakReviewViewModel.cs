﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels.Terminal
{
    public class ShaparakReviewViewModel
    {
        public TerminalViewModel TerminalDetails { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public Guid TerminalId { get; set; }

        [Display(Name = "وضعیت شاپرک")]
        [Required(ErrorMessage = "الزامی")]
        public ShaparakStatus ShaparakStatus { set; get; }

        [Display(Name = "شماره پذیرنده", Prompt = "123456", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{3,}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("ShaparakStatus == 5", ErrorMessage = "الزامی")]
        public string MerchantNumber { set; get; }

        [Display(Name = "شماره پایانه", Prompt = "123456", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{3,}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("ShaparakStatus == 5", ErrorMessage = "الزامی")]
        public string TerminalNumber { set; get; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        [RequiredIf("ShaparakStatus != 5", ErrorMessage = "الزامی")]
        public string ReviewComment { set; get; }
    }
}
