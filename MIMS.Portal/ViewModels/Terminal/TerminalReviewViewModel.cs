﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels.Terminal
{
    public class TerminalReviewViewModel
    {
        public TerminalViewModel TerminalDetails { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public Guid TerminalId { get; set; }

        [Display(Name = "شرکت پرداخت")]
        [Required(ErrorMessage = "الزامی")]
        public PspProvider PspProvider { set; get; }

        [Display(Name = "وضعیت", Prompt = "", Description = "")]
        public ReviewStatus Status { set; get; }

        [Display(Name = "تعیین وضعیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public bool ReviewStatus { get; set; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        [RequiredIf("ReviewStatus == false", ErrorMessage = "الزامی")]
        public string ReviewComment { set; get; }
    }
}
