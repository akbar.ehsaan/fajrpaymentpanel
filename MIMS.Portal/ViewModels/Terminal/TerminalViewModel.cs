﻿using System;
using System.ComponentModel.DataAnnotations;
using UoN.ExpressiveAnnotations.NetCore.Attributes;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.ViewModels.Terminal
{
    public class TerminalViewModel
    {
        public Guid Id { set; get; }

        [Display(Name = "نوع پایانه")]
        public TerminalType TerminalType { set; get; } = TerminalType.TransactionDispatch;

        [Display(Name = "شرکت پرداخت")]
        [Required(ErrorMessage = "الزامی")]
        public PspProvider? PspProvider { set; get; }

        [Required(ErrorMessage = "الزامی")]
        public Guid MerchantId { set; get; }

        [Display(Name = "نام پذیرنده")]
        public string MerchantFullName { set; get; }

        [Display(Name = "شماره ملی پذیرنده")]
        public string MerchantNationalCode { set; get; }

        [Display(Name = "فروشگاه")]
        [Required(ErrorMessage = "الزامی")]
        public Guid StoreId { set; get; }

        [Display(Name = "فروشگاه")]
        public string StoreFullName { set; get; }

        [Display(Name = "شماره حساب")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BankAccountId { set; get; }

        [Display(Name = "شماره حساب")]
        public string BankAccountFullName { set; get; }

        [Display(Name = "نوع دستگاه", Prompt = "", Description = "نوع دستگاه بر حسب اتصال")]
        [Required(ErrorMessage = "الزامی")]
        public PosConnectionType DeviceConnectionType { set; get; }

        [Display(Name = "برند دستگاه", Prompt = "", Description = "")]
        [RequiredIf("Status == 2", ErrorMessage = "الزامی")]
        //[AssertThat("Status == 2", ErrorMessage = "الزامی")]
        public Guid? DeviceBrandId { set; get; }
        public string DeviceBrandName { set; get; }

        [Display(Name = "مدل دستگاه", Prompt = "", Description = "")]
        [RequiredIf("Status == 2", ErrorMessage = "الزامی")]
        //[AssertThat("Status == 2", ErrorMessage = "الزامی")]
        public Guid? DeviceModelId { set; get; }
        public string DeviceModelName { set; get; }

        [Display(Name = "سریال دستگاه", Prompt = "", Description = "")]
        [RequiredIf("Status == 2", ErrorMessage = "الزامی")]
        [AssertThat("Status == 2", ErrorMessage = "الزامی")]
        [RequiredIf("DeviceBrandId != null ", ErrorMessage = "الزامی")]
        [RequiredIf("DeviceModelId != null ", ErrorMessage = "الزامی")]
        public Guid? DeviceId { set; get; }

        public string DeviceSerial { set; get; }

        [Display(Name = "شماره پذیرنده", Prompt = "123456", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{3,}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("Status == 2", ErrorMessage = "الزامی")]
        public string MerchantNumber { set; get; }

        [Display(Name = "شماره پایانه", Prompt = "123456", Description = "صرفا عدد")]
        [RegularExpression(@"^\d{3,}$", ErrorMessage = "نامعتبر")]
        [RequiredIf("Status == 2", ErrorMessage = "الزامی")]
        public string TerminalNumber { set; get; }

        [Display(Name = "وضعیت", Prompt = "", Description = "وضعیت ترمینال")]
        public TerminalStatus Status { set; get; } = TerminalStatus.Pending;

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        public string ReviewComment { set; get; }

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator TerminalViewModel(Core.Data.Domains.Terminal entity)
        {
            var view = new TerminalViewModel()
            {
                Id = entity.Id,
                PspProvider = entity.PspProvider,
                TerminalType = TerminalType.TransactionDispatch,
                MerchantId = entity.Merchant?.Id ?? entity.MerchantId,
                MerchantFullName = $"{entity.Merchant?.FirstName} {entity.Merchant?.LastName}",
                MerchantNationalCode = entity.Merchant?.NationalCode,
                StoreId = entity.Store?.Id ?? entity.StoreId,
                StoreFullName = entity.Store?.StoreName,
                BankAccountId = entity.BankAccount?.Id ?? entity.AccountNumberId,
                BankAccountFullName = $"{entity.BankAccount?.IbanNumber} - {entity.BankAccount?.Bank?.BankName}",
                DeviceConnectionType = entity.Device?.DeviceConnectionType ?? entity.ConnectionType,
                DeviceBrandId = entity.Brand?.Id ?? entity.PosDeviceBrandId ?? Guid.Empty,
                DeviceBrandName = entity.Brand?.Name,
                DeviceModelId = entity.Model?.Id ?? entity.PosDeviceModelId ?? Guid.Empty,
                DeviceModelName = entity.Model?.Name,
                DeviceId = entity.Device?.Id ?? entity.PosDeviceId ?? Guid.Empty,
                DeviceSerial = entity.Device?.DeviceSerial,
                MerchantNumber = entity.MerchantNumber,
                TerminalNumber = entity.TerminalNumber,
                Status = entity.TerminalStatus,
                ReviewComment = entity.ReviewComment,
                CreatedDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator Core.Data.Domains.Terminal(TerminalViewModel view)
        {
            var model = new Core.Data.Domains.Terminal()
            {
                Id = view.Id,
                Type = view.TerminalType,
                PspProvider = view.PspProvider,
                MerchantNumber = view.MerchantNumber,
                TerminalNumber = view.TerminalNumber,
                MerchantId = view.MerchantId,
                AccountNumberId = view.BankAccountId,
                StoreId = view.StoreId,
                ConnectionType = view.DeviceConnectionType,
                PosDeviceBrandId = view.DeviceBrandId,
                PosDeviceModelId = view.DeviceModelId,
                PosDeviceId = view.DeviceModelId,
                TerminalStatus = view.Status,
                ReviewComment = view.ReviewComment,
            };

            return model;
        }
    }
}
