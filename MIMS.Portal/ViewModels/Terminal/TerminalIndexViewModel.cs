﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels.Terminal
{
    public class TerminalIndexViewModel
    {
        public TerminalFilterViewModel Filters { get; set; }

        public IEnumerable<KeyValuePair<TerminalStatus, long>> Stats { get; set; }

        public IEnumerable<TerminalViewModel> Terminals { get; set; } = Enumerable.Empty<TerminalViewModel>();
    }
}
