﻿using System;
using System.ComponentModel.DataAnnotations;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.ViewModels.BankAccount
{
    public class BankAccountViewModel
    {
        public Guid Id { set; get; }

        [Required(ErrorMessage = "الزامی")]
        public Guid MerchantId { set; get; }

        [Display(Name = "نام و نام خانوادگی پذیرنده")]
        public string MerchantFullName { set; get; }

        [Display(Name = "شماره ملی پذیرنده")]
        public string MerchantNationalCode { set; get; }

        [Display(Name = "بانک")]
        [Required(ErrorMessage = "الزامی")]
        public Guid BankId { get; set; }

        [Display(Name = "بانک")]
        public string BankName { set; get; }

        [Display(Name = "کد شعبه")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^\d{1,10}$", ErrorMessage = "نامعتبر")]
        public string BankBranchCode { get; set; }

        [Display(Name = "شماره حساب", Prompt = "", Description = "صرفا شماره حساب معتبر بانکی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^[0-9]+(-|.|\/)+([0-9]+)+$", ErrorMessage = "نامعتبر")]
        public string AccountNumber { get; set; }

        [Display(Name = "شماره شبا", Prompt = "IR062960000000100324200001", Description = "صرفا شماره شبای بانکی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^(IR|ir)+([0-9]{24})$", ErrorMessage = "نامعتبر")]
        public string AccountIban { get; set; }

        public string BankAccountFullName { get { return $"{AccountIban} [{BankName}]"; } }

        [StringLength(255)]
        [Display(Name = "نام صاحب حساب", Prompt = " علی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string AccountHolderFirstName { set; get; }

        [StringLength(255)]
        [Display(Name = "نام خانوادگی صاحب حساب", Prompt = " احمدی", Description = "صرفا حروف فارسی")]
        [Required(ErrorMessage = "الزامی")]
        [RegularExpression(@"^([\u0600-\u06FF\s]+){2,}$", ErrorMessage = "نامعتبر")]
        public string AccountHolderLastName { set; get; }

        [Display(Name = "صاحب حساب", Prompt = " علی", Description = "صرفا حروف فارسی")]
        public string AccountHolderFullName { get { return $"{AccountHolderFirstName} {AccountHolderLastName}"; } }

        [Display(Name = "وضعیت", Prompt = "", Description = "")]
        public ReviewStatus Status { set; get; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        public string ReviewComment { set; get; }

        [Display(Name = "تاریخ ثبت", Prompt = "", Description = "")]
        public DateTime CreatedDate { set; get; }

        //
        //-- mapping --//
        //

        public static implicit operator BankAccountViewModel(Core.Data.Domains.BankAccount entity)
        {
            var view = new BankAccountViewModel()
            {
                Id = entity.Id,
                MerchantId = entity.Merchant?.Id ?? entity.MerchantId,
                MerchantFullName = entity.Merchant.FullName,
                MerchantNationalCode = entity.Merchant?.NationalCode,
                BankId = entity.Bank?.Id ?? entity.BankId,
                BankName = entity.Bank?.BankName,
                BankBranchCode = entity.BankBranchCode,
                AccountNumber = entity.AccountNumber,
                AccountIban = entity.IbanNumber,
                AccountHolderFirstName = entity.AccountHolderFirstName,
                AccountHolderLastName = entity.AccountHolderLastName,
                Status = entity.Status,
                ReviewComment = entity.ReviewComment,
                CreatedDate = entity.CreatedDate
            };

            return view;
        }

        public static implicit operator Core.Data.Domains.BankAccount(BankAccountViewModel view)
        {

            var entity = new Core.Data.Domains.BankAccount()
            {
                Id = view.Id,
                MerchantId = view.MerchantId,
                BankId = view.BankId,
                BankBranchCode = view.BankBranchCode,
                AccountNumber = view.AccountNumber,
                IbanNumber = view.AccountIban.ToUpper(),
                AccountHolderFirstName = view.AccountHolderFirstName,
                AccountHolderLastName = view.AccountHolderLastName,
                Status = view.Status,
                ReviewComment = view.ReviewComment,
            };

            return entity;
        }
    }
}
