﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UoN.ExpressiveAnnotations.NetCore.Attributes;

namespace MIMS.Portal.ViewModels.BankAccount
{
    public class BankAccountReviewViewModel
    {
        public BankAccountViewModel BankAccountDetails { get; set; }

        [Required(ErrorMessage = "الزامی")]
        public Guid BankAccountId { get; set; }

        [Display(Name = "وضعیت", Prompt = "", Description = "")]
        public ReviewStatus Status { set; get; }

        [Display(Name = "تعیین وضعیت", Prompt = "", Description = "")]
        [Required(ErrorMessage = "الزامی")]
        public bool ReviewStatus { get; set; }

        [Display(Name = "توضیحات", Prompt = "توضیحات بررسی", Description = "درج توضیحات بررسی در صورت نیاز")]
        [RequiredIf("ReviewStatus == false", ErrorMessage = "الزامی")]
        public string ReviewComment { set; get; }
    }
}
