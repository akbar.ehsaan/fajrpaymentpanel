﻿using MIMS.Portal.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.ViewModels.BankAccount
{
    public class BankAccountIndexViewModel
    {
        public BankAccountFilterViewModel Filters { get; set; }

        public IEnumerable<KeyValuePair<ReviewStatus, long>> Stats { get; set; }

        public IEnumerable<BankAccountViewModel> BankAccounts { get; set; } = Enumerable.Empty<BankAccountViewModel>();
    }
}
