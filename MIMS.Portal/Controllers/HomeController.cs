﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MIMS.Portal.Models;
using Microsoft.AspNetCore.Authorization;
using MIMS.Portal.Service;
using System.Net;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Text.Json;
using MIMS.Portal.ViewModels;
using MIMS.Portal.ValueObjects;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Controllers
{

    public class HomeController : Controller
    {
        private ITicketService _ticketService;
        private INewsService _newsService;
        private IMerchantService _merchantService;
        private IBankAccountService _bankAccountService;

        public HomeController(ITicketService ticketservice, INewsService newsService, IMerchantService merchantService, IBankAccountService bankAccountService)
        {
            _ticketService = ticketservice;
            _newsService = newsService;
            _merchantService = merchantService;
            _bankAccountService = bankAccountService;
        }

        [Authorize(AuthenticationSchemes = "Bearer"/*, Roles = "admin"*/)]

        public IActionResult Index()
        {
            var userClaims = HttpContext.User.Identity as ClaimsIdentity;

            ViewBag.UserId = new Guid(userClaims.FindFirst("userId").Value);
            ViewBag.UserRole = userClaims.FindFirst("UserRole").Value;

            bool a = User.IsInRole("admin");



            ViewBag.news = _newsService.FindAll();

            var q = _ticketService.TicketDailyReport();
            ViewBag.datasets = JsonSerializer.Serialize(q.Select(i => i.value));
            ViewBag.labels = JsonSerializer.Serialize(q.Select(i => i.key));



            return View();
        }

        public IActionResult Upgrade()
        {

            //var merchants = _merchantService.GetAll();
            //foreach (var merchant in merchants)
            //{
            //    var fullName = Newtonsoft.Json.JsonConvert.DeserializeObject<FullName>(merchant.ReferrerFullname);
            //    merchant.ReferrerFirstName = fullName.FirstName;
            //    merchant.ReferrerLastName = fullName.LastName;
            //    _merchantService.Update(merchant);
            //}

            //var bankAccounts = _bankAccountService.GetAll();
            //foreach (var account in bankAccounts)
            //{
            //    var fullName = Newtonsoft.Json.JsonConvert.DeserializeObject<FullName>(account.AccountHolderFullName);
            //    account.AccountHolderFirstName = fullName.FirstName;
            //    account.AccountHolderLastName = fullName.LastName;
            //    _bankAccountService.Update(account);
            //}



            return Ok();
        }




    }
}
