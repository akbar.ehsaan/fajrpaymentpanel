﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class MessageController : Controller
    {
        IMessageService _messageService;
        IUsersService _userservice;
        public MessageController(IMessageService messageService, IUsersService userService)
        {
            _messageService = messageService;
            _userservice = userService;
        }
        public IActionResult Index(Guid id)
        {
            if (id != Guid.Empty)
                ViewBag.number = _userservice.FindById(id).Mobile;
            return View();
        }
        [HttpPost]
        public IActionResult Create(string number, string text, string type)
        {
            List<string> numbers = new List<string>();
            if (type == "0") numbers.Add(number);
            if (type == "1") numbers = _userservice.FindUser().Select(i => i.Mobile).ToList();
            if (type == "2") numbers = _userservice.FindByRole("agent").Select(i => i.Mobile).ToList();
            if (type == "3") numbers = _userservice.FindByRole("merchant").Select(i => i.Mobile).ToList();

            _messageService.Send(numbers, text);
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return View("Index");
        }
    }
}
