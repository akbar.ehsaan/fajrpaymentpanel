﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.Store;
using MIMS.Portal.Core.Data.Domains;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class StoresController : Controller
    {
        IMerchantService _merchantService;
        IStoreService _storeService;
        IStoreScopeService _businessScopeService;
        IGeoService _countrySectionService;
        IFilesManagerService _filesManagerService;
        private readonly int _maxFileSize = 1500000;

        public StoresController(IStoreScopeService storeScopeService, IMerchantService merchantService, IStoreService businessService,
            IGeoService countrySectionService, IFilesManagerService filesManagerService)
        {
            _merchantService = merchantService;
            _storeService = businessService;
            _businessScopeService = storeScopeService;
            _countrySectionService = countrySectionService;
            _filesManagerService = filesManagerService;
        }

        [AllowAnonymous]
        public JsonResult GetScopes(Guid id)
        {
            var scopes = Enumerable.Empty<BusinessScope>();

            scopes = _businessScopeService.GetAll().Where(x => x.ParentId == id);

            var items = scopes.OrderBy(i => i.Name).Select(i => new { Value = i.Id, Text = i.Name });
            return Json(items);
        }

        public IActionResult Index(Guid? id)
        {
            //if (id == Guid.Empty)
            //    return BadRequest();

            //var merchantId = id;
            //var merchant = _merchantService.FindById(merchantId);

            //if (merchant == null)
            //    return NotFound();

            //ViewBag.MerchantId = merchantId;

            //return View(_storeService.FindByMerchantId(merchantId));


            //var store = Enumerable.Empty<StoreViewModel>();
            //store = _storeService.GetAll().ToList().ConvertAll(x => (StoreViewModel) x);
            //    return View(store);

            var storesIndex = new StoreIndexViewModel();


            Guid userId = Guid.Empty;

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            if (User.IsInRole("Admin"))
            {
                storesIndex.Stats = _storeService.StatsByReviewStatus(null);
                storesIndex.Stores = _storeService.Filter(include: x => x.Include(i => i.Merchant).Include(i => i.BusinessScope).ThenInclude(b => b.Parent).Include(i => i.Provience).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (StoreViewModel)x);

                //if (id == null || id == Guid.Empty)
                //    merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                //else
                //    merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }
            else
            {
                storesIndex.Stats = _storeService.StatsByReviewStatus(userId);
                storesIndex.Stores = _storeService.GetByUserId(userId).ToList().ConvertAll(x => (StoreViewModel)x);
            }

            return View(storesIndex);
        }

        [HttpPost]
        public IActionResult Index(Guid? id, StoreFilterViewModel? filters)
        {
            var storesIndex = new StoreIndexViewModel();

            Guid userId = id.GetValueOrDefault();

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            //Expression<Func<Merchant, bool>> predicate = null;
            var predicate = PredicateBuilder.True<Store>();
            var andCriteria = new List<Predicate<Store>>();
            var orCriteria = new List<Predicate<Store>>();
            var orParams = PredicateBuilder.False<Store>();
            var andParams = PredicateBuilder.True<Store>();

            if (!string.IsNullOrWhiteSpace(filters.NationalCode))
            {
                orCriteria.Add(f => f.Merchant.NationalCode == filters.NationalCode);
                orParams = orParams.Or(f => f.Merchant.NationalCode == filters.NationalCode);
            }

            if (!string.IsNullOrWhiteSpace(filters.FirstName))
            {
                orCriteria.Add(f => f.Merchant.FirstName.Contains(filters.FirstName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.FirstName.Contains(filters.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(filters.LastName))
            {
                orCriteria.Add(f => f.Merchant.LastName.Contains(filters.LastName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.LastName.Contains(filters.LastName));
            }

            if (filters.Status != null)
            {
                orCriteria.Add(f => f.Status == filters.Status);
                orParams = orParams.Or(f => f.Status == filters.Status);
            }

            if (!User.IsInRole("Admin"))
            {
                andCriteria.Add(f => f.CreatorUserId == userId);
                andParams = andParams.And(f => f.CreatorUserId == userId);
                andParams = andParams.And(orParams);
            }


            predicate = c => andCriteria.All(pred => pred(c)) && orCriteria.Any(pred => pred(c));
            predicate = !User.IsInRole("Admin") ? andParams : orParams;


            if (User.IsInRole("Admin"))
            {
                if (id.GetValueOrDefault() == Guid.Empty)
                {
                    storesIndex.Stats = _storeService.StatsByReviewStatus(null);
                    storesIndex.Stores = _storeService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BusinessScope).ThenInclude(b => b.Parent).Include(i=>i.Provience).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (StoreViewModel)x);
                    //merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                else
                {
                    storesIndex.Stats = _storeService.StatsByReviewStatus(userId);
                    storesIndex.Stores = _storeService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BusinessScope).ThenInclude(b => b.Parent).Include(i => i.Provience).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (StoreViewModel)x);
                    //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
            }
            else
            {
                storesIndex.Stats = _storeService.StatsByReviewStatus(userId);
                storesIndex.Stores = _storeService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BusinessScope).ThenInclude(b => b.Parent).Include(i => i.Provience).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (StoreViewModel)x);

                //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }

            return View(storesIndex);
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var storeId = id;
            var store = _storeService.GetById(storeId);

            if (store == null)
                return NotFound();

            var model = (StoreViewModel)store;

            var merchantFiles = _filesManagerService.FindByParentId(store.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Store).OrderByDescending(f => f.CreatedDate);

            var storeDocument = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument).FirstOrDefault();
            model.StoreDocumentUrl = storeDocument?.FileUrl;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var storeViewModel = new StoreViewModel();
            storeViewModel.MerchantId = merchant.Id;
            storeViewModel.MerchantFullName = merchant.FullName;
            storeViewModel.MerchantNationalCode = merchant.NationalCode;

            //ViewBag.BusinessScopes = new SelectList(GroupBusinessScopes(_businessScopeService.GetAll()), "Value", "Text", null, "Group.Name");
            ViewBag.BusinessGroups = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == Guid.Empty || x.ParentId == null).OrderBy(i => i.Name), "Id", "Name");
            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name");

            return View(storeViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(StoreViewModel model, bool next)
        {
            var isFileSizeValid = true;

            if (model.StoreDocument != null && model.StoreDocument.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.StoreDocument), "حجم فایل نامعتبر");
            }

            if (ModelState.IsValid && isFileSizeValid)
            {
                model.Id = Guid.Empty;
                model.Status = ReviewStatus.Pending;
                var store = (Store)model;

                _storeService.Create(store);
                _filesManagerService.SaveFile(model.StoreDocument, FileSource.Store, FileType.StoreDocument, store.Id);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "فروشگاه",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                if (next)
                    return RedirectToAction("Create", "Terminals", new { Id = model.MerchantId, Next = next });

                return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
            }

            var merchant = _merchantService.GetById(model.MerchantId);

            if (merchant == null)
                return NotFound();

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;
            model.MerchantNationalCode = merchant.NationalCode;

            var districts = _countrySectionService.GetSectionsById(model.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", model.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(model.ProvinceId), "Id", "Name", model.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.BusinessGroups = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == Guid.Empty || x.ParentId == null), "Id", "Name", model.BusinessGroupId);
            ViewBag.BusinessScopes = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == model.BusinessGroupId), "Id", "Name", model.BusinessScopeId);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var storeId = id;
            var store = new StoreEditViewModel();

            store = _storeService.GetById(storeId);

            var districts = _countrySectionService.GetSectionsById(store.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            var merchantFiles = _filesManagerService.FindByParentId(store.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Store).OrderByDescending(f => f.CreatedDate);

            var storeDocument = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument).FirstOrDefault();
            store.StoreDocumentId = storeDocument?.Id ?? Guid.Empty;
            store.StoreDocumentUrl = storeDocument?.FileUrl;

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", store.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(store.ProvinceId), "Id", "Name", store.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", store.CityId);
            ViewBag.BusinessGroups = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == Guid.Empty || x.ParentId == null), "Id", "Name", store.BusinessGroupId);
            ViewBag.BusinessScopes = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == store.BusinessGroupId), "Id", "Name", store.BusinessScopeId);

            return View(store);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(StoreEditViewModel model)
        {
            var isFileSizeValid = true;

            if (model.StoreDocument != null && model.StoreDocument.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.StoreDocument), "حجم فایل نامعتبر");
            }

            if (ModelState.IsValid && isFileSizeValid)
            {
                var store = (Store)model;
                store.Status = ReviewStatus.Pending;
                _storeService.Edit(store);

                if (model.StoreDocument != null)
                {
                    _filesManagerService.SaveFile(model.StoreDocument, FileSource.Store, FileType.StoreDocument, store.Id);
                }
                else
                {
                    _filesManagerService.SetStatus(model.StoreDocumentId, ReviewStatus.Pending);
                }

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "فروشگاه",
                    Content = "اطلاعات با موفقیت ویرایش شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
            }

            var merchant = _merchantService.GetById(model.MerchantId);

            if (merchant == null)
                return NotFound();

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;
            model.MerchantNationalCode = merchant.NationalCode;

            var merchantFiles = _filesManagerService.FindByParentId(model.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Store).OrderByDescending(f => f.CreatedDate);

            var storeDocument = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument).FirstOrDefault();
            model.StoreDocumentId = storeDocument?.Id ?? Guid.Empty;
            model.StoreDocumentUrl = storeDocument?.FileUrl;

            var districts = _countrySectionService.GetSectionsById(model.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", model.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(model.ProvinceId), "Id", "Name", model.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.BusinessGroups = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == Guid.Empty || x.ParentId == null), "Id", "Name", model.BusinessGroupId);
            ViewBag.BusinessScopes = new SelectList(_businessScopeService.GetAll().Where(x => x.ParentId == model.BusinessGroupId), "Id", "Name", model.BusinessScopeId);

            return View(model);
        }

        public ActionResult Pending()
        {
            var stores = Enumerable.Empty<StoreViewModel>();

            stores = _storeService.GetAll().Where(m => (m.Status == ValueObjects.Enum.ReviewStatus.Pending || m.Status == ValueObjects.Enum.ReviewStatus.Unknown) && (m.Merchant.Status == ReviewStatus.PreApproved || m.Merchant.Status == ReviewStatus.Approved)).ToList().ConvertAll(x => (StoreViewModel)x);

            return View(stores);
        }

        public ActionResult Review(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var storeId = id;
            var store = _storeService.GetById(storeId);

            if (store == null)
                return NotFound();

            var merchantFiles = _filesManagerService.FindByParentId(store.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Store).OrderByDescending(f => f.CreatedDate);

            var storeDetails = (StoreViewModel)store;
            var model = new StoreReviewViewModel();

            model.StoreDetails = storeDetails;

            var storeDocument = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            model.StoreDocumentId = storeDocument?.Id ?? Guid.Empty;
            model.StoreDocumentUrl = storeDocument?.FileUrl;


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Review(StoreReviewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model;

                var store = _storeService.GetById(review.StoreId);
                store.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                store.ReviewComment = review.ReviewComment;

                var storeFiles = _filesManagerService.FindByParentId(store.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

                var storeDocument = storeFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();

                if (storeDocument != null)
                {
                    storeDocument.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                }

                _storeService.Edit(store);
                //_filesManagerService.Update();

                //return RedirectToAction(nameof(Pending));
                return RedirectToAction("details", "merchants", new { id = store.MerchantId });


            }

            return View(model);
        }

        public IActionResult Delete(Guid id, string merchantid)
        {
            if (_storeService.Delete(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("Index", new { merchantid = merchantid });
        }


        private List<SelectListItem> GroupBusinessScopes(List<BusinessScope> items)
        {
            List<SelectListItem> groupedItems = new List<SelectListItem>();

            //Loop and add the Parent Nodes
            var parents = items.Where(i => i.ParentId == Guid.Empty || i.ParentId == null).OrderBy(i => i.Name);
            foreach (var parent in parents)
            {
                //Create the Group
                SelectListGroup scopeGroup = new SelectListGroup() { Name = parent.Name };

                //Loop and add the Items
                var children = items.Where(i => i.ParentId == parent.Id).OrderBy(i => i.Name);
                foreach (var child in children)
                {
                    groupedItems.Add(new SelectListItem
                    {
                        Group = scopeGroup,
                        Value = child.Id.ToString(),
                        Text = child.Name,
                        //Selected = SelectedItems != null ? SelectedItems.Contains(insuranceType.InsuranceTypeID.ToString()) : false
                    });
                }
            }

            return groupedItems;

        }
    }
}
