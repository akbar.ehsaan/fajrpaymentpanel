﻿using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class CountrySectionController : Controller
    {
        readonly private IGeoService _countrySectionService;

        public CountrySectionController(IGeoService countrySectionService)
        {
            _countrySectionService = countrySectionService;
        }

        [AllowAnonymous]
        public JsonResult GetSections(Guid id)
        {
            var section = _countrySectionService.FindById(id).FirstOrDefault();
            var sections = Enumerable.Empty<GeoSection>();

            if (section.Type == "County")
            {
                var districts = _countrySectionService.GetSectionsById(id);
                var cities = new List<GeoSection>();

                foreach (var dis in districts)
                {
                    var children = _countrySectionService.GetSectionsById(dis.Id);
                    cities.AddRange(children.Where(x => x.Type == "City").ToList());
                }

                sections = cities;
            }
            else
            {
                sections = _countrySectionService.GetSectionsById(id);
            }

            var items = sections.OrderBy(i => i.Name).Select(i => new { Value = i.Id, Text = i.Name });
            return Json(items);
        }
    }
}
