﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Service;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels;
using MIMS.Portal.ViewModels.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class OperationsController : Controller
    {
        IMerchantService _merchantService;
        IBankAccountService _bankAccountService;
        IStoreService _storeService;
        ITerminalService _terminalService;
        IDeviceService _deviceService;
        IPosBrandService _posBrandService;

        public OperationsController(ITerminalService terminalService, IMerchantService merchantService, IBankAccountService bankAccountService, IStoreService storeService, IDeviceService deviceService, IPosBrandService posBrandService)
        {
            _terminalService = terminalService;
            _merchantService = merchantService;
            _bankAccountService = bankAccountService;
            _storeService = storeService;
            _deviceService = deviceService;
            _posBrandService = posBrandService;
        }

        public IActionResult Index(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest();

            var status = id;

            if (!Enum.TryParse(status.Replace("-", ""), true, out TerminalStatus terminalStatus))
                return BadRequest();

          
            var terminals = Enumerable.Empty<TerminalViewModel>();

            if (User.IsInRole("Admin"))
            {
                terminals = _terminalService.GetByTerminalStatus( terminalStatus).ToList().ConvertAll(x => (TerminalViewModel)x);
            }
            else
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                var userId = new Guid(x.FindFirst("userId").Value);
                terminals = _terminalService.GetByUserId(userId).Where(i => i.TerminalStatus == terminalStatus).ToList().ConvertAll(x => (TerminalViewModel)x);
            }

            var actionName = string.Empty;

            switch (terminalStatus)
            {
                case TerminalStatus.Sent:
                    actionName = "Shaparak";
                    break;
                case TerminalStatus.Approved:
                    actionName = "AssignDevice";
                    break;
                case TerminalStatus.DeviceAssigned:
                    actionName = "SubmitDevice";
                    break;
                case TerminalStatus.DeviceSubmitted:
                    actionName = "InitialDevice";
                    break;
                case TerminalStatus.InitialSetup:
                    actionName = "DeliveryDevice";
                    break;
                default:
                    //
                    break;
            }

            ViewBag.ActionName = actionName;

            return View(terminals);
        }

        [HttpGet]
        public IActionResult Shaparak(Guid id)
        {
            //Action on Sent status (or AwaitingForShaparakResponse)

            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            //if (terminal.TerminalStatus != TerminalStatus.Sent)
            //    return NotFound();

            var model = new ShaparakReviewViewModel();
            model.TerminalDetails = terminal;
            return RedirectToAction("details", "merchants", new { id = terminal.MerchantId });


            //return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Shaparak(ShaparakViewModel model)
        {
            var view = new ShaparakReviewViewModel();

            if (ModelState.IsValid)
            {
                var review = model;

                var terminal = _terminalService.FindById(review.TerminalId);
                view.TerminalDetails = terminal;

                terminal.ReviewComment = review.ReviewComment;

                if (review.ShaparakStatus == ShaparakStatus.Approved)
                {
                    terminal.TerminalStatus = TerminalStatus.Approved;
                    terminal.MerchantNumber = review.MerchantNumber;
                    terminal.TerminalNumber = review.TerminalNumber;
                }

                if (review.ShaparakStatus == ShaparakStatus.RejectedByWhois)
                {
                    terminal.TerminalStatus = TerminalStatus.RejectedByWhois;

                    var merchant = _merchantService.GetById(terminal.MerchantId);
                    merchant.Status = ReviewStatus.Rejected;
                    merchant.ReviewComment = review.ReviewComment;

                    _merchantService.Edit(merchant);
                }

                if (review.ShaparakStatus == ShaparakStatus.RejectedByBankAccount)
                {
                    terminal.TerminalStatus = TerminalStatus.RejectedByBankAccount;

                    var bankAccount = _bankAccountService.GetById(terminal.AccountNumberId);
                    bankAccount.Status = ReviewStatus.Rejected;
                    bankAccount.ReviewComment = review.ReviewComment;

                    _bankAccountService.Edit(bankAccount);
                }

                if (review.ShaparakStatus == ShaparakStatus.RejectedByStore)
                {
                    terminal.TerminalStatus = TerminalStatus.RejectedByStore;

                    var store = _storeService.GetById(terminal.StoreId);
                    store.Status = ReviewStatus.Rejected;
                    store.ReviewComment = review.ReviewComment;

                    _storeService.Edit(store);
                }

                if (review.ShaparakStatus == ShaparakStatus.RejectedByOther)
                {
                    terminal.TerminalStatus = TerminalStatus.RejectedByOther;

                    var merchant = _merchantService.GetById(terminal.MerchantId);
                    merchant.Status = ReviewStatus.Rejected;
                    merchant.ReviewComment = review.ReviewComment;

                    _merchantService.Edit(merchant);

                    var bankAccount = _bankAccountService.GetById(terminal.AccountNumberId);
                    bankAccount.Status = ReviewStatus.Rejected;
                    bankAccount.ReviewComment = review.ReviewComment;

                    _bankAccountService.Edit(bankAccount);

                    var store = _storeService.GetById(terminal.StoreId);
                    store.Status = ReviewStatus.Rejected;
                    store.ReviewComment = review.ReviewComment;

                    _storeService.Edit(store);
                }

                _terminalService.Edit(terminal);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "تعیین وضعیت",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction(nameof(Index), new { id = TerminalStatus.Sent });
            }

            return View(view);
        }

        [HttpGet]
        public IActionResult AssignDevice(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            if (terminal.TerminalStatus != TerminalStatus.Approved)
                return NotFound();

            var model = new AssignDeviceViewModel();
            model.TerminalDetails = terminal;
            model.DeviceConnectionType = terminal.ConnectionType;

            ViewBag.DeviceBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AssignDevice(AssignDeviceViewModel model)
        {

            if (ModelState.IsValid)
            {
                var terminal = _terminalService.FindById(model.TerminalId);

                terminal.TerminalStatus = TerminalStatus.DeviceAssigned;
                terminal.PosDeviceBrandId = model.DeviceBrandId;
                terminal.PosDeviceModelId = model.DeviceModelId;
                terminal.PosDeviceId = model.DeviceId;
                _terminalService.Edit(terminal);

                var device = _deviceService.FindById(model.DeviceId.GetValueOrDefault());
                device.DeviceStatus = PosStatus.TerminalAssigned;
                _deviceService.Update(device);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "تخصیص دستگاه",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                //return RedirectToAction(nameof(Index), new { id = TerminalStatus.Approved });
                return RedirectToAction("details", "merchants", new { id = terminal.MerchantId });

            }


            return View(model);
        }


        public IActionResult SubmitDevice(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            if (terminal.TerminalStatus != TerminalStatus.DeviceAssigned)
                return NotFound();

            terminal.TerminalStatus = TerminalStatus.DeviceSubmitted;
            _terminalService.Edit(terminal);

            var toastr = new Toastr
            {
                Type = ToastrType.Success,
                Subject = "ثبت دستگاه",
                Content = "اطلاعات با موفقیت ثبت شد"
            };
            Toastrs.ToastrsList.Add(toastr);

            return RedirectToAction(nameof(Index), new { id = TerminalStatus.DeviceAssigned });

        }

        public IActionResult InitialDevice(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            //if (terminal.TerminalStatus != TerminalStatus.DeviceSubmitted)
            //    return NotFound();

            terminal.TerminalStatus = TerminalStatus.InitialSetup;
            _terminalService.Edit(terminal);

            var toastr = new Toastr
            {
                Type = ToastrType.Success,
                Subject = "فعالسازی",
                Content = "اطلاعات با موفقیت ثبت شد"
            };
            Toastrs.ToastrsList.Add(toastr);

            return RedirectToAction("details", "merchants", new { id = terminal.MerchantId });

        }

        public IActionResult DeliveryDevice(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            //if (terminal.TerminalStatus != TerminalStatus.InitialSetup)
            //    return NotFound();

            terminal.TerminalStatus = TerminalStatus.Delivery;
            _terminalService.Edit(terminal);

            var toastr = new Toastr
            {
                Type = ToastrType.Success,
                Subject = "نصب و تحویل",
                Content = "اطلاعات با موفقیت ثبت شد"
            };
            Toastrs.ToastrsList.Add(toastr);

            return RedirectToAction(nameof(Index), new { id = TerminalStatus.InitialSetup });

        }
    }
}
