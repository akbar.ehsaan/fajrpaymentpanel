﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class InvoiceController : Controller
    {
        IInvoiceService _invoiceService;
        IUsersService _userService;
        public InvoiceController(IInvoiceService invoiceService, IUsersService userservice)
        {
            _invoiceService = invoiceService;
            _userService = userservice;

        }
        public IActionResult Index(string id)
        {
            var x = HttpContext.User.Identity as ClaimsIdentity;

           if (id=="" || id==null) id= x.FindFirst("userId").Value;
           
            return View(_invoiceService.FindByUserId(id));
        }
        [HttpPost]
        public async Task<IActionResult> Create(Invoice invoice)
        {
            var x = HttpContext.User.Identity as ClaimsIdentity;

            invoice.User = _userService.FindById(new Guid((x.FindFirst("userId").Value)));
            

            string callback = Url.Action(nameof(PaymentForPackageVerify));
            var payment = await new Zarinpal.Payment("53a85fc4-d22c-11e9-9745-000c295eb8fc", 100).PaymentRequest("عنوان",callback);
            if (payment.Status == 100)
            {
                invoice.BankOrderId = payment.Authority;
                invoice.PaymentState = "Pending";
                _invoiceService.Add(invoice);
                return Redirect(payment.Link);
            }
            else
            {
                TempData["Error"] = "اشکال در پرداخت رخ داده است";
                return RedirectToAction("Index", new { id = x.FindFirst("userId").Value });
            }
          

        }

        public async Task<IActionResult> PaymentForPackageVerify(string Authority)
        {

            //واقعی
            var verification = await new Zarinpal.Payment("53a85fc4-d22c-11e9-9745-000c295eb8fc", 100).Verification(Authority);
            //تستی
            // var verification = await new ZarinpalSandbox.Payment(1000).Verification(Authority);

            //ارسال به صفحه خطا
            if (verification.Status != 100)
            {
                TempData["Error"] = "اشکال در پرداخت رخ داده است";
                return RedirectToAction("Index");
            }
            //return RedirectToAction("RetuenFromBankPortal");
            else
            {
                TempData["Message"] = "پرداخت با موفقیت انجام شد";

                _invoiceService.SetSuccessPayment(Authority);
                return RedirectToAction("Index");

            }
        }
        public IActionResult Delete(string id)
        {
            if (_invoiceService.Delete(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("Index");
        }
    }
}
