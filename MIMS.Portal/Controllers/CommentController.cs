﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class CommentController : Controller
    {
        ICommentService _CommentService; 
        IMerchantService _merchantService; 
        IUsersService _userService; 
        public CommentController(ICommentService commentService, IMerchantService merchantService,IUsersService userService)
        {
            _CommentService = commentService;
            _merchantService = merchantService;
            _userService = userService;
        }
        public IActionResult Index(string merchantid)
        {
            ViewBag.merchantid = merchantid;
            return View(_CommentService.FindByMerchantId(merchantid));
        }
        [HttpPost]
        public IActionResult Create(Comment comment,Guid MerchantId)
        {
          comment.Merchant=  _merchantService.GetById(MerchantId);

            var x = HttpContext.User.Identity as ClaimsIdentity;

            comment.UserSubmitName = _userService.FindById(new Guid(x.FindFirst("userId").Value)).Fullname;

            _CommentService.Add(comment);
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return RedirectToAction("Index",new { merchantid= MerchantId });
        }
    }
}
