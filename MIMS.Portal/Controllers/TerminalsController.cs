﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using MIMS.Portal.ViewModels.Store;
using MIMS.Portal.ViewModels.BankAccount;
using MIMS.Portal.Core.Data.Domains;
using MIMS.Portal.ViewModels.Terminal;
using Newtonsoft.Json;
using System.Net.Http;
using MIMS.Portal.PSPApies;

namespace MIMS.Portal.Controllers
{
    public class TokenValue
    {
        [JsonProperty("Success")]
        public bool Success { get; set; }

        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Result")]
        public Result Result { get; set; }
    }

    public class Result
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class TerminalsController : Controller
    {
        ITerminalService _terminalService;
        IMerchantService _merchantService;
        IBankAccountService _bankAccountService;
        IStoreService _storeService;
        IPosBrandService _posBrandService;
        IDeviceService _deviceService;

        public TerminalsController(ITerminalService terminalService, IMerchantService merchantService, IBankAccountService accountNumberService, IStoreService storeService, IPosBrandService posBrandService, IDeviceService deviceService)
        {
            _terminalService = terminalService;
            _merchantService = merchantService;
            _bankAccountService = accountNumberService;
            _storeService = storeService;
            _posBrandService = posBrandService;
            _deviceService = deviceService;
        }

        public IActionResult Index(Guid? id)
        {
            var terminalsIndex = new TerminalIndexViewModel();

            //if (id == Guid.Empty)
            //{
            //    terminals = _terminalService.Find().ConvertAll(x => (TerminalViewModel)x);
            //    return View(terminals);
            //}
            //var merchantId = id;
            //var merchant = _merchantService.GetById(merchantId);

            //if (merchant == null)
            //    return NotFound();

            //ViewBag.MerchantId = merchantId;

            //terminals = _terminalService.FindByMerchantId(merchantId).ConvertAll(x => (TerminalViewModel)x);

            Guid userId = Guid.Empty;

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            if (User.IsInRole("Admin"))
            {
                terminalsIndex.Stats = _terminalService.StatsByTerminalStatus(null);
                terminalsIndex.Terminals = _terminalService.Filter(include: x => x.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(b => b.Bank).Include(i => i.Store).Include(i => i.Model).ThenInclude(b => b.Parent).Include(i => i.Device).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (TerminalViewModel)x);

                //if (id == null || id == Guid.Empty)
                //    merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                //else
                //    merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }
            else
            {
                terminalsIndex.Stats = _terminalService.StatsByTerminalStatus(userId);
                terminalsIndex.Terminals = _terminalService.GetByUserId(userId).ToList().ConvertAll(x => (TerminalViewModel)x);
            }

            return View(terminalsIndex);
        }

        [HttpPost]
        public IActionResult Index(Guid? id, TerminalFilterViewModel? filters)
        {
            var terminalsIndex = new TerminalIndexViewModel();

            Guid userId = id.GetValueOrDefault();

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            //Expression<Func<Merchant, bool>> predicate = null;
            var predicate = PredicateBuilder.True<Terminal>();
            var andCriteria = new List<Predicate<Terminal>>();
            var orCriteria = new List<Predicate<Terminal>>();
            var orParams = PredicateBuilder.False<Terminal>();
            var andParams = PredicateBuilder.True<Terminal>();

            if (!string.IsNullOrWhiteSpace(filters.NationalCode))
            {
                orCriteria.Add(f => f.Merchant.NationalCode == filters.NationalCode);
                orParams = orParams.Or(f => f.Merchant.NationalCode == filters.NationalCode);
            }

            if (!string.IsNullOrWhiteSpace(filters.FirstName))
            {
                orCriteria.Add(f => f.Merchant.FirstName.Contains(filters.FirstName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.FirstName.Contains(filters.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(filters.LastName))
            {
                orCriteria.Add(f => f.Merchant.LastName.Contains(filters.LastName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.LastName.Contains(filters.LastName));
            }

            if (filters.Status != null)
            {
                orCriteria.Add(f => f.Status == filters.Status);
                orParams = orParams.Or(f => f.Status == filters.Status);
            }

            if (!User.IsInRole("Admin"))
            {
                andCriteria.Add(f => f.CreatorUserId == userId);
                andParams = andParams.And(f => f.CreatorUserId == userId);
                andParams = andParams.And(orParams);
            }


            predicate = c => andCriteria.All(pred => pred(c)) && orCriteria.Any(pred => pred(c));
            predicate = !User.IsInRole("Admin") ? andParams : orParams;


            if (User.IsInRole("Admin"))
            {
                if (id.GetValueOrDefault() == Guid.Empty)
                {
                    terminalsIndex.Stats = _terminalService.StatsByTerminalStatus(null);
                    terminalsIndex.Terminals = _terminalService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(b => b.Bank).Include(i => i.Store).Include(i => i.Model).ThenInclude(b => b.Parent).Include(i => i.Device).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (TerminalViewModel)x);
                    //merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                else
                {
                    terminalsIndex.Stats = _terminalService.StatsByTerminalStatus(userId);
                    terminalsIndex.Terminals = _terminalService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(b => b.Bank).Include(i => i.Store).Include(i => i.Model).ThenInclude(b => b.Parent).Include(i => i.Device).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (TerminalViewModel)x);
                    //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
            }
            else
            {
                terminalsIndex.Stats = _terminalService.StatsByTerminalStatus(userId);
                terminalsIndex.Terminals = _terminalService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(b => b.Bank).Include(i => i.Store).Include(i => i.Model).ThenInclude(b => b.Parent).Include(i => i.Device).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (TerminalViewModel)x);

                //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }

            return View(terminalsIndex);
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            var model = (TerminalViewModel)terminal;

            //var merchant = _merchantService.FindById(model.MerchantId);

            //model.MerchantId = merchant.Id;
            //model.MerchantFullName = merchant?.FullName;
            //model.MerchantNationalCode = merchant?.NationalCode;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var terminalViewModel = new TerminalViewModel();
            terminalViewModel.MerchantId = merchant.Id;
            terminalViewModel.MerchantFullName = merchant.FullName;

            ViewBag.Stores = new SelectList(_storeService.GetByMerchantId(merchantId), "Id", "StoreName");
            ViewBag.BankAccounts = new SelectList(_bankAccountService.GetByMerchantId(merchantId).ConvertAll(x => (BankAccountViewModel)x), "Id", "BankAccountFullName");
            ViewBag.DeviceBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name");

            return View(terminalViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TerminalViewModel model, bool next)
        {
            if (ModelState.IsValid)
            {
                model.Id = Guid.Empty;
                model.Status = TerminalStatus.Pending;
                var terminal = (Terminal)model;

                if (_terminalService.Create(terminal))
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Success,
                        Subject = "پایانه",
                        Content = "اطلاعات با موفقیت ثبت شد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
                }
                else
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Error,
                        Subject = "پایانه",
                        Content = "خطا در ثبت اطلاعات"
                    };
                    Toastrs.ToastrsList.Add(toastr);
                }
            }

            var merchant = _merchantService.GetById(model.MerchantId);

            if (merchant == null)
                return NotFound();

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;

            ViewBag.Stores = new SelectList(_storeService.GetByMerchantId(model.MerchantId), "Id", "StoreName", model.StoreId);
            ViewBag.BankAccounts = new SelectList(_bankAccountService.GetByMerchantId(model.MerchantId).ConvertAll(x => (BankAccountViewModel)x), "Id", "BankAccountFullName", model.BankAccountId);
            ViewBag.DeviceBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name", model.DeviceBrandId);
            ViewBag.ModelBrands = new SelectList(_posBrandService.FindByParentId(model.DeviceBrandId.GetValueOrDefault()), "Id", "Name", model.DeviceModelId);
            ViewBag.Devices = new SelectList(_deviceService.FindByModelId(model.DeviceModelId.GetValueOrDefault()), "Id", "Name", model.DeviceId);

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            var model = (TerminalViewModel)terminal;

            var merchant = _merchantService.GetById(model.MerchantId);

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant?.FullName;
            model.MerchantNationalCode = merchant?.NationalCode;

            ViewBag.Stores = new SelectList(_storeService.GetByMerchantId(model.MerchantId), "Id", "StoreName", model.StoreId);
            ViewBag.BankAccounts = new SelectList(_bankAccountService.GetByMerchantId(model.MerchantId).ConvertAll(x => (BankAccountViewModel)x), "Id", "BankAccountFullName", model.BankAccountId);
            ViewBag.DeviceBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name", model.DeviceBrandId);
            ViewBag.ModelBrands = new SelectList(_posBrandService.FindByParentId(model.DeviceBrandId.GetValueOrDefault()), "Id", "Name", model.DeviceModelId);
            ViewBag.Devices = new SelectList(_deviceService.FindByModelId(model.DeviceModelId.GetValueOrDefault()), "Id", "Name", model.DeviceId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TerminalViewModel model)
        {
            if (ModelState.IsValid)
            {
                var terminal = (Terminal)model;
                terminal.TerminalStatus = TerminalStatus.Pending;

                if (_terminalService.Edit(terminal))
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Success,
                        Subject = "پایانه",
                        Content = "اطلاعات با موفقیت ویرایش شد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
                }
                else
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Error,
                        Subject = "پایانه",
                        Content = "خطا در ثبت اطلاعات"
                    };
                    Toastrs.ToastrsList.Add(toastr);
                }
            }

            var merchant = _merchantService.GetById(model.MerchantId);

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;
            model.MerchantNationalCode = merchant.NationalCode;

            ViewBag.Stores = new SelectList(_storeService.GetByMerchantId(model.MerchantId), "Id", "StoreName", model.StoreId);
            ViewBag.BankAccounts = new SelectList(_bankAccountService.GetByMerchantId(model.MerchantId).ConvertAll(x => (BankAccountViewModel)x), "Id", "BankAccountFullName", model.BankAccountId);
            ViewBag.DeviceBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name", model.DeviceBrandId);
            ViewBag.ModelBrands = new SelectList(_posBrandService.FindByParentId(model.DeviceBrandId.GetValueOrDefault()), "Id", "Name", model.DeviceModelId);
            ViewBag.Devices = new SelectList(_deviceService.FindByModelId(model.DeviceModelId.GetValueOrDefault()), "Id", "Name", model.DeviceId);

            return View(model);
        }

        public ActionResult Pending()
        {
            var terminals = Enumerable.Empty<TerminalViewModel>();

            var predicate = PredicateBuilder.True<Terminal>();

            predicate = predicate.Or(m => (m.TerminalStatus == TerminalStatus.Pending)
            && (m.Merchant.Status == ReviewStatus.PreApproved || m.Merchant.Status == ReviewStatus.Approved) && (m.BankAccount.Status == ReviewStatus.PreApproved || m.BankAccount.Status == ReviewStatus.Approved) && (m.Store.Status == ReviewStatus.PreApproved || m.Store.Status == ReviewStatus.Approved));

            terminals = _terminalService.Filter(predicate,
                include: i => i.Include(i => i.Merchant).Include(i => i.BankAccount).ThenInclude(i => i.Bank).Include(i => i.Store).ThenInclude(i => i.BusinessScope).Include(i => i.Store.BusinessGroup).Include(i => i.Device)).ToList().ConvertAll(x => (TerminalViewModel)x);

            //terminals = _terminalService.Find().Where(m => (m.TerminalStatus == TerminalStatus.Pending || m.TerminalStatus == TerminalStatus.Unknown)
            //&& (m.Merchant.Status == ReviewStatus.PreApproved || m.Merchant.Status == ReviewStatus.Approved) && (m.BankAccount.Status == ReviewStatus.PreApproved || m.BankAccount.Status == ReviewStatus.Approved) && (m.Store.Status == ReviewStatus.PreApproved || m.Store.Status == ReviewStatus.Approved)).ToList().ConvertAll(x => (TerminalViewModel)x);

            return View(terminals);
        }

        public ActionResult Review(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            var terminalDetails = (TerminalViewModel)terminal;
            var model = new TerminalReviewViewModel();

            model.TerminalDetails = terminalDetails;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Review(TerminalReviewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model;

                var terminal = _terminalService.FindById(review.TerminalId);

                terminal.PspProvider = review.PspProvider;
                terminal.TerminalStatus = (review.ReviewStatus == true) ? ValueObjects.Enum.TerminalStatus.PreApproved : ValueObjects.Enum.TerminalStatus.RejectedByOther;

                terminal.ReviewComment = review.ReviewComment;

                _terminalService.Edit(terminal);
                if (model.ReviewStatus == true)
                {
                    HttpClient request = new HttpClient();
                    request.BaseAddress = new Uri("https://mmsapi.pep.co.ir");
                    PSPApies.Pasargad Pasargad = new PSPApies.Pasargad(request);
                    TokenValue res = JsonConvert.DeserializeObject<TokenValue>(Pasargad.GetTokenAsync(new UserDto()
                    {
                        UserName = "f.negar",
                        Password = "f.negar13@"
                    }, "").Result.ToString());
                    //--------------------------------
                    var merchant = new ShaparakPspRequest()
                    {
                        PublicIban = new PublicIbanViewModel()
                        {
                            Description = "1"
                        }
               ,
                        RelatedMerchants = new List<ShaparakPspMerchantRelated>()
            {new ShaparakPspMerchantRelated()
            {
                      BirthCrtfctNumber = 1111111,
                    BirthCrtfctSerial = 1,
                    BirthCrtfctSeriesLetter = ShaparakPspMerchantRelatedBirthCrtfctSeriesLetter._0,
                    BirthCrtfctSeriesNumber = 1,
                    BirthDate = 121212,
                    CellPhoneNumber = "0912",
                    CommercialCode = "12",
                    ComNameEn = "Ehsan",
                    ComNameFa = "akbar",
                    CountryCode = "1",
                    Description = "",
                    EmailAddress = "akbar@yahoo.com",
                    FatherNameEn = "1"
                  ,
                    FatherNameFa = "es",
                    Fax = "0129",
                    FirstNameEn = "1",
                    FirstNameFa = "1",
                    ForeignPervasiveCode = "1",
                    Gender = 1,
                    LastNameEn = "22",
                    LastNameFa = "22",
                    MerchantType = ShaparakPspMerchantRelatedMerchantType._0,
                    UpdateAction = 1,
                    MerchantIbans = new List<ShaparakPspMerchantIbans>()
                   {
                       new ShaparakPspMerchantIbans()
                       {
                           AccountNumber="1",UpdateAction=1,BranchCode="1",Description="",FirstName="1",LastName="1",MerchantIban="ir"
                           ,Message="1",Sequence="1"
                       }
                   },
                    PassportNumber = "1",
                    Message = "12",
                    NationalCode = "12",
                    NationalLegalCode = "12",
                    PassportExpireDate = 1212,
                    RegisterDate = 1212,
                    RegisterNumber = "121",
                    ResidencyType = ShaparakPspMerchantRelatedResidencyType._0,
                    TelephoneNumber = "021",
                    VitalStatus = ShaparakPspMerchantRelatedVitalStatus._0,
                    WebSite = "s",
                    MerchantOfficers = new List<ShaparakMerchantOfficerViewModel>()
                    {

                       new ShaparakMerchantOfficerViewModel()
                       {
                           BirthCrtfctNumber=121212,VitalStatus=ShaparakMerchantOfficerViewModelVitalStatus._0,BirthCrtfctSerial=1212,BirthCrtfctSeriesLetter=ShaparakMerchantOfficerViewModelBirthCrtfctSeriesLetter._0,
                           BirthCrtfctSeriesNumber=1,BirthDate=1212,CountryCode="11",Description="111",FatherNameEn="1",FatherNameFa="11",FirstNameEn="1",FirstNameFa="1",ForeignPervasiveCode="1",
                           Gender=1,LastNameEn="1",LastNameFa="1",NationalCode="1",PassportExpireDate=1111,PassportNumber="11",ResidencyType=ShaparakMerchantOfficerViewModelResidencyType._0,UpdateAction=1

                       }
                   }
            }
            }
                ,
                        PspRequestShopAcceptors = new List<ShaparakPspShopAcceptor>()
            {new ShaparakPspShopAcceptor()
            {
                Acceptors=new List<ShaparakPspAcceptor>()
            {
               new ShaparakPspAcceptor()
               {
                   AcceptCreditCardTransaction=false,AcceptorCode="1",
                  AddOrInActiveIbans=new List<IbanViewModel>()
                  {
                      new IbanViewModel()
                      {
                          AddOrInActive=1,Description="1",Iban="12"
                      }
                  },Description="1",AllowAmericanExpressTrx=false,AllowFoodSecurityTrx=false,AllowGoodsBasketTrx=false,
                  AllowIranianProductsTrx=false,AllowJcbCardTrx=false,AllowKaraCardTrx=false,AllowMasterCardTrx=false,AllowOtherInternationaCardsTrx=false
                  ,AllowScatteredSettlement=ShaparakPspAcceptorAllowScatteredSettlement._0,AllowUpiCardTrx=false,AllowVisaCardTrx=false,Blockable=false,Cancelable=false,ChargeBackable=false,
                  Iin="1",Refundable=false,SettledSeparately=false,UpdateAction=1,Ibans=new List<ShaparakPspAcceptorIban>()
                  {
                      new ShaparakPspAcceptorIban()
                      {
                          Iban="1",IsMain=false,SharePercent=12
                      }
                  },Terminals=new List<ShaparakPspTerminal>()
                  {
                      new ShaparakPspTerminal()
                      {
                          AccessAddress="",AccessPort=1,BankRegisterCode=12,CallbackAddress="!2",CallbackPort=1,Description="",DeviceType=1,
                          EmvType=1,HardwareBrand="!ss",HardwareModel="!2",KeyType=1,KeyValue="!2",ProjectId=1,Sequence=1,SerialNumber="1",SetupDate=111
                      }
                  }
               }
            },Shop=new ShopShaparakViewModel()
                    {
                Address="!,",UpdateAction=1,BusinessCategoryCode="!",BusinessCertificateNumber="1",BusinessSubCategoryCode="1",BusinessType=ShopShaparakViewModelBusinessType._0,
                CertificateExpiryDate=1,CertificateIssueDate=11,CityCode="1",CountryCode="1",Description="1",EmailAddress="1",EnglishName="1",EtrustCertificateExpiryDate=1,
                EtrustCertificateIssueDate=1,EtrustCertificateType=1,FarsiName="1",Message="1",OwnershipType=ShopShaparakViewModelOwnershipType._0,
                PostalCode="1",ProvinceCode="1",RentalContractNumber="1",RentalExpiryDate=11,TaxPayerCode="1",TelephoneNumber="121",WebsiteAddress="1"
                    }
            }

            },
                        CreatorUserID = new Guid("0d4415ce-10dc-40bd-95fb-c70c3ea70680"),
                        Description = "",
                        RequestType = ShaparakPspRequestRequestType._11,
                        TrackingNumberPsp = "1",
                        Contract = new ContractViewModel()
                        {
                            ContractDate = 11702121,
                            ContractNumber = "1",
                            Description = "",
                            ExpiryDate = 12548798,
                            ServiceStartDate = 124578,
                            UpdateAction = 1
                        },
                        LimitationRequestDetail = new LimitationRequestDetailViewModel()
                        {
                            AcceptorCode = "1",
                            Iin = "1",
                            InfractionCode = LimitationRequestDetailViewModelInfractionCode._0,
                            TerminalNumber = "1"
                        },
                        Merchant = new ShaparakPspMerchant()
                        {
                            BirthCrtfctNumber = 1111111,
                            BirthCrtfctSerial = 1,
                            BirthCrtfctSeriesLetter = ShaparakPspMerchantBirthCrtfctSeriesLetter._0,
                            BirthCrtfctSeriesNumber = 1,
                            BirthDate = 121212,
                            CellPhoneNumber = "0912",
                            CommercialCode = "12",
                            ComNameEn = "Ehsan",
                            ComNameFa = "akbar",
                            CountryCode = "1",
                            Description = "",
                            EmailAddress = "akbar@yahoo.com",
                            FatherNameEn = "1"
                  ,
                            FatherNameFa = "es",
                            Fax = "0129",
                            FirstNameEn = "1",
                            FirstNameFa = "1",
                            ForeignPervasiveCode = "1",
                            Gender = 1,
                            LastNameEn = "22",
                            LastNameFa = "22",
                            MerchantType = ShaparakPspMerchantMerchantType._0,
                            UpdateAction = 1,
                            MerchantIbans = new List<ShaparakPspMerchantIbans>()
                   {
                       new ShaparakPspMerchantIbans()
                       {
                           AccountNumber="1",UpdateAction=1,BranchCode="1",Description="",FirstName="1",LastName="1",MerchantIban="ir"
                           ,Message="1",Sequence="1"
                       }
                   },
                            PassportNumber = "1",
                            Message = "12",
                            NationalCode = "12",
                            NationalLegalCode = "12",
                            PassportExpireDate = 1212,
                            RegisterDate = 1212,
                            RegisterNumber = "121",
                            ResidencyType = ShaparakPspMerchantResidencyType._0,
                            TelephoneNumber = "021",
                            VitalStatus = ShaparakPspMerchantVitalStatus._0,
                            WebSite = "s",
                            MerchantOfficers = new List<ShaparakMerchantOfficerViewModel>()
                    {

                       new ShaparakMerchantOfficerViewModel()
                       {
                           BirthCrtfctNumber=121212,VitalStatus=ShaparakMerchantOfficerViewModelVitalStatus._0,BirthCrtfctSerial=1212,BirthCrtfctSeriesLetter=ShaparakMerchantOfficerViewModelBirthCrtfctSeriesLetter._0,
                           BirthCrtfctSeriesNumber=1,BirthDate=1212,CountryCode="11",Description="111",FatherNameEn="1",FatherNameFa="11",FirstNameEn="1",FirstNameFa="1",ForeignPervasiveCode="1",
                           Gender=1,LastNameEn="1",LastNameFa="1",NationalCode="1",PassportExpireDate=1111,PassportNumber="11",ResidencyType=ShaparakMerchantOfficerViewModelResidencyType._0,UpdateAction=1

                       }
                   }
                        }
                    };
                    //----------
                    List<ShaparakPspRequest> lst = new List<ShaparakPspRequest>();
                     lst.Add(merchant);
                    ShaparakPspClient shaparakPspClient = new ShaparakPspClient(request);
                    var q = shaparakPspClient.WriteRequestPspAsync(lst, "1", res.Result.Token).Result;

                }

                return RedirectToAction("details", "merchants", new { id = terminal.MerchantId });
            }

            return View(model);
        }

        [Route("[controller]/shaparak/pending", Name = "ShaparakPending")]
        public ActionResult ShaparakPending()
        {
            var terminals = Enumerable.Empty<TerminalViewModel>();

            terminals = _terminalService.Find().Where(m => m.TerminalStatus == TerminalStatus.Sent).ToList().ConvertAll(x => (TerminalViewModel)x);

            return View(terminals);
        }

        public ActionResult Shaparak(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;
            var terminal = _terminalService.FindById(terminalId);

            if (terminal == null)
                return NotFound();

            var terminalDetails = (TerminalViewModel)terminal;
            var model = new ShaparakReviewViewModel();

            model.TerminalDetails = terminalDetails;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Shaparak(ShaparakReviewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model;

                var terminal = _terminalService.FindById(review.TerminalId);
                terminal.TerminalStatus = (TerminalStatus)Enum.Parse(typeof(TerminalStatus), review.ShaparakStatus.ToString(), true);
                terminal.ReviewComment = review.ReviewComment;

                if (review.ShaparakStatus == ShaparakStatus.Approved)
                {
                    terminal.MerchantNumber = review.MerchantNumber;
                    terminal.TerminalNumber = review.TerminalNumber;
                }

                _terminalService.Edit(terminal);
            }
            return RedirectToAction(nameof(Pending));
        }


        public IActionResult confirm(string id, string merchantId)
        {
            ViewBag.id = id;
            ViewBag.merchantId = merchantId;
            return View();
        }
        [HttpPost]
        public IActionResult confirm(Guid id, string merchantId, string TerminalId, string MerchantNumber)
        {
            Terminal ter = _terminalService.FindById(id);
            //ter.TerminalId = TerminalId;
            ter.MerchantNumber = MerchantNumber;
            //ter.State = "Success";
            _terminalService.Edit(ter);
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return RedirectToAction("Index", new { merchantId = merchantId });
        }

        public IActionResult Delete(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            if (_terminalService.Delete(id))
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "پایانه",
                    Content = "اطلاعات با موفقیت حذف شد"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            else
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "پایانه",
                    Content = "حذف اطلاعات امکان پذیر نیست"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return RedirectToAction("Details", "Merchants", new { Id = id });
        }
    }
}
