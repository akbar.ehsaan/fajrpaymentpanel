﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Controllers
{
    //[Route("/Reviews/")]
    public class ReviewsController : Controller
    {
        IMerchantService _merchantService;
        IUsersService _userService;
        IFilesManagerService _filesManagerService;


        public ReviewsController(IMerchantService merchantService, IUsersService userService, IFilesManagerService filesManagerService)
        {
            _userService = userService;
            _merchantService = merchantService;
            _filesManagerService = filesManagerService;
        }

        //[Route("/Merchant")]
        public ActionResult Merchants()
        {
            var merchants = Enumerable.Empty<MerchantViewModel>();

            merchants = _merchantService.GetAll().Where(m => m.Status == ValueObjects.Enum.ReviewStatus.Pending || m.Status == ValueObjects.Enum.ReviewStatus.Unknown).ToList().ConvertAll(x => (MerchantViewModel)x);

            return View(merchants);
        }

        [Route("[controller]/Merchant/Details/{id}", Name = "MerchantDetails")]
        public ActionResult MerchantDetails(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

            var merchantDetails = (MerchantViewModel)merchant;
            var merchantReview = new MerchantReviewViewModel();

            var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.IDCardFrontId = idCardFront?.Id ?? Guid.Empty;
            merchantReview.IDCardFrontUrl = idCardFront?.FileUrl;

            var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.IDCardBackId = idCardBack?.Id ?? Guid.Empty;
            merchantReview.IDCardBackUrl = idCardBack?.FileUrl;

            var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.CredentialMainPageId = credentialMainPage?.Id ?? Guid.Empty;
            merchantReview.CredentialMainPageUrl = credentialMainPage?.FileUrl;

            var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.CredentialLastPageId = credentialLastPage?.Id ?? Guid.Empty;
            merchantReview.CredentialLastPageUrl = credentialLastPage?.FileUrl;


            var model = new MerchantDetailsReviewViewModel();
            model.MerchantDetails = merchantDetails;
            model.MerchantReview = merchantReview;

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[controller]/Merchant/Submit", Name = "ReviewedMerchant")]
        public ActionResult ReviewMerchant(MerchantReviewedViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model.MerchantReview;

                var merchant = _merchantService.GetById(review.MerchantId);
                merchant.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                merchant.ReviewComment = review.ReviewComment;

                var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

                var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                idCardFront.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if (idCardBack != null) idCardBack.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                credentialMainPage.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if(credentialLastPage !=null)
                credentialLastPage.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                _merchantService.Edit(merchant);
                //_filesManagerService.Update();
            }
            return RedirectToAction(nameof(Merchants));
        }

        // GET: ReviewsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ReviewsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ReviewsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ReviewsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
