﻿using Microsoft.AspNetCore.Mvc;

namespace MIMS.Portal.Controllers
{
    public class ErrorsController : Controller
    {
        [HttpGet]
        //[Route("[controller]/Errors/{id}")]
        public IActionResult Index(int? id = null, int? status = 404)
        {
            //return a static file. 
            return View($"{status}");
            //return File("~/errors/${status}.html", "text/html");
        }
    }
}
