﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class UsersController : Controller
    {
        IGeoService _countrySectionService;
        IUsersService _userService;
        IDepartmentService _departmentService;

        public UsersController(IUsersService userService, IGeoService countrySectionService,
            IDepartmentService departmentService)
        {
            _userService = userService;
            _countrySectionService = countrySectionService;
            _departmentService = departmentService;

        }

        public IActionResult Index()
        {
            var users = _userService.FindUser().ToList().ConvertAll(x => (UserViewModel)x);

            return View(users);
        }

        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var userId = id;

            var user = _userService.FindById(userId);

            if (user == null)
                return NotFound();

            var model = (UserViewModel)user;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userEntity = (User)model;

                userEntity.Permission = "1";
                userEntity.Status = "Active";

                _userService.Create(userEntity);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "کاربر",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Index");
            }

            var districts = _countrySectionService.GetSectionsById(model.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", model.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(model.ProvinceId), "Id", "Name", model.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", model.CityId);

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var user = _userService.FindById(id);

            if (user == null)
                return NotFound();

            var model = (UserViewModel)user;

            var districts = _countrySectionService.GetSectionsById(model.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", model.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(model.ProvinceId), "Id", "Name", model.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", model.CityId);

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userEntity = (User)model;

                userEntity.Permission = "1";
                userEntity.Status = "Active";

                _userService.Create(userEntity);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "کاربر",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Index");
            }

            var districts = _countrySectionService.GetSectionsById(model.CountyId);
            var cities = new List<GeoSection>();

            foreach (var dis in districts)
            {
                var children = _countrySectionService.GetSectionsById(dis.Id);
                cities.AddRange(children.Where(x => x.Type == "City").ToList());
            }

            ViewBag.Provinces = new SelectList(_countrySectionService.GetSectionsById(null), "Id", "Name", model.ProvinceId);
            ViewBag.Counties = new SelectList(_countrySectionService.GetSectionsById(model.ProvinceId), "Id", "Name", model.CountyId);
            ViewBag.Cities = new SelectList(cities, "Id", "Name", model.CityId);

            return View(model);
        }

        public IActionResult Delete(Guid id)
        {
            if (_userService.Delete(id))
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "کاربر",
                    Content = "اطلاعات با موفقیت حذف شد"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            else
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "کاربر",
                    Content = "حذف اطلاعات امکان پذیر نیست"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return RedirectToAction("Index");
        }

        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ChangePassword(string oldpassword, string newpassword, string repeatnewpass)
        {
            var x = HttpContext.User.Identity as ClaimsIdentity;
            var c = x.Claims.First().Type;
            var userId = new Guid(x.FindFirst("userId").Value);
            if (newpassword == repeatnewpass)
            {
                if (_userService.ChangePassword(userId, oldpassword, newpassword))
                {
                    TempData["Message"] = "اطلاعات با موفقیت ویرایش شد ";
                    return RedirectToAction("logout");

                }
                else
                {
                    TempData["Error"] = "ویرایش اطلاعات ناموفق بود";
                    return View("ChangePassword");

                }
            }
            else
            {
                TempData["Error"] = "کلمه عبور جدید با تکرار آن مطابقت ندارد";
                return View("ChangePassword");

            }

        }

    }
}
