﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using MIMS.Portal.Service;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Controllers
{
    //[RoutePrefix("Validator")]
    //[Authorize]
    [AllowAnonymous]
    public class ValidatorController : Controller
    {
        private readonly IMerchantService _merchantService;
        private readonly IUsersService _usersService;
        private readonly IDeviceService _deviceService;

        public ValidatorController(IMerchantService merchantService, IUsersService usersService, IDeviceService deviceService)
        {
            _merchantService = merchantService;
            _usersService = usersService;
            _deviceService = deviceService;
        }

        #region Checking

        [HttpGet]
        public JsonResult CheckUsername(string userName)
        {
            try
            {
                if (!string.IsNullOrEmpty(userName) && IsUserNameExist(userName))
                {
                    return Json("تکراری");
                }
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        private bool IsMerchantExist(string nationalCode, Guid? userId)
        {
            bool result = false;

            var u = _merchantService.GetByNationalCode(nationalCode);
            if (u != null)
                if (u.Id != userId.GetValueOrDefault())
                    result = true;
                else
                    result = false;

            return result;
        }

        private bool IsUserExist(string nationalCode, int? id = 0)
        {
            bool result = false;

            var u = _usersService.FindByNationalCode(nationalCode);
            if (u != null)
                result = true;

            return result;
        }

        private bool IsUserNameExist(string userName, int? id = 0)
        {
            bool result = false;

            var u = _usersService.FindByUserName(userName);
            if (u != null)
                result = true;

            return result;
        }

        [HttpGet]
        public JsonResult CheckNationalID(string nationalCode, Guid? id)
        {
            if (!string.IsNullOrEmpty(nationalCode) && !IsNationalIdValid(nationalCode))
            {
                return Json("نامعتبر");
            }
            if (!string.IsNullOrEmpty(nationalCode) && IsMerchantExist(nationalCode, id))
            {
                return Json("تکراری");
            }
            return Json(true);
        }
        [HttpGet]
        public JsonResult CheckUserNationalCode(string nationalCode, int? userId)
        {
            if (!string.IsNullOrEmpty(nationalCode) && !IsNationalIdValid(nationalCode))
            {
                return Json("نامعتبر");
            }
            if (!string.IsNullOrEmpty(nationalCode) && IsUserExist(nationalCode))
            {
                return Json("تکراری");
            }
            return Json(true);
        }


        private bool IsNationalIDExist(string nationalId, int? id = 0)
        {
            bool result = false;

            //result = db.Users.Any(x => x.NationalID == nationalId);
            //if (result && id != null)
            //    result = !db.Users.Any(x => x.NationalID == nationalId && x.UserID == id);

            return result;
        }

        /// <summary>
        /// تعیین معتبر بودن کد ملی
        /// </summary>
        /// <param name="nationalCode">کد ملی وارد شده</param>
        /// <returns>
        /// در صورتی که کد ملی صحیح باشد خروجی <c>true</c> و در صورتی که کد ملی اشتباه باشد خروجی <c>false</c> خواهد بود
        /// </returns>
        /// <exception cref="System.Exception"></exception>
        private bool IsNationalIdValid(string nationalCode)
        {
            //var Pattern = @"^[0-9]{10}$";
            //bool Result = false;

            //if (!String.IsNullOrEmpty(NationalID) && Regex.IsMatch(NationalID, Pattern))
            //    Result = true;

            //در صورتی که کد ملی وارد شده خالی باشد
            if (String.IsNullOrEmpty(nationalCode))
                return false;
            //throw new Exception("لطفا کد ملی را صحیح وارد نمایید");

            //در صورتی که کد ملی وارد شده طولش کمتر از 10 رقم باشد
            if (nationalCode.Length != 10)
                return false;
            //throw new Exception("طول کد ملی باید 10 کاراکتر باشد");

            //در صورتی که کد ملی ده رقم عددی نباشد
            var regex = new Regex(@"\d{10}");
            if (!regex.IsMatch(nationalCode))
                return false;
            //throw new Exception("کد ملی تشکیل شده از ده رقم عددی می‌باشد؛ لطفا کد ملی را صحیح وارد نمایید");

            //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
            var allDigitEqual = new[] { "0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999" };
            if (allDigitEqual.Contains(nationalCode)) return false;

            //عملیات شرح داده شده در بالا
            var chArray = nationalCode.ToCharArray();
            var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
            var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
            var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
            var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
            var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
            var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
            var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
            var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
            var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
            var a = Convert.ToInt32(chArray[9].ToString());

            var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
            var c = b % 11;

            return (((c < 2) && (a == c)) || ((c >= 2) && ((11 - c) == a)));
        }
        static public bool check_code(string nationalCode)
        {
            bool result;
            try
            {

                //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
                var allDigitEqual = new[] { "0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999" };
                if (allDigitEqual.Contains(nationalCode)) return false;


                //عملیات شرح داده شده در بالا
                var chArray = nationalCode.ToCharArray();
                var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
                var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
                var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
                var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
                var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
                var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
                var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
                var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
                var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
                var a = Convert.ToInt32(chArray[9].ToString());

                var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
                var c = b % 11;

                return (((c < 2) && (a == c)) || ((c >= 2) && ((11 - c) == a)));
            }
            catch { result = false; }

            return result;
        }

        [HttpGet]
        public JsonResult CheckLegalID(string LegalCode)
        {
            if (!IsLegallIdValid(LegalCode))
            {
                return Json("شناسه ملی نامعتبر");
            }

            return Json(true);
        }
        private bool IsLegallIdValid(string legalCode)
        {
            //در صورتی که شناسه ملی ده رقم عددی نباشد
            var regex = new Regex(@"\d{11}");
            if (string.IsNullOrWhiteSpace(legalCode) || !regex.IsMatch(legalCode))
                return false;

            //در صورتی که رقم‌های شناسه ملی وارد شده یکسان باشد
            var allDigitEqual = new[] { "00000000000", "11111111111", "22222222222", "33333333333", "44444444444", "55555555555", "66666666666", "77777777777", "88888888888", "99999999999" };
            if (allDigitEqual.Contains(legalCode))
                return false;

            //عملیات شرح داده شده در بالا
            var chArray = legalCode.ToCharArray();
            // شناسايي رقم كنترل
            var controlDdigit = Convert.ToInt32(chArray[10].ToString());
            // محاسبه دهگان +2
            var tot = Convert.ToInt32(chArray[9].ToString()) + 2;

            var sum = 0;     //1- براي محاسبه رقم کنترل از روي ساير ارقام ، هر رقم را  با رقم
                             // دهگان کد +2 کرده و سپس در ضريب آن ضرب مي کنيم و حاصل را با هم جمع مي کنيم.
            sum = sum + ((tot + Convert.ToInt32(chArray[0].ToString())) * 29);
            sum = sum + ((tot + Convert.ToInt32(chArray[1].ToString())) * 27);
            sum = sum + ((tot + Convert.ToInt32(chArray[2].ToString())) * 23);
            sum = sum + ((tot + Convert.ToInt32(chArray[3].ToString())) * 19);
            sum = sum + ((tot + Convert.ToInt32(chArray[4].ToString())) * 17);
            sum = sum + ((tot + Convert.ToInt32(chArray[5].ToString())) * 29);
            sum = sum + ((tot + Convert.ToInt32(chArray[6].ToString())) * 27);
            sum = sum + ((tot + Convert.ToInt32(chArray[7].ToString())) * 23);
            sum = sum + ((tot + Convert.ToInt32(chArray[8].ToString())) * 19);
            sum = sum + ((tot + Convert.ToInt32(chArray[9].ToString())) * 17);

            var checkSum = sum % 11;
            //3- اگر باقيمانده برابر 10 باشد ، باقيمانده را برابر 0 قرار مي دهيم

            if (checkSum == 10)
                checkSum = 0;

            //4-اگر رقم کنترل برابر باقيمانده باشد شناسه ملي صحيح فرض مي شود
            if (checkSum == controlDdigit)
                return true;
            else
                return false;
            //return (((c < 2) && (controlDdigit == c)) || ((c >= 2) && ((11 - c) == controlDdigit)));
        }


        [HttpGet]
        public JsonResult CheckIsLegal(bool IsOwnerLegal, string NationalId, string RegisterNumber)
        {
            if (IsOwnerLegal)
            {
                if (string.IsNullOrWhiteSpace(NationalId) || string.IsNullOrWhiteSpace(RegisterNumber))
                    return Json("اطلاعات حقوقی نامعتبر");
            }
            return Json(true);

        }
        public bool IsUserAvailable(string EmailId)
        {
            return false;
        }


        #endregion

        [HttpGet]
        public JsonResult CheckDeviceSerial(string serial, Guid? id, Guid? posBrandId)
        {
            if (!string.IsNullOrEmpty(serial) &&  posBrandId.GetValueOrDefault() == Guid.Empty)
            {
                return Json("برند نامعتبر");
            }
            if (!string.IsNullOrEmpty(serial) && IsSerialExist(serial, id, posBrandId))
            {
                return Json("تکراری");
            }
            return Json(true);
        }
        private bool IsSerialExist(string serial, Guid? deviceId, Guid? posBrandId)
        {
            bool result = false;

            var u = _deviceService.FindDeviceBySerial(serial).FirstOrDefault();
            if (u != null)
            {
                if (u.PosDeviceBrandId == posBrandId.GetValueOrDefault() && deviceId.GetValueOrDefault()!= Guid.Empty)
                {
                    if (u.Id != deviceId.GetValueOrDefault())
                        result = true;
                }
                if (u.PosDeviceBrandId == posBrandId.GetValueOrDefault() && deviceId.GetValueOrDefault() == Guid.Empty)
                    result = true;
            }

            return result;
        }
    }
}