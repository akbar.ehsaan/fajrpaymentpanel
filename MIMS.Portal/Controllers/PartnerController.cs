﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class PartnerController : Controller
    {
        IPartnerService _partnerService;
        IMerchantService _merchantservice;


        public PartnerController(IPartnerService partnerService, IMerchantService merchantService)
        {
            _merchantservice= merchantService;

            _partnerService = partnerService;


        }
        public IActionResult Index(string id)
        {
            List<Partner> partners = _partnerService.FindByMerchantId(id);
            ViewBag.Partners = partners;

            ViewBag.merchantId = id;

            return View();
        }
        [HttpPost]
        public IActionResult Create(Partner partner, Guid merchantId)
        {

            Merchant _mer = _merchantservice.GetById(merchantId);
            partner.Merchant = _mer;
            _partnerService.Add(partner);
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";
            return RedirectToAction("index", new { id = merchantId });
        }
        public IActionResult Delete(string id, string MerchantId)
        {
            if (_partnerService.Remove(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("index", new { id = MerchantId });
        }
    }
}
