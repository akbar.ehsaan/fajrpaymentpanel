﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class PosDevicesController : Controller
    {
        private readonly IDeviceService _deviceService;
        private readonly IPosBrandService _posBrandService;
        private readonly IUsersService _userService;

        public PosDevicesController(IDeviceService deviceService, IUsersService userservice, IPosBrandService posBrandService)
        {
            _userService = userservice;
            _deviceService = deviceService;
            _posBrandService = posBrandService;
        }

        [AllowAnonymous]
        public JsonResult GetDevices(Guid id, PosConnectionType? connectionType)
        {
            var devices = Enumerable.Empty<PosDevice>();
            var userClaims = HttpContext.User.Identity as ClaimsIdentity;
            var supervisorUserId = new Guid(userClaims.FindFirst("userId").Value);

            devices = _deviceService.FindByModelId(id);
            devices = devices.Where(i => i.DeviceStatus != PosStatus.TerminalAssigned && i.SupervisorUserId == supervisorUserId);

            if (connectionType != null)
                devices = devices.Where(i => i.DeviceConnectionType == connectionType);

            var items = devices.OrderBy(i => i.DeviceSerial).Select(i => new { Value = i.Id, Text = i.DeviceSerial });

            return Json(items);
        }
        public IActionResult Index()
        {

            var device = Enumerable.Empty<DeviceViewModel>();

            // var device = _deviceService.FindDevice().ToList().ConvertAll(x => (DeviceViewModel)x);


            if (User.IsInRole("Admin"))
            {
                var device2 = _deviceService.FindDevice().ToList();
                device = _deviceService.FindDevice().ToList().ConvertAll(x => (DeviceViewModel)x);
            }


            List<DeviceViewModel> model = new List<DeviceViewModel>();

            foreach (var item in device)
            {
                item.BrandName = _posBrandService.FindById(item.PosBrandId).Name;
                item.ModelName = _posBrandService.FindById(item.PosModelId).Name;
                model.Add(item);
            }


            return View(model);



        }


        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var deviceId = id;
            var device = _deviceService.FindById(deviceId);

            if (device == null)
                return NotFound();

            var model = (DeviceViewModel)device;
            model.BrandName = _posBrandService.FindById(model.PosBrandId).Name;
            model.ModelName = _posBrandService.FindById(model.PosModelId).Name;

            return View(model);
        }


        [HttpGet]
        public IActionResult Create()
        {
            var userClaims = HttpContext.User.Identity as ClaimsIdentity;
            ViewBag.SupervisorUserId = new Guid(userClaims.FindFirst("userId").Value);
            ViewBag.PosBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(DeviceViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userClaims = HttpContext.User.Identity as ClaimsIdentity;
                    model.SupervisorUserId = new Guid(userClaims.FindFirst("userId").Value);

                    var device = (PosDevice)model;

                    _deviceService.Add(device);


                    var toastr = new Toastr
                    {
                        Type = ToastrType.Success,
                        Subject = "دستگاه",
                        Content = "اطلاعات با موفقیت ثبت شد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    //return RedirectToAction("Create", "Devices", new { Id = Id, Next = true });
                    return RedirectToAction("Index");

                }

                ViewBag.PosBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name", model.PosBrandId);
                ViewBag.PosModels = new SelectList(_posBrandService.FindByParentId(model.PosBrandId), "Id", "Name", model.PosModelId);

                return View(model);
            }
            catch (Exception e)
            {

                throw;
            }

        }


        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == null)
                return BadRequest();

            var deviceId = id;
            var device = _deviceService.FindById(deviceId);


            if (device == null)
                return NotFound();

            var model = (DeviceViewModel)device;

            ViewBag.PosBrands = new SelectList(_posBrandService.FindParents(), "Id", "Name", model.PosBrandId);
            ViewBag.PosModels = new SelectList(_posBrandService.FindByParentId(model.PosBrandId), "Id", "Name", model.PosModelId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(DeviceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var device = (PosDevice)model;


                _deviceService.Update(device);


                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "دستگاه",
                    Content = "اطلاعات با موفقیت ویرایش شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult Delete(string id)
        {
            if (_deviceService.Delete(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("Index");
        }
    }
}
