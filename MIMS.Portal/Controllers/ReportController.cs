﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Biz.IServices;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer"/*, Roles = "admin"*/)]

    public class ReportController : Controller
    {
        private IMerchantService _merchantService;
        private IStoreService _storeService;

        public ReportController(IMerchantService merchantService, IStoreService storeService)
        {
            _merchantService = merchantService;
            _storeService = storeService;
        }
        public IActionResult Index()
        {
            ;
            //var x = HttpContext.User.Identity as ClaimsIdentity;
            //var c = x.Claims.First().Type;
            //var cc = x.FindFirst("userId").Value;

            var q = _merchantService.MerchantDateCountReport();

            ViewBag.datasets = JsonSerializer.Serialize(q.Select(i => i.value));
            ViewBag.labels = JsonSerializer.Serialize(q.Select(i => i.key));

            var q1 = _storeService.StatsByStoreProvienceCountReport();

            ViewBag.datasetsStore = JsonSerializer.Serialize(q1.Select(i => i.value));
            ViewBag.labelsStore = JsonSerializer.Serialize(q1.Select(i => i.key));

            return View();
        }
    }
}
