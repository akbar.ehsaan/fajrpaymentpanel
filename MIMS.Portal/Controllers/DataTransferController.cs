﻿using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIMS.Portal.Controllers
{
    public class DataTransferController : Controller
    {
        ITerminalService _terminalService;


        public DataTransferController(ITerminalService terminalService)
        {
            _terminalService = terminalService;

        }

        public IActionResult PepIndex()
        {
            var model = Enumerable.Empty<PspPasargadViewModel>();

            try
            {
                model = _terminalService.Find().Where(i=> i.TerminalStatus == TerminalStatus.PreApproved && i.PspProvider == PspProvider.Pep).ToList().ConvertAll(x => (PspPasargadViewModel)x);

                    return View(model);              
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "MerchantTerminalsService",
                    Content = "خطا در پردازش اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return View(model);
        }

        public IActionResult Index(string id)
        {
            if (!Enum.TryParse(id, true, out PspProvider psp))
            {
                return BadRequest();
            }

            ViewBag.PspProvider = psp;
            return View();
        }

        public IActionResult Done(Guid id, PspProvider psp)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var terminalId = id;

            try
            {
                var terminal = _terminalService.FindById(terminalId);

                terminal.TerminalStatus = TerminalStatus.Sent;

                _terminalService.Edit(terminal);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "ترمینال",
                    Content = "اطلاعات برای سرویس دهنده ارسال/ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction(nameof(Index), new { id = terminal.PspProvider });
            }
            catch (Exception ex)
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "TerminalService.Edit",
                    Content = "خطا در ثبت اطلاعات"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            return RedirectToAction(nameof(Index), new { id = psp });
        }
    }
}
