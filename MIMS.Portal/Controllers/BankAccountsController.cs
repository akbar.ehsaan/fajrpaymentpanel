﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.ViewModels.BankAccount;
using MIMS.Portal.Core.Data.Domains;
using Microsoft.EntityFrameworkCore;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class BankAccountsController : Controller
    {
        IBankAccountService _bankAccountService;
        IBankService _bankService;
        IMerchantService _merchantService;
        IFilesManagerService _filesManagerService;


        public BankAccountsController(IBankAccountService accountNumberService, IMerchantService merchantService,
            IBankService bankService, IFilesManagerService filesManagerService)
        {
            _bankAccountService = accountNumberService;
            _merchantService = merchantService;
            _bankService = bankService;
            _filesManagerService = filesManagerService;
        }

        public IActionResult Index(Guid? id)
        {
            var bankAccountsIndex = new BankAccountIndexViewModel();


            Guid userId = Guid.Empty;

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            if (User.IsInRole("Admin"))
            {
                bankAccountsIndex.Stats = _bankAccountService.StatsByReviewStatus(null);
                bankAccountsIndex.BankAccounts = _bankAccountService.Filter(include: x => x.Include(i => i.Merchant).Include(i => i.Bank).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (BankAccountViewModel)x);

                //if (id == null || id == Guid.Empty)
                //    merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                //else
                //    merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }
            else
            {
                bankAccountsIndex.Stats = _bankAccountService.StatsByReviewStatus(userId);
                bankAccountsIndex.BankAccounts = _bankAccountService.GetByUserId(userId).ToList().ConvertAll(x => (BankAccountViewModel)x);
            }

            return View(bankAccountsIndex);
        }

        [HttpPost]
        public IActionResult Index(Guid? id, BankAccountFilterViewModel? filters)
        {
            var bankAccountsIndex = new BankAccountIndexViewModel();

            Guid userId = id.GetValueOrDefault();

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            //Expression<Func<Merchant, bool>> predicate = null;
            var predicate = PredicateBuilder.True<BankAccount>();
            var andCriteria = new List<Predicate<BankAccount>>();
            var orCriteria = new List<Predicate<BankAccount>>();
            var orParams = PredicateBuilder.False<BankAccount>();
            var andParams = PredicateBuilder.True<BankAccount>();

            if (!string.IsNullOrWhiteSpace(filters.NationalCode))
            {
                orCriteria.Add(f => f.Merchant.NationalCode == filters.NationalCode);
                orParams = orParams.Or(f => f.Merchant.NationalCode == filters.NationalCode);
            }

            if (!string.IsNullOrWhiteSpace(filters.FirstName))
            {
                orCriteria.Add(f => f.Merchant.FirstName.Contains(filters.FirstName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.FirstName.Contains(filters.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(filters.LastName))
            {
                orCriteria.Add(f => f.Merchant.LastName.Contains(filters.LastName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.Merchant.LastName.Contains(filters.LastName));
            }

            if (filters.Status != null)
            {
                orCriteria.Add(f => f.Status == filters.Status);
                orParams = orParams.Or(f => f.Status == filters.Status);
            }

            if (!User.IsInRole("Admin"))
            {
                andCriteria.Add(f => f.CreatorUserId == userId);
                andParams = andParams.And(f => f.CreatorUserId == userId);
                andParams = andParams.And(orParams);
            }


            predicate = c => andCriteria.All(pred => pred(c)) && orCriteria.Any(pred => pred(c));
            predicate = !User.IsInRole("Admin") ? andParams : orParams;


            if (User.IsInRole("Admin"))
            {
                if (id.GetValueOrDefault() == Guid.Empty)
                {
                    bankAccountsIndex.Stats = _bankAccountService.StatsByReviewStatus(null);
                    bankAccountsIndex.BankAccounts = _bankAccountService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.Bank).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (BankAccountViewModel)x);
                    //merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                else
                {
                    bankAccountsIndex.Stats = _bankAccountService.StatsByReviewStatus(userId);
                    bankAccountsIndex.BankAccounts = _bankAccountService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.Bank).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (BankAccountViewModel)x);
                    //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
            }
            else
            {
                bankAccountsIndex.Stats = _bankAccountService.StatsByReviewStatus(userId);
                bankAccountsIndex.BankAccounts = _bankAccountService.Filter(predicate, include: x => x.Include(i => i.Merchant).Include(i => i.Bank).Include(i => i.CreatorUser)).ToList().ConvertAll(x => (BankAccountViewModel)x);

                //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }

            return View(bankAccountsIndex);
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var Bank = _bankAccountService.GetById(merchantId);

            if (Bank == null)
                return NotFound();

            var model = (BankAccountViewModel)Bank;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(Guid id, bool next)
        {
            if (id == Guid.Empty)
                return NotFound();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var bankAccount = new BankAccountViewModel();
            bankAccount.MerchantId = merchant.Id;
            bankAccount.MerchantFullName = merchant.FullName;
            bankAccount.MerchantNationalCode = merchant.NationalCode;

            ViewBag.Banks = new SelectList(_bankService.Find(), "Id", "BankName");

            return View(bankAccount);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BankAccountViewModel model, bool next)
        {
            if (ModelState.IsValid)
            {
                model.Id = Guid.Empty;
                model.Status = ReviewStatus.Pending;
                var bankAccount = (BankAccount)model;

                try
                {
                    _bankAccountService.Create(bankAccount);

                    var toastr = new Toastr
                    {
                        Type = ToastrType.Success,
                        Subject = "شماره حساب",
                        Content = "اطلاعات با موفقیت ثبت شد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    if (next)
                        return RedirectToAction("Create", "Stores", new { Id = model.MerchantId, Next = next });

                    return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
                }
                catch (Exception ex)
                {
                    var toastr = new Toastr
                    {
                        Type = ToastrType.Error,
                        Subject = "BankAccountService.Add",
                        Content = "خطا در ثبت اطلاعات"
                    };
                    Toastrs.ToastrsList.Add(toastr);
                }

            }

            var merchant = _merchantService.GetById(model.MerchantId);

            if (merchant == null)
                return NotFound();

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;
            model.MerchantNationalCode = merchant.NationalCode;

            ViewBag.Banks = new SelectList(_bankService.Find(), "Id", "BankName", model.BankId);

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
                return NotFound();

            var bankAccountId = id;
            var bankAccount = new BankAccountViewModel();

            bankAccount = _bankAccountService.GetById(bankAccountId);

            if (bankAccount == null)
                return NotFound();

            var merchant = _merchantService.GetById(bankAccount.MerchantId);

            if (merchant == null)
                return NotFound();

            bankAccount.MerchantId = merchant.Id;
            bankAccount.MerchantFullName = merchant.FullName;
            bankAccount.MerchantNationalCode = merchant.NationalCode;

            ViewBag.Banks = new SelectList(_bankService.Find(), "Id", "BankName", bankAccount.BankId);

            return View(bankAccount);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BankAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                var bankAccount = (BankAccount)model;
                //bankAccount.Bank = _bankService.FindById(model.BankId);
                //bankAccount.Merchant = _merchantService.FindById(model.MerchantId);

                _bankAccountService.Edit(bankAccount);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "شماره حساب",
                    Content = "اطلاعات با موفقیت ویرایش شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Details", "Merchants", new { Id = model.MerchantId });
            }

            var merchant = _merchantService.GetById(model.MerchantId);

            if (merchant == null)
                return NotFound();

            model.MerchantId = merchant.Id;
            model.MerchantFullName = merchant.FullName;
            model.MerchantNationalCode = merchant.NationalCode;

            ViewBag.Banks = new SelectList(_bankService.Find(), "Id", "BankName", model.BankId);

            return View(model);
        }

        public ActionResult Pending()
        {
            var bankAccounts = Enumerable.Empty<BankAccountViewModel>();

            bankAccounts = _bankAccountService.GetAll().Where(m => (m.Status == ValueObjects.Enum.ReviewStatus.Pending || m.Status == ValueObjects.Enum.ReviewStatus.Unknown) && (m.Merchant.Status == ReviewStatus.PreApproved || m.Merchant.Status == ReviewStatus.Approved)).ToList().ConvertAll(x => (BankAccountViewModel)x);

            return View(bankAccounts);
        }

        public ActionResult Review(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var bankAccountId = id;
            var bankAccount = _bankAccountService.GetById(bankAccountId);

            if (bankAccount == null)
                return NotFound();

            var bankAccountFiles = _filesManagerService.FindByParentId(bankAccount.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Store).OrderByDescending(f => f.CreatedDate);

            var bankAccountDetails = (BankAccountViewModel)bankAccount;
            var model = new BankAccountReviewViewModel();

            model.BankAccountDetails = bankAccountDetails;

            //var storeDocument = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            //model.StoreDocumentId = storeDocument?.Id ?? Guid.Empty;
            //model.StoreDocumentUrl = storeDocument?.FileUrl;


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Review(BankAccountReviewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model;

                var bankAccount = _bankAccountService.GetById(review.BankAccountId);
                bankAccount.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                bankAccount.ReviewComment = review.ReviewComment;

                var bankAccountFiles = _filesManagerService.FindByParentId(bankAccount.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

                var bankDocument = bankAccountFiles.Where(f => f.Type == ValueObjects.Enum.FileType.StoreDocument && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();

                if (bankDocument != null)
                {
                    bankDocument.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                }

                _bankAccountService.Edit(bankAccount);
                //_filesManagerService.Update();

                //return RedirectToAction(nameof(Pending));
                return RedirectToAction("details", "merchants", new { id = bankAccount.MerchantId });

            }

            return View(model);
        }
        public IActionResult Delete(Guid id)
        {
            if (id == Guid.Empty)
                return NotFound();

            var bankAccountId = id;

            var bankAccount = _bankAccountService.GetById(bankAccountId);

            if (bankAccount == null)
                return NotFound();

            if (_bankAccountService.Delete(id))
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "حساب بانکی",
                    Content = "اطلاعات با موفقیت حذف شد"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            else
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "حساب بانکی",
                    Content = "حذف اطلاعات امکان پذیر نیست"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return RedirectToAction("Index", new { Id = bankAccount.Merchant.Id });
        }



    }
}