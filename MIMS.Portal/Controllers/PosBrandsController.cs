﻿using MIMS.Portal.Core.Domains;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIMS.Portal.Commons.Extensions;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class PosBrandsController : Controller
    {
        private readonly IPosBrandService _posBrandService;
        private readonly IUsersService _userService;

        public PosBrandsController(IPosBrandService posBrandService, IUsersService userService)
        {
            _posBrandService = posBrandService;
            _userService = userService;
        }

        [AllowAnonymous]
        public JsonResult GetModels(Guid id)
        {
            var posModels = Enumerable.Empty<PosBrand>();

            posModels = _posBrandService.FindByParentId(id);

            var items = posModels.OrderBy(i => i.Name).Select(i => new { Value = i.Id, Text = i.Name });
            return Json(items);
        }

        public IActionResult Index(Guid? id)
        //public IActionResult Index(/*Guid id*/)
        {
            //var brandId = id;
            //var brand = Enumerable.Empty<BrandViewModel>();
            var brands = Enumerable.Empty<BrandViewModel>();
            var parentId = Guid.Empty; //or brandId

            if (id != Guid.Empty || id != null)
                parentId = id.GetValueOrDefault();

            if (parentId != Guid.Empty)
            {
                brands = _posBrandService.FindByParentId(parentId).ToList().ConvertAll(x => (BrandViewModel)x);
            }
            else
            {
                brands = _posBrandService.FindParents().ToList().ConvertAll(x => (BrandViewModel)x);
            }

            // var brand2 = _posBrandService.FindByParentId(brandId).ToList();
            //if (User.IsInRole("Admin"))
            //{
            //    brands = _posBrandService.Find().ToList().ConvertAll(x => (BrandViewModel)x);
            //}

            return View(brands);
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var brandId = id;
            var brand = _posBrandService.FindById(brandId);

            if (brand == null)
                return NotFound();

            var model = (BrandViewModel)brand;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(Guid? id)
        {
            var parentId = Guid.Empty; //or brandId
            var model = new BrandViewModel();

            if (id != Guid.Empty || id != null)
            {
                parentId = id.GetValueOrDefault();
                var parent = _posBrandService.FindById(parentId);
                model.ParentId = parentId;
                model.Parent = parent;
            }

            //if (Id != null)
            //{

            //    var BrandViewModel = new BrandViewModel();
            //    BrandViewModel.ParentId = Id;

            //    return View(BrandViewModel);
            //}

            //var BrandViewModel2 = new BrandViewModel();
            //BrandViewModel2.ParentId = null;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BrandViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //دیگه این محتواها لازم نیست چون بصورت سیستمی در کرنل پروژه ایجاد میشه
                    //var userClaims = HttpContext.User.Identity as ClaimsIdentity;
                    //model.Id = Guid.NewGuid();
                    //لازم به دادن اطلاعات پدر نیست همان آی دی پدر کفایت میکنه
                    //var parent = _posBrandService.Find();


                    //if (model.ParentId!=null)
                    //{
                    //    foreach (var item in parent)
                    //    {
                    //        if (model.ParentId==item.Id)
                    //        {
                    //            model.Parent.Name = item.Name;
                    //        }
                    //    }
                    //}

                    var brand = (PosBrand)model;
                    if (brand.ParentId != null || brand.ParentId != Guid.Empty)
                        brand.Id = Guid.Empty;

                    _posBrandService.Add(brand);

                    var toastr = new Toastr
                    {
                        Type = ToastrType.Success,
                        Subject = "برند",
                        Content = "اطلاعات با موفقیت ثبت شد"
                    };
                    Toastrs.ToastrsList.Add(toastr);

                    return RedirectToAction(nameof(Index), new { id = brand.ParentId });
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == null)
                return BadRequest();

            var brandId = id;
            var brand = _posBrandService.FindById(brandId);

            if (brand == null)
                return NotFound();

            var model = (BrandViewModel)brand;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BrandViewModel model)
        {
            if (ModelState.IsValid)
            {
                var brand = (PosBrand)model;

                _posBrandService.Update(brand);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "برند",
                    Content = "اطلاعات با موفقیت ویرایش شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Details", "PosBrands", new { Id = model.Id });
            }
            return View(model);
        }

        public IActionResult Delete(Guid id)
        {
            var brand = _posBrandService.FindById(id);
            if (brand.ParentId == null)
            {
                var brand2 = _posBrandService.Find();
                foreach (var item in brand2)
                {
                    if (item.ParentId == id)
                    {
                        var toastr = new Toastr
                        {
                            Type = ToastrType.Error,
                            Subject = "برند",
                            Content = "برند مورد نظر دارای زیرمجموعه است. برای حذف باید در ابتدا زیرمجموعه ها حذف شوند."
                        };
                        Toastrs.ToastrsList.Add(toastr);
                        return RedirectToAction("Index");
                    }
                }

            }

            if (_posBrandService.Delete(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("Index");
        }
    }
}
