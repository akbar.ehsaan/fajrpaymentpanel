﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Biz.IServices;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels.Upgarde;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace MIMS.Portal.Controllers
{
    public class UpgradeController : Controller
    {
         private readonly IGeoService _countrySectionService;
         private readonly IUsersService _userService;
         private readonly IDepartmentService _departmentService;
        private readonly IMessageService _messageService;
        private readonly IMerchantService _merchantService;


        public UpgradeController(IUsersService userService, IGeoService countrySectionService,
            IDepartmentService departmentService, IMessageService messageService, IMerchantService merchantService)
        {
            _userService = userService;
            _countrySectionService = countrySectionService;
            _departmentService = departmentService;
            _messageService = messageService;
            _merchantService = merchantService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult SignIn(AuthSignInViewModel signIn)
        {
            var captchaCode = string.Empty;

            if (TempData.ContainsKey("CaptchaCode"))
                captchaCode = TempData["CaptchaCode"].ToString();


            if (ModelState.IsValid)
            {
                if (captchaCode.Equals(signIn.Captcha, StringComparison.OrdinalIgnoreCase))
                {
                    var user = _userService.FindByNationalCode(signIn.NationalCode);

                    if (user != null && user.Mobile == signIn.Mobile)
                    {
                        var otpCode = new Random().Next(1000, 9999);
                        var verifySession = Guid.NewGuid();
                        var numbers = new List<string> { user.Mobile };
                        var text = $"کد تاییدیه عملیات {otpCode}";

                        TempData["OtpCode"] = otpCode;
                        TempData["VerifySession"] = verifySession;
                        TempData["VerifyTicket"] = DateTime.Now.AddMinutes(10);
                        TempData["VerifyId"] = user.Id;

                        _messageService.Send(numbers, text);

                        return RedirectToAction("Verify", new { Id = verifySession });
                    }
                }
            }

            ModelState.AddModelError("", "تایید هویت با این مشخصات مقدور نیست.");
            ViewBag.AuthError = "تایید هویت با این مشخصات مقدور نیست";

            signIn.NationalCode = null;
            signIn.Mobile = null;
            signIn.Captcha = null;

            return View(signIn);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Verify(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var otpCode = string.Empty;
            var verifySession = Guid.Empty;
            var verifyTicket = DateTime.MinValue;
            var verifyId = Guid.Empty;

            if (TempData.ContainsKey("OtpCode"))
                otpCode = TempData["OtpCode"].ToString();

            if (TempData.ContainsKey("VerifySession"))
                verifySession = new Guid(TempData["VerifySession"].ToString());

            if (TempData.ContainsKey("VerifyTicket"))
                verifyTicket = DateTime.Parse(TempData["VerifyTicket"].ToString());

            if (TempData.ContainsKey("VerifyId"))
                verifyId = new Guid(TempData["VerifyId"].ToString());

            if (id != verifySession || verifyTicket < DateTime.Now)
                return BadRequest();

            var model = new AuthVerifyViewModel();
            model.VerifyId = verifyId;
            model.Token = Crypto.HashPassword(otpCode);

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Verify(AuthVerifyViewModel verifyModel)
        {
            if (ModelState.IsValid)
            {
                var userId = verifyModel.VerifyId;
                if (Crypto.VerifyHashedPassword(verifyModel.Token, verifyModel.OtpCode))
                {
                    var verifySession = Guid.NewGuid();
                    TempData["VerifySession"] = verifySession;
                    TempData["VerifyTicket"] = DateTime.Now.AddMinutes(10);
                    TempData["VerifyId"] = verifyModel.VerifyId;

                    return RedirectToAction("UpdateInfo", new { Id = verifyModel.VerifyId, Session = verifySession });
                }
            }

            ModelState.AddModelError("", "تایید عملیات با این مشخصات مقدور نیست.");
            ViewBag.AuthError = "تایید عملیات با این مشخصات مقدور نیست";

            verifyModel.OtpCode = string.Empty;

            return View(verifyModel);
        }


        [AllowAnonymous]
        [HttpGet]
        public IActionResult UpdateInfo(Guid id, Guid session)
        {
            //if (id == Guid.Empty || session == Guid.Empty)
            //    return BadRequest();

            var verifySession = Guid.Empty;
            var verifyTicket = DateTime.MinValue;
            var verifyId = Guid.Empty;

            if (TempData.ContainsKey("VerifySession"))
                verifySession = new Guid(TempData["VerifySession"].ToString());

            if (TempData.ContainsKey("VerifyTicket"))
                verifyTicket = DateTime.Parse(TempData["VerifyTicket"].ToString());

            if (TempData.ContainsKey("VerifyId"))
                verifyId = new Guid(TempData["VerifyId"].ToString());

            //if (id != verifyId || session != verifySession || verifyTicket < DateTime.Now)
            //    return BadRequest();

            var merchant = _merchantService.GetByNationalCode("0945198043");

            var merchantInfo = new  MerchantInfoViewModel();
            merchantInfo.Merchant = merchant;
            merchantInfo.VerifyId = verifyId;
            merchantInfo.Token = Crypto.HashPassword(session.ToString());

            return View(merchantInfo);
        }

    }
}
