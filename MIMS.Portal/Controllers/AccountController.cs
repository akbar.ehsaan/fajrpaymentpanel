﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Commons;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AccountController : Controller
    {
        IGeoService _countrySectionService;
        IUsersService _userService;
        IDepartmentService _departmentService;
        IMessageService _messageService;


        public AccountController(IUsersService userService, IGeoService countrySectionService,
            IDepartmentService departmentService, IMessageService messageService)
        {
            _userService = userService;
            _countrySectionService = countrySectionService;
            _departmentService = departmentService;
            _messageService = messageService;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Profile()
        {
            var x = HttpContext.User.Identity as ClaimsIdentity;
            var userId = new Guid(x.FindFirst("userId").Value);

            var user = _userService.FindById(userId);

            if (user == null)
                return NotFound();

            var model = (UserViewModel)user;

            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult SignIn(SignInViewModel signIn)
        {
            var captchaCode = string.Empty;

            if (TempData.ContainsKey("CaptchaCode"))
                captchaCode = TempData["CaptchaCode"].ToString();

            if (ModelState.IsValid)
            {
                if (captchaCode.Equals(signIn.Captcha, StringComparison.OrdinalIgnoreCase))
                {
                    string token = _userService.SignIn(signIn.UserName, signIn.Password);

                    if (token != "Not_Found")
                    {
                        Response.Cookies.Append("AccessToken", token);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            ModelState.AddModelError("", "تایید هویت با این مشخصات مقدور نیست.");
            ViewBag.AuthError = "تایید هویت با این مشخصات مقدور نیست";

            signIn.UserName = string.Empty;
            signIn.Password = string.Empty;
            signIn.Captcha = string.Empty;

            return View(signIn);

        }

        public IActionResult SignOut()
        {
            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
            }
            return RedirectToAction("SignIn");
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult ForgotPassword(ForgotPasswordViewModel forgotPassword)
        {
            var captchaCode = string.Empty;

            if (TempData.ContainsKey("CaptchaCode"))
                captchaCode = TempData["CaptchaCode"].ToString();

            if (ModelState.IsValid)
            {
                if (captchaCode.Equals(forgotPassword.Captcha, StringComparison.OrdinalIgnoreCase))
                {
                    var user = _userService.FindByUserName(forgotPassword.UserName);

                    if (user != null && user.Mobile == forgotPassword.Mobile)
                    {
                        var otpCode = new Random().Next(1000, 9999);
                        var recoverySession = Guid.NewGuid();
                        var numbers = new List<string> { user.Mobile };
                        var text = $"کد تاییدیه عملیات {otpCode}";

                        TempData["OtpCode"] = otpCode;
                        TempData["RecoverySession"] = recoverySession;
                        TempData["RecoveryTicket"] = DateTime.Now.AddMinutes(10);
                        TempData["RecoveryId"] = user.Id;

                        _messageService.Send(numbers, text);

                        return RedirectToAction("RecoveryPassword", new { Id = recoverySession });
                    }
                }
            }

            ModelState.AddModelError("", "تایید هویت با این مشخصات مقدور نیست.");
            ViewBag.AuthError = "تایید هویت با این مشخصات مقدور نیست";

            forgotPassword.UserName = string.Empty;
            forgotPassword.Mobile = string.Empty;
            forgotPassword.Captcha = string.Empty;

            return View(forgotPassword);

        }


        [AllowAnonymous]
        [HttpGet]
        public IActionResult RecoveryPassword(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var otpCode = string.Empty;
            var recoverySession = Guid.Empty;
            var recoveryTicket = DateTime.MinValue;
            var recoveryId = Guid.Empty;

            if (TempData.ContainsKey("OtpCode"))
                otpCode = TempData["OtpCode"].ToString();

            if (TempData.ContainsKey("RecoverySession"))
                recoverySession = new Guid(TempData["RecoverySession"].ToString());

            if (TempData.ContainsKey("RecoveryTicket"))
                recoveryTicket = DateTime.Parse(TempData["RecoveryTicket"].ToString());

            if (TempData.ContainsKey("RecoveryTicket"))
                recoveryId = new Guid(TempData["RecoveryId"].ToString());


            if (id != recoverySession || recoveryId != recoveryId || recoveryTicket < DateTime.Now)
                return BadRequest();

            var model = new RecoveryPasswordViewModel();
            model.RecoveryId = recoveryId;
            model.Token = Crypto.HashPassword(otpCode);

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult RecoveryPassword(RecoveryPasswordViewModel recoveryPassword)
        {

            if (ModelState.IsValid)
            {
                var userId = recoveryPassword.RecoveryId;
                if (Crypto.VerifyHashedPassword(recoveryPassword.Token, recoveryPassword.OtpCode))
                {
                    var user = _userService.SetPassword(userId, recoveryPassword.NewPassword);
                    return RedirectToAction("SignIn");
                }
            }

            ModelState.AddModelError("", "تایید عملیات با این مشخصات مقدور نیست.");
            ViewBag.AuthError = "تایید عملیات با این مشخصات مقدور نیست";

            recoveryPassword.OtpCode = string.Empty;
            recoveryPassword.NewPassword = string.Empty;
            recoveryPassword.ConfirmNewPassword = string.Empty;

            return View(recoveryPassword);
        }

        [AllowAnonymous]
        public IActionResult GetCaptcha()
        {
            int width = 500;
            int height = 165;
            var captchaCode = new Captcha().GenerateCaptchaCode();
            var result = new Captcha().GenerateCaptchaImage(width, height, captchaCode);

            Response.Clear();
            Response.ContentType = "image/jpeg";
            //HttpContext.Session.SetString("CaptchaCode", result.CaptchaCode);
            TempData["CaptchaCode"] = captchaCode;

            Stream s = new MemoryStream(result.CaptchaByteData);

            using (var ms = new MemoryStream())
            {
                var img = new System.Drawing.Bitmap(s);
                img.Save(ms, ImageFormat.Jpeg);
                img.Dispose();
                return File(ms.ToArray(), "image/jpeg");
            }
            //return new FileStreamResult(s, "image/png");
        }
    }
}
