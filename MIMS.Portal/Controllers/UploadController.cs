﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class UploadController : Controller
    {
        IFilesManagerService _fileuploadService;
         IHostingEnvironment _hostingEnvironment;

        public UploadController(IFilesManagerService fileuploadService, IHostingEnvironment hostingEnvironment)
        {
            _fileuploadService = fileuploadService;
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index(Guid id)
        {
            ViewBag.destinationid = id;
            return View(_fileuploadService.FindByParentId(id));
            
        }

        [HttpPost]
        public async Task<IActionResult> Index(List<IFormFile> files,string Destinationid, string Type)
        {
            long size = files.Sum(f => f.Length);
            
            var filePaths = new List<string>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    // full path to file in temp location

                    
                   //  var filePath = Environment.CurrentDirectory+"\\wwwroot\\upload"; //we are using Temp file name just for the example. Add your own file path.
                   var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");
                    filePaths.Add(uploads);
                    Guid fileid= Guid.NewGuid();
                    using (var stream = new FileStream(Path.Combine(uploads,fileid+".jpg"), FileMode.Create))
                    {
                        UploadFile fileupload = new UploadFile();
                        fileupload.Id =fileid;
                        //fileupload.Type = Type;
                        fileupload.ParentItemId =new Guid( Destinationid);
                        _fileuploadService.Create(fileupload);
                        await formFile.CopyToAsync(stream);
                    }
                }
            }
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return RedirectToAction("Index",new { id = Destinationid });
            // process uploaded files
            // Don't rely on or trust the FileName property without validation.
            //return Ok(new { count = files.Count, size, filePaths });
        }
       
        public IActionResult Download(Guid id)
        {
            var path = Path.Combine(_hostingEnvironment.WebRootPath, "Upload", $"{id}.jpg");
            var imageFileStream = System.IO.File.OpenRead(path);
            
            return File(imageFileStream, "Upload/jpeg",id+".jpg");
        }
        public IActionResult Delete(Guid id, string Destinationid)
        {
            if (_fileuploadService.Delete(id))
                TempData["Message"] = "اطلاعات با موفقیت حذف شد";
            else TempData["Error"] = "حذف اطلاعات امکان پذیر نمی باشد";

            return RedirectToAction("Index", new { Destinationid = Destinationid });
        }
    }
}
