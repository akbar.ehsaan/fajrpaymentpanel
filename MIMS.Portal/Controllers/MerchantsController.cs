﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.ViewModels;
using MIMS.Portal.ValueObjects.Enum;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Biz.IServices;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class MerchantsController : Controller
    {
        private readonly IMerchantService _merchantService;
        private readonly IUsersService _userService;
        private readonly IFilesManagerService _filesManagerService;
        private readonly int _maxFileSize = 1500000;

        public MerchantsController(IMerchantService merchantService, IUsersService userService, IFilesManagerService filesManagerService)
        {
            _userService = userService;
            _merchantService = merchantService;
            _filesManagerService = filesManagerService;
        }

        //---//


        public IActionResult Index(Guid? id)
        {
            var merchantsIndex = new MerchantIndexViewModel();

            Guid userId = id.GetValueOrDefault();

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            if (User.IsInRole("Admin"))
            {
                merchantsIndex.Stats = _merchantService.StatsByReviewStatus(null);
                merchantsIndex.Merchants = _merchantService.Filter(include: x => x.Include(i => i.CreatorUser)).ToList().ConvertAll(x => (MerchantViewModel)x);

                //if (id == null || id == Guid.Empty)
                //    merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                //else
                //    merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }
            else
            {
                merchantsIndex.Stats = _merchantService.StatsByReviewStatus(userId);
                merchantsIndex.Merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }

            return View(merchantsIndex);
        }

        [HttpPost]
        public IActionResult Index(Guid? id, MerchantFilterViewModel? filters)
        {
            Guid userId = id.GetValueOrDefault();
            var merchantsIndex = new MerchantIndexViewModel();

            if (userId == Guid.Empty)
            {
                var x = HttpContext.User.Identity as ClaimsIdentity;
                userId = new Guid(x.FindFirst("userId").Value);
            }

            //Expression<Func<Merchant, bool>> predicate = null;
            var predicate = PredicateBuilder.True<Merchant>();
            var andCriteria = new List<Predicate<Merchant>>();
            var orCriteria = new List<Predicate<Merchant>>();
            var orParams = PredicateBuilder.False<Merchant>();
            var andParams = PredicateBuilder.True<Merchant>();

            if (!string.IsNullOrWhiteSpace(filters.NationalCode))
            {
                orCriteria.Add(f => f.NationalCode == filters.NationalCode);
                orParams = orParams.Or(f => f.NationalCode == filters.NationalCode);
            }

            if (!string.IsNullOrWhiteSpace(filters.FirstName))
            {
                orCriteria.Add(f => f.FirstName.Contains(filters.FirstName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.FirstName.Contains(filters.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(filters.LastName))
            {
                orCriteria.Add(f => f.LastName.Contains(filters.LastName, StringComparison.OrdinalIgnoreCase));
                orParams = orParams.Or(f => f.LastName.Contains(filters.LastName));
            }

            if (filters.Status != null)
            {
                orCriteria.Add(f => f.Status == filters.Status);
                orParams = orParams.Or(f => f.Status == filters.Status);
            }

            if (!User.IsInRole("Admin"))
            {
                andCriteria.Add(f => f.CreatorUserId == userId);
                andParams= andParams.And(f => f.CreatorUserId == userId);
                andParams= andParams.And(orParams);
            }


            predicate = c => andCriteria.All(pred => pred(c)) && orCriteria.Any(pred => pred(c));
            predicate = !User.IsInRole("Admin") ? andParams : orParams;


            if (User.IsInRole("Admin"))
            {
                if (id.GetValueOrDefault() == Guid.Empty)
                {
                    merchantsIndex.Stats = _merchantService.StatsByReviewStatus(null);
                    merchantsIndex.Merchants = _merchantService.Filter(predicate, include: x => x.Include(i => i.CreatorUser)).ToList().ConvertAll(x => (MerchantViewModel)x);
                    //merchants = _merchantService.GetAll().ToList().ConvertAll(x => (MerchantViewModel)x);
                }
                else
                {
                    merchantsIndex.Stats = _merchantService.StatsByReviewStatus(userId);
                    merchantsIndex.Merchants = _merchantService.Filter(predicate, include: x => x.Include(i => i.CreatorUser)).ToList().ConvertAll(x => (MerchantViewModel)x);
                    //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
                }
            }
            else
            {
                merchantsIndex.Stats = _merchantService.StatsByReviewStatus(userId);
                merchantsIndex.Merchants = _merchantService.Filter(predicate, include: x => x.Include(i => i.CreatorUser)).ToList().ConvertAll(x => (MerchantViewModel)x);

                //merchants = _merchantService.GetByUserId(userId).ToList().ConvertAll(x => (MerchantViewModel)x);
            }

            return View(merchantsIndex);
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var model = (MerchantViewModel)merchant;

            var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

            var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront).FirstOrDefault();
            model.IDCardFrontUrl = idCardFront?.FileUrl;

            var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack).FirstOrDefault();
            model.IDCardBackUrl = idCardBack?.FileUrl;

            var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage).FirstOrDefault();
            model.CredentialMainPageUrl = credentialMainPage?.FileUrl;

            var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage).FirstOrDefault();
            model.CredentialLastPageUrl = credentialLastPage?.FileUrl;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(MerchantViewModel model)
        {

            var isFileSizeValid = true;


            if (model.IDCardFront != null && model.IDCardFront?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.IDCardFront), "حجم فایل نامعتبر");
            }
            if (model.IDCardBack != null && model.IDCardBack?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.IDCardBack), "حجم فایل نامعتبر");
            }
            if (model.CredentialMainPage != null && model.CredentialMainPage?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.CredentialMainPage), "حجم فایل نامعتبر");
            }
            if (model.CredentialLastPage != null && model.CredentialLastPage?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.CredentialLastPage), "حجم فایل نامعتبر");
            }

            if (ModelState.IsValid && isFileSizeValid)
            {
                var merchant = (Merchant)model;
                merchant.Status = ReviewStatus.Pending;
                _merchantService.Create(merchant);

                _filesManagerService.SaveFile(model.IDCardFront, FileSource.Merchant, FileType.IDCardFront, merchant.Id);
                _filesManagerService.SaveFile(model.IDCardBack, FileSource.Merchant, FileType.IDCardBack, merchant.Id);
                _filesManagerService.SaveFile(model.CredentialMainPage, FileSource.Merchant, FileType.CredentialMainPage, merchant.Id);
                _filesManagerService.SaveFile(model.CredentialLastPage, FileSource.Merchant, FileType.CredentialLastPage, merchant.Id);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "پذیرنده",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Create", "BankAccounts", new { Id = merchant.Id, Next = true });
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var model = (MerchantEditViewModel)merchant;

            var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

            var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront).FirstOrDefault();
            model.IDCardFrontId = idCardFront?.Id ?? Guid.Empty;
            model.IDCardFrontUrl = idCardFront?.FileUrl;

            var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack).FirstOrDefault();
            model.IDCardBackId = idCardBack?.Id ?? Guid.Empty;
            model.IDCardBackUrl = idCardBack?.FileUrl;

            var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage).FirstOrDefault();
            model.CredentialMainPageId = credentialMainPage?.Id ?? Guid.Empty;
            model.CredentialMainPageUrl = credentialMainPage?.FileUrl;

            var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage).FirstOrDefault();
            model.CredentialLastPageId = credentialLastPage?.Id ?? Guid.Empty;
            model.CredentialLastPageUrl = credentialLastPage?.FileUrl;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(MerchantEditViewModel model)
        {
            var isFileSizeValid = true;

            if (model.IDCardFront != null && model.IDCardFront?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.IDCardFront), "حجم فایل نامعتبر");
            }
            if (model.IDCardBack != null && model.IDCardBack?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.IDCardBack), "حجم فایل نامعتبر");
            }
            if (model.CredentialMainPage != null && model.CredentialMainPage?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.CredentialMainPage), "حجم فایل نامعتبر");
            }
            if (model.CredentialLastPage != null && model.CredentialLastPage?.Length > _maxFileSize)
            {
                isFileSizeValid = false;
                ModelState.AddModelError(nameof(model.CredentialLastPage), "حجم فایل نامعتبر");
            }

            if (ModelState.IsValid && isFileSizeValid)
            {
                var merchant = (Merchant)model;
                merchant.Status = ReviewStatus.Pending;
                _merchantService.Edit(merchant);

                if (model.IDCardFront != null)
                {
                    _filesManagerService.SaveFile(model.IDCardFront, FileSource.Merchant, FileType.IDCardFront, merchant.Id);
                }
                else if (model.IDCardFrontId != null && model.IDCardFrontId != Guid.Empty)
                {
                    _filesManagerService.SetStatus(model.IDCardFrontId, ReviewStatus.Pending);
                }

                if (model.IDCardBack != null)
                {
                    _filesManagerService.SaveFile(model.IDCardBack, FileSource.Merchant, FileType.IDCardBack, merchant.Id);
                }
                else if (model.IDCardBackId != null && model.IDCardBackId != Guid.Empty)
                {
                    _filesManagerService.SetStatus(model.IDCardBackId, ReviewStatus.Pending);
                }

                if (model.CredentialMainPage != null)
                {
                    _filesManagerService.SaveFile(model.CredentialMainPage, FileSource.Merchant, FileType.CredentialMainPage, merchant.Id);
                }
                else if (model.CredentialMainPageId != null && model.CredentialMainPageId != Guid.Empty)
                {
                    _filesManagerService.SetStatus(model.CredentialMainPageId, ReviewStatus.Pending);
                }

                if (model.CredentialLastPage != null)
                {
                    _filesManagerService.SaveFile(model.CredentialLastPage, FileSource.Merchant, FileType.CredentialLastPage, merchant.Id);
                }
                else if (model.CredentialLastPageId != null && model.CredentialLastPageId != Guid.Empty)
                {
                    _filesManagerService.SetStatus(model.CredentialLastPageId, ReviewStatus.Pending);
                }

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "پذیرنده",
                    Content = "اطلاعات با موفقیت ویرایش شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Details", "Merchants", new { Id = model.Id });
            }

            var merchantFiles = _filesManagerService.FindByParentId(model.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

            var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront).FirstOrDefault();
            model.IDCardFrontId = idCardFront?.Id ?? Guid.Empty;
            model.IDCardFrontUrl = idCardFront?.FileUrl;

            var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack).FirstOrDefault();
            model.IDCardBackId = idCardBack?.Id ?? Guid.Empty;
            model.IDCardBackUrl = idCardBack?.FileUrl;

            var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage).FirstOrDefault();
            model.CredentialMainPageId = credentialMainPage?.Id ?? Guid.Empty;
            model.CredentialMainPageUrl = credentialMainPage?.FileUrl;

            var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage).FirstOrDefault();
            model.CredentialLastPageId = credentialLastPage?.Id ?? Guid.Empty;
            model.CredentialLastPageUrl = credentialLastPage?.FileUrl;

            return View(model);
        }

        public ActionResult Pending()
        {
            var merchants = Enumerable.Empty<MerchantViewModel>();

            merchants = _merchantService.GetAll().Where(m => m.Status == ValueObjects.Enum.ReviewStatus.Pending || m.Status == ValueObjects.Enum.ReviewStatus.Unknown).ToList().ConvertAll(x => (MerchantViewModel)x);

            return View(merchants);
        }

        public ActionResult Review(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var merchantId = id;
            var merchant = _merchantService.GetById(merchantId);

            if (merchant == null)
                return NotFound();

            var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

            var merchantDetails = (MerchantViewModel)merchant;
            var merchantReview = new MerchantReviewViewModel();

            var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.IDCardFrontId = idCardFront?.Id ?? Guid.Empty;
            merchantReview.IDCardFrontUrl = idCardFront?.FileUrl;

            var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.IDCardBackId = idCardBack?.Id ?? Guid.Empty;
            merchantReview.IDCardBackUrl = idCardBack?.FileUrl;

            var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.CredentialMainPageId = credentialMainPage?.Id ?? Guid.Empty;
            merchantReview.CredentialMainPageUrl = credentialMainPage?.FileUrl;

            var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
            merchantReview.CredentialLastPageId = credentialLastPage?.Id ?? Guid.Empty;
            merchantReview.CredentialLastPageUrl = credentialLastPage?.FileUrl;


            var model = new MerchantDetailsReviewViewModel();
            model.MerchantDetails = merchantDetails;
            model.MerchantReview = merchantReview;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Review(MerchantReviewedViewModel model)
        {
            if (ModelState.IsValid)
            {
                var review = model.MerchantReview;

                var merchant = _merchantService.GetById(review.MerchantId);
                merchant.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;
                merchant.ReviewComment = review.ReviewComment;

                var merchantFiles = _filesManagerService.FindByParentId(merchant.Id).Where(f => f.Source == ValueObjects.Enum.FileSource.Merchant).OrderByDescending(f => f.CreatedDate);

                var idCardFront = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardFront && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if (idCardFront != null)
                    idCardFront.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var idCardBack = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.IDCardBack && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if (idCardBack != null)
                    idCardBack.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var credentialMainPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialMainPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if (credentialMainPage != null)
                    credentialMainPage.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                var credentialLastPage = merchantFiles.Where(f => f.Type == ValueObjects.Enum.FileType.CredentialLastPage && (f.Status == ValueObjects.Enum.ReviewStatus.Pending || f.Status == ValueObjects.Enum.ReviewStatus.Unknown)).FirstOrDefault();
                if (credentialLastPage != null)
                    credentialLastPage.Status = review.ReviewStatus ? ValueObjects.Enum.ReviewStatus.PreApproved : ValueObjects.Enum.ReviewStatus.Rejected;

                _merchantService.Edit(merchant);
                //_filesManagerService.Update();
            }
            //return RedirectToAction(nameof(Pending));
            return RedirectToAction("details", "merchants", new { id = model.MerchantReview.MerchantId });

        }

        public IActionResult Delete(Guid id)
        {
            if (_merchantService.Delete(id))
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "پذیرنده",
                    Content = "اطلاعات با موفقیت حذف شد"
                };
                Toastrs.ToastrsList.Add(toastr);
            }
            else
            {
                var toastr = new Toastr
                {
                    Type = ToastrType.Error,
                    Subject = "پذیرنده",
                    Content = "حذف اطلاعات امکان پذیر نیست"
                };
                Toastrs.ToastrsList.Add(toastr);
            }

            return RedirectToAction("Index");
        }
    }
}
