﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Service;
using MIMS.Portal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.Core.Data.Domains;

namespace MIMS.Portal.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]

    public class TicketsController : Controller
    {
        ITicketService _ticketService;
        IUsersService _userService;
        IHostingEnvironment _hostingEnvironment;
        IFilesManagerService _fileuploadService;

        public TicketsController(IFilesManagerService fileuploadService, ITicketService ticketService, IUsersService userService, IHostingEnvironment hostingEnvironment)
        {
            _userService = userService;
            _ticketService = ticketService;
            _hostingEnvironment = hostingEnvironment;
            _fileuploadService = fileuploadService;

        }

        public IActionResult Index()
        {
            var x = HttpContext.User.Identity as ClaimsIdentity;
            var tickets = _ticketService.TicketByUserId(x.FindFirst("userId").Value).ToList().ConvertAll(x => (TicketViewModel)x);

            return View(tickets);
        }

        public IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var ticketId = id;
            var ticketThreads = _ticketService.FindTicketDetailById(ticketId).ToList().ConvertAll(x => (TicketViewModel)x);

            if (ticketThreads == null)
                return NotFound();

            var model = ticketThreads;

            var x = HttpContext.User.Identity as ClaimsIdentity;
            ViewBag.currentuserid = x.FindFirst("userId").Value;
            List<User> lstuser = _userService.FindByRole("agent");
            lstuser.AddRange(_userService.FindByRole("admin"));
            ViewBag.agentusers = lstuser.Where(i => i.Role == "agent").Distinct().ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TicketViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ticket = (Ticket)model;

                var x = HttpContext.User.Identity as ClaimsIdentity;

                ticket.User = _userService.FindById(new Guid( x.FindFirst("userId").Value));
                ticket.SenderUserId = new Guid(x.FindFirst("userId").Value);
                ticket.ReciverrUserId = _userService.FindByRole("admin").First().Id;
                ticket.Id = new Guid();
                ticket.SubmitDate = DateTime.Now;

                _ticketService.Add(ticket);

                if (model.Attachment != null)
                {
                    var attachments = new List<IFormFile>();
                    attachments.Add(model.Attachment);
                    long size = attachments.Sum(f => f.Length);

                    var filePaths = new List<string>();

                    foreach (var file in attachments)
                    {
                        if (file.Length > 0)
                        {
                            // full path to file in temp location


                            //  var filePath = Environment.CurrentDirectory+"\\wwwroot\\upload"; //we are using Temp file name just for the example. Add your own file path.
                            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");
                            filePaths.Add(uploads);
                            Guid fileid = Guid.NewGuid();
                            using (var stream = new FileStream(Path.Combine(uploads, fileid + Path.GetExtension(file.FileName)), FileMode.Create))
                            {
                                UploadFile fileupload = new UploadFile();
                                fileupload.Id = fileid;
                                fileupload.ParentItemId = ticket.Id;
                                //fileupload.Type = "Ticket";
                                // fileupload.DestinationId = new Guid(Destinationid);
                                _fileuploadService.Create(fileupload);
                                await file.CopyToAsync(stream);
                            }
                        }
                    }
                }

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "تیکت پشتیبانی",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Close(string ticketid)
        {


            _ticketService.Close(ticketid);
            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return RedirectToAction("index");
        }
        [HttpPost]
        public async Task<IActionResult> Create1(string Subject, string Body, List<IFormFile> files)
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateReply(string Subject, string Body, List<IFormFile> files, string parentid, Guid reciverid)
        {
            long size = files.Sum(f => f.Length);
            Ticket _tmp = new Ticket();
            _tmp.Id = Guid.NewGuid();
            _tmp.Subject = Subject;
            _tmp.ParentId = new Guid(parentid);
            _tmp.Body = Body;
            _tmp.TicketStatus = "Pending";
            _tmp.SubmitDate = DateTime.Now;
            var x = HttpContext.User.Identity as ClaimsIdentity;
            _tmp.User = _userService.FindById(new Guid(x.FindFirst("userId").Value));
            _tmp.SenderUserId = new Guid(x.FindFirst("userId").Value);
            _tmp.ReciverrUserId = _userService.FindById(reciverid).Id;

            _ticketService.Add(_tmp);
            var filePaths = new List<string>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    // full path to file in temp location


                    //  var filePath = Environment.CurrentDirectory+"\\wwwroot\\upload"; //we are using Temp file name just for the example. Add your own file path.
                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");
                    filePaths.Add(uploads);
                    Guid fileid = Guid.NewGuid();
                    using (var stream = new FileStream(Path.Combine(uploads, fileid + ".zip"), FileMode.Create))
                    {
                        UploadFile fileupload = new UploadFile();
                        fileupload.Id = fileid;
                        fileupload.ParentItemId = _tmp.Id;
                        //fileupload.Type = "Ticket";
                        // fileupload.DestinationId = new Guid(Destinationid);
                        _fileuploadService.Create(fileupload);
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            TempData["Message"] = "اطلاعات با موفقیت ثبت شد";

            return RedirectToAction("ticketdetail", new { ticketid = parentid });
        }

    }
}
