﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIMS.Portal.Service;
using Microsoft.AspNetCore.Mvc;
using MIMS.Portal.Core.Domains;
using MIMS.Portal.Commons.Extensions;
using MIMS.Portal.ViewModels;

namespace MIMS.Portal.Controllers
{
    public class NewsController : Controller
    {
 
        INewsService _newService;
        public NewsController(  INewsService newService)
        {
        
            _newService = newService;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var news = _newService.FindAll().ToList().ConvertAll(x => (NewsViewModel)x);

            return View(news);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(NewsViewModel model)
        {

            if (ModelState.IsValid)
            {
                var news = (News)model;
                _newService.Add(news);

                var toastr = new Toastr
                {
                    Type = ToastrType.Success,
                    Subject = "ثبت خبر",
                    Content = "اطلاعات با موفقیت ثبت شد"
                };
                Toastrs.ToastrsList.Add(toastr);

                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }
    }
}
