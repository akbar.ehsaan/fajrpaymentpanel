﻿//--- Geo Data ---//

$(document).ready(function () {
    var geoUrl = '/country-section/get-sections';
    var country = $("#CountryId");
    var province = $("#ProvinceId");
    var county = $("#CountyId");
    var city = $("#CityId");
    var town = $("#TownId");

    province.change(function () {
        county.empty();
        city.empty();

        $.ajax({
            type: 'POST',
            url: geoUrl,
            dataType: 'json',
            data: { id: province.val() },

            success: function (items) {
                city.empty().append($('<option></option>').val(null).text("--"));
                county.append($('<option></option>').val(null).text("--"));
                $.each(items, function (i, item) {
                    county.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                county.append($('<option></option>').val(null).text("--"));
                city.empty().append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })

    county.change(function () {

        city.empty();

        $.ajax({
            type: 'POST',
            url: geoUrl,
            dataType: 'json',
            data: { id: county.val() },

            success: function (items) {
                city.append($('<option></option>').val(null).text("--"));
                $.each(items, function (i, item) {
                    city.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                city.append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })

    city.change(function () {
        town.empty();
        $.ajax({
            type: 'POST',
            url: geoUrl,
            dataType: 'json',
            data: { id: city.val() },

            success: function (items) {
                town.append($('<option></option>').val(null).text("--"));
                $.each(items, function (i, item) {
                    town.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                town.append($('<option></option>').val(null).text("--"));
            }
        });
        return false;
    })
});

//--- Business Scopes ---//

$(document).ready(function () {
    var businessUrl = '/stores/get-scopes';
    var businessGroup = $("#BusinessGroupId");
    var businessScope = $("#BusinessScopeId");

    businessGroup.change(function () {

        businessScope.empty();

        $.ajax({
            type: 'POST',
            url: businessUrl,
            dataType: 'json',
            data: { id: businessGroup.val() },

            success: function (items) {
                businessScope.empty().append($('<option></option>').val(null).text("--"));

                $.each(items, function (i, item) {
                    businessScope.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                businessScope.append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })
});

//--- POS Brnads Data ---//

$(document).ready(function () {
    var brandUrl = '/pos-brands/get-models';
    var posBrand = $("#PosBrandId");
    var posModel = $("#PosModelId");

    posBrand.change(function () {

        posModel.empty();

        $.ajax({
            type: 'POST',
            url: brandUrl,
            dataType: 'json',
            data: { id: posBrand.val() },

            success: function (items) {
                posModel.empty().append($('<option></option>').val(null).text("--"));

                $.each(items, function (i, item) {
                    posModel.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                posModel.append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })
});

//--- Device Data ---//

$(document).ready(function () {
    var brandUrl = '/pos-brands/get-models';
    var deviceUrl = '/pos-devices/get-devices';
    var deviceConnectionType = $("#DeviceConnectionType");
    var deviceBrandId = $("#DeviceBrandId");
    var deviceModelId = $("#DeviceModelId");
    var deviceId = $("#DeviceId");

    deviceBrandId.change(function () {
        deviceModelId.empty();
        deviceId.empty();

        $.ajax({
            type: 'POST',
            url: brandUrl,
            dataType: 'json',
            data: { id: deviceBrandId.val() },

            success: function (items) {
                deviceModelId.empty().append($('<option></option>').val(null).text("--"));
                deviceId.append($('<option></option>').val(null).text("--"));
                $.each(items, function (i, item) {
                    deviceModelId.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                deviceModelId.append($('<option></option>').val(null).text("--"));
                deviceId.empty().append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })

    deviceModelId.change(function () {

        deviceId.empty();

        $.ajax({
            type: 'POST',
            url: deviceUrl,
            dataType: 'json',
            data: { id: deviceModelId.val(), connectionType: deviceConnectionType.val() },

            success: function (items) {
                deviceId.append($('<option></option>').val(null).text("--"));
                $.each(items, function (i, item) {
                    deviceId.append($('<option></option>').val(item.value).text(item.text));
                });
            },
            error: function () {
                deviceId.append($('<option></option>').val(null).text("--"));
            }
        });

        return false;
    })
});