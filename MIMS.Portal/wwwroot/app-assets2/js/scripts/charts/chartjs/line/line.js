﻿/*=========================================================================================
    File Name: line.js
    Description: Chartjs simple line chart
    ----------------------------------------------------------------------------------------
    Item Name: Rybin - Responsive Admin Theme
    Version: 1.2
    Author: MEITS Group
    Author URL: http://www.meitsgroup.ir
==========================================================================================*/

// Line chart
// ------------------------------
$(window).on("load", function(){

    Chart.defaults.global.defaultFontFamily = "'Vazir', 'Tahoma', 'Arial', sans-serif";

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#line-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false, 
        legend: {
            position: 'bottom',
            labels: {
                rtl: true,
                //textDirection: rtl
            }
        },
        tooltips: {
            //titleAlign: right,
            rtl: true,
            //textDirection: rtl
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'ماه'
                },
                ticks: {
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'مقدار'
                }
            }]
        },
        title: {
            display: true,
            text: 'نمودار نمونه ترکیبی'
        }
    };

    // Chart Data
    var chartData = {
        labels: ["تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی"],
        datasets: [{
            label: "چشمی",
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderDash: [5, 5],
            borderColor: "#673AB7",
            pointBorderColor: "#673AB7",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }, {
            label: "پذیرش",
            data: [28, 48, 40, 19, 86, 27, 90],
            fill: false,
            borderDash: [5, 5],
            borderColor: "#00BCD4",
            pointBorderColor: "#00BCD4",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }, {
            label: "ابطال",
            data: [45, 25, 16, 36, 67, 18, 76],
            lineTension: 0,
            fill: false,
            borderColor: "#fff",
            pointBorderColor: "#FF57ff22",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };

    var config = {
        type: 'line',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);
});

