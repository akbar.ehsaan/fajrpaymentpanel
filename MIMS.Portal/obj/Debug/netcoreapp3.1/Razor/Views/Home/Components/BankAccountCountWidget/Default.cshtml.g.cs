#pragma checksum "C:\Users\tahere\Desktop\FAjr\MIMS.Portal\Views\Home\Components\BankAccountCountWidget\Default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "434ec9db4798ca4b9c2e41fff0438c24a9e27e95"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Components_BankAccountCountWidget_Default), @"mvc.1.0.view", @"/Views/Home/Components/BankAccountCountWidget/Default.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\tahere\Desktop\FAjr\MIMS.Portal\Views\_ViewImports.cshtml"
using MIMS.Portal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\tahere\Desktop\FAjr\MIMS.Portal\Views\_ViewImports.cshtml"
using MIMS.Portal.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"434ec9db4798ca4b9c2e41fff0438c24a9e27e95", @"/Views/Home/Components/BankAccountCountWidget/Default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dcbe9cfbfd4b5187f9f3921f68fc8316a1c9ee9c", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Components_BankAccountCountWidget_Default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"col-lg-3 col-sm-6\">\r\n    <div class=\"card gradient-3\">\r\n        <div class=\"card-body\">\r\n            <h3 class=\"card-title text-white\">پذیرندگان</h3>\r\n            <div class=\"d-inline-block\">\r\n                <h2 class=\"text-white\">");
#nullable restore
#line 6 "C:\Users\tahere\Desktop\FAjr\MIMS.Portal\Views\Home\Components\BankAccountCountWidget\Default.cshtml"
                                  Write(TempData["MerchantsCount"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n                <p class=\"text-white mb-0\">ثبت شده</p>\r\n            </div>\r\n            <span class=\"float-right display-5 opacity-5\"><i class=\"fa fa-users\"></i></span>\r\n        </div>\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
